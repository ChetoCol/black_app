var nombreaplicacion = 'Black';
var direccionservidor = "https://www.blacktransport.co/";
var dirllegadaPush = null;
var dirdestinoPush = null;
var IDPUSH='704366433629';
document.addEventListener("deviceready", registerdevice, false);
function registerdevice(){
    //var ua = navigator.userAgent;
    /*if( ua.indexOf("Android") >= 0 ) {
        cordova.plugins.ConfigAlert.alert({
            title: "Huawei Protected Apps",
            message: "Esta aplicación debe estar habilitada en 'Aplicaciones protegidas' para funcionar correctamente, por favor no omita este mensaje y realice el ajuste, gracias.",
            ok: "Habilitar",
            cancel: "Recordar",
            package: "com.huawei.systemmanager",
            class: "optimize.process.ProtectActivity",
            force: false, // force alert, ignoring user selection and value of SharedPreference
            key: "skipAlert" // key to SharedPreference
        });
    }*/
    console.log('registrando');
    try{
        PushNotification.createChannel(function(){console.log('se creo el canal silencioso');},
        function(){console.log('fallo crear el canal silencioso');},
        {id: 'silencioso',
        description: 'canal de prueba1',
        importance: 1,
        vibration: false,
        visibility:-1});

        PushNotification.createChannel(function(){console.log('se creo el canal normal');},
        function(){console.log('fallo crear el canal normal');},
        {id: 'PushPluginChannel',
        description: 'canal normal1',
        importance: 5,
        vibration: true});

        var push = PushNotification.init({
            android: {
                senderID: IDPUSH,
                //badge: true,
                //sound: true,
                //vibrate: true
            },
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            },
            windows: {},
        });

        /*var push = PushNotification.init({
            android: {
                senderID: IDPUSH,
                //forceShow: "true",
                badge: true,
                sound: true,
                vibrate: true
                //clearNotifications: true
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            }
        });*/

        push.on('registration', function(data) {
            var tipo = null;
            console.log('registration');
            console.log(JSON.stringify(data));
            if (data.registrationId.length > 0){

                var ua = navigator.userAgent;
                if( ua.indexOf("Android") >= 0 ) {tipo = 'android';}else if( ua.indexOf("iPhone") >= 0 ){tipo = 'ios';}
                var deviceToken = data.registrationId;
                console.log(tipo);
                console.log(localStorage['device']+' != '+deviceToken+' && '+localStorage['idusuario']);
                if(localStorage['device'] != deviceToken && localStorage['idusuario']){
                    var y = '{ "userid" : ["' + localStorage['idusuario'] + '"] , "token" : ["' + localStorage['token'] + '"], "device" : ["'+deviceToken+'"], "tipo" : ["'+tipo+'"]}';
                    var url = direccionservidor + "web/app.php/userapi/setDevice";
                    console.log(y);
                    var objeto = $.ajax({
                        type: "POST",
                        url: url,
                        data: y,
                        cache: false,
                        success: function(){
                            var json = JSON.parse(objeto.responseText);
                            console.log(json);
                            if(json.msg == "correcto"){
                                localStorage['device'] = deviceToken;
                            }
                            else{
                                alert(json.msg);
                            }
                        },
                        error: function(){
                            alert("error al insertar la apikey en el servidor:");
                        }
                    });
                }else{
                    console.log('no entro');
                }
            }
        });

        push.on('notification', function (data) {
            console.log(JSON.stringify(data));
            var ua = navigator.userAgent;

            if (data.additionalData.android_channel_id != 'silencioso') {
                console.log('notificacion ruidosa, additional tipo: '+data.additionalData.tipo);
                if (ua.indexOf("Android") >= 0) {
                    //si es, y el caso el inverso
                    if (data.additionalData.foreground == false) {
                        //cordova.plugins.backgroundMode.wakeUp();
                        //cordova.plugins.backgroundMode.unlock();
                        //cordova.backgroundapp.show();
                        //cordova.plugins.backgroundMode.moveToForeground();
                    } else {
                        navigator.notification.vibrate(1000);
                    }
                }
                var mensaje = data.message;
                console.log('mensaje del push: '+mensaje);
                if (data.additionalData.tipo=='informacion') {
                    console.log('se recibio un push de info');
                }
                }else{
                console.log('se recibio notificacion silenciosa');
                console.log('additional tipo: '+data.additionalData.tipo);
                if (data.additionalData.tipo=='datosconductor') {
                    parar();
                    notif_actualizarLlegada(data.additionalData.datosconductor,data.additionalData.id);
                }else{
                    var mensaje = data.message;
                    console.log('mensaje del push: '+mensaje);
                    if (data.additionalData.id) {
                        var id = data.additionalData.id;
                    } if (data.additionalData.conductor) {
                        var conductor = data.additionalData.conductor;
                    } if (data.additionalData.celular) {
                        var celular = data.additionalData.celular;
                    } if (data.additionalData.rutadestino) {
                        var rutadestino = data.additionalData.rutadestino;
                    } if (data.additionalData.rutallegada) {
                        var rutallegada = data.additionalData.rutallegada;
                    } if (data.additionalData.placa) {
                        var placa = data.additionalData.placa;
                    } if (data.additionalData.dirllegada) {
                        var dirllegada = data.additionalData.dirllegada;
                    } if (data.additionalData.dirdestino) {
                        var dirdestino = data.additionalData.dirdestino;
                    } if (data.additionalData.fuec) {
                        var fuec = data.additionalData.fuec;
                    }
                    var titulo = data.title;
                    var tipo = data.additionalData.tipo;

                    //alert(JSON.stringify(data));
                    if (titulo === undefined)
                        titulo = data.additionalData.titulo;
                    if (dirdestino != null && dirllegada != null) {
                        dirdestinoPush = dirdestino;
                        dirllegadaPush = dirllegada;
                    }
                    mostrarpush(mensaje, titulo, id, tipo, conductor, celular, rutadestino, rutallegada, placa, dirllegadaPush, dirdestinoPush, fuec);
                }
            }

            push.finish(function () {                              //cordova.plugins.backgroundMode.moveToForeground();
                console.log('push exitoso');
            }, function () {
                console.log("algo salio mal para el push");
            }, '1');
        });

        push.on('error', function(e){
            // e.message
            alert(e.message);
        });
    }catch(errr){
        console.log("error registro : "+errr);
    }
}

function notif_actualizarLlegada(datosimportantes,idrutis){
    console.log('entro a notif_actualizarLlegada');
    if (idrutis!=null) {
        localStorage.setItem('idrutanotificado',idrutis);
    }
    $('#segundos_rest').text(datosimportantes.valor);
    $('#tiempo_cheto').css('display','block');
    $('#tiempo_rest').css('display','none');
    $('#tiempo_rest').text(datosimportantes.texto);
    $('#tiempo_cheto').show();
    if (!localStorage.conductornotificado || localStorage.conductornotificado==null || localStorage.conductornotificado==undefined) {
        $.mobile.changePage( "#page-seguimiento", { transition: "slide"});
        localStorage.ubicacion='seguimiento';
        createMapaant();
        polilinea=null;
        //horaRecogida();
    }
    console.log('ls.conductornotificado= '+localStorage.conductornotificado);
    if (mapa){
        servicios_seguimientoC(datosimportantes.posicion);
        if (polilinea==null) {
            servicios_obtenerPoli(idrutis);
        }

        //createMapaant();
        //horaRecogida();
    }
    var demora_seg=parseInt(datosimportantes.valor);
    var demora_min=demora_seg/60;
    var demora_h=demora_min/60;
    horasR=Math.floor(demora_h);
    var min_rest=demora_min-(horasR*60);
    minutosR=Math.round(min_rest);
    control=setInterval(cronometro,6);
}

function obtenerData(){
    var data = {"title":"Nuevo Servicio","sound":"default","additionalData":{"gcm.notification.flag":"1","gcm.notification.force-start":"1","gcm.notification.id":"10","gcm.notification.content-available":"1","gcm.message_id":"0:1513109071691253%485be051485be051","foreground":true,"gcm.notification.vibrate":"1","gcm.notification.tipo":"nuevoservicio","coldstart":false},"message":"Se le ha asignado una nueva ruta (RUTA1), desea aceptarla"};
    console.log(data);
    console.log(data.additionalData["gcm.notification.tipo"]);
}

function successHandler(result){
    console.log('result = ' + result);
}

function errorHandler(error){
    console.log('error = ' + error);
}

function tokenHandler (result){
    var deviceToken = result;
    if(localStorage['device'] != deviceToken){
        var y = '{ "userid" : ["' + localStorage['idusuario'] + '"] , "token" : ["' + localStorage['token'] + '"], "device" : ["'+deviceToken+'"]}';
        var url = direccionservidor + "web/app.php/userapi/setDevice";
        try{
            var objeto = $.ajax({
                type: "POST",
                url: url,
                data: y,
                cache: false,
                success: function(){
                    var json = JSON.parse(objeto.responseText);
                    if(json.msg == "correcto"){
                        localStorage['device'] = deviceToken;
                    }else{
                        alert(json.msg);
                    }
                },
                error: function(){
                    alert("error al conectarse al servidor");
                }
            });
        }
        catch(err){
            alert(err);
        }
    }
}

// iOS
function onNotificationAPN (event){
    if ( event.alert ){
        navigator.notification.alert(event.alert);
    }

    if ( event.sound ){
        var snd = new Media(event.sound);
        snd.play();
    }

    if ( event.badge ){
        pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
    }
}

// Android
function onNotification(e){
    var id;
    var claase;
    var mensaje;
    console.log('entro a una funcion rara onNotificacion(e)');
    switch( e.event ){
        case 'registered':
            if ( e.regid.length > 0 ){
                var deviceToken = e.regid;
                if(localStorage['device'] != deviceToken){
                    var y = '{ "userid" : ["' + localStorage['idusuario'] + '"] , "token" : ["' + localStorage['token'] + '"], "device" : ["'+deviceToken+'"]}';
                    var url = direccionservidor + "web/app.php/userapi/setDevice";
                    try{
                        var objeto = $.ajax({
                            type: "POST",
                            url: url,
                            data: y,
                            cache: false,
                            success: function(){
                                var json = JSON.parse(objeto.responseText);
                                if(json.msg == "correcto"){
                                    localStorage['device'] = deviceToken;
                                }else{
                                    alert(json.msg);
                                }
                            },
                            error: function(){
                                alert("error al conectarse al servidor");
                            }
                        });
                    }
                    catch(err){
                        alert(err);
                    }
                }
            }
        break;
        case 'message':
            mensaje = e.payload.message;
            id = e.payload.data.id;
            titulo = e.payload.data.title;

            if ( e.foreground ){
                //var soundfile = e.soundname;
                //var my_media = new Media("/android_asset/www/"+ soundfile);
                //my_media.play();
                //navigator.notification.alert("Aplicacion en uso", null, nombreaplicacion, "Aceptar");
                mostrarpush(mensaje, titulo, id);
            }else{
                if ( e.coldstart ){
                    //navigator.notification.alert('COLDSTART NOTIFICATION', null, nombreaplicacion, "Aceptar");
                    setTimeout(function(){ mostrarpush(mensaje, titulo, id); }, 6000);
                }else{
                    //navigator.notification.alert('BACKGROUND NOTIFICATION', null, nombreaplicacion, "Aceptar");
                    mostrarpush(mensaje, titulo, id);
                }
            }
            //Only works for GCM
            //navigator.notification.alert('MESSAGE -> MSGCNT: ' + e.payload.msgcnt, null, nombreaplicacion, "Aceptar");

            //navigator.notification.alert('Data => id:' + e.payload.data.id + ", title: " + e.payload.data.title, null, nombreaplicacion, "Aceptar");
        break;
        case 'error':
            alert('ERROR -> MSG:' + e.msg);
        break;

        default:
            alert('EVENT -> Desconocido, un evento fue recibido y no sabemos que es');
        break;
    }
}

function mostrarpush(mensaje, titulo, id, tipo, conductor, celular,rutadestino, rutallegada, placa, dirllegada, dirdestino,fuec){
    console.log(tipo);
    //alert('mensaje '+mensaje+' titulo '+titulo+' tipo '+tipo+' id '+id+'destino'+dirdestino+' llegada'+dirllegada);
    switch(tipo){
        case 'notificadoconductor':
            alerta(mensaje,titulo,tipo,id);
            //navigator.notification.alert(mensaje,callback, titulo,'Aceptar');
            break;
        case 'pushprueba'://si llega un push de prueba
            console.log('llego un push de prueba en mostrarpush');
            alerta(mensaje,titulo,tipo,id);
            break;
        case 'avisollegada':
            alerta(mensaje, titulo, tipo, id, conductor, celular,rutadestino, rutallegada, placa, dirllegadaPush, dirdestinoPush);
            //navigator.notification.confirm(mensaje,callback, titulo,'Aceptar');
            break;
        case 'primerAviso':
            alerta(mensaje, titulo, tipo, id, conductor, celular,rutadestino, rutallegada, placa, dirllegadaPush, dirdestinoPush);
            //navigator.notification.confirm(mensaje,callback, titulo,'Aceptar');
            break;
        case 'llegadaconductor':
            alerta(mensaje,titulo,tipo,id, conductor, celular,rutadestino, rutallegada, placa, dirllegadaPush, dirdestinoPush, fuec);
            break;
        case 'llegadaoficina':
            alerta(mensaje,titulo,tipo,id,conductor,celular);
            $('#tiempo_cheto').css('display','none');
            localStorage.removeItem('idrutanotificado');
            parar();
            break;
        case 'QRexitoso':
            alerta(mensaje,titulo,tipo,id);
            $('#tiempo_cheto').css('display','none');
            parar();
            break;
        case 'nuevoservicio':
            //alert('nuevo servicio '+ mensaje+' '+titulo+' '+tipo);
            alerta(mensaje,titulo,tipo,id,null,null,null,null,null,null,null,fuec);
            break;
        case 'servicioaceptado': //es cuando el conductor recibió notificación de la ruta asignada, llega en pasajero
            //alert(cuerpo,titulo,tipo,id,conductor,celular,rutadestino,rutallegada,placa,dirllegadaPush,dirdestinoPush);
            var cuerpo = mensaje+'\n'+'Ruta de Origen: '+rutadestino+'\n'+'Ruta de destino: '+rutallegada+'\n'+'Nombre del conductor: '+conductor+'\n'+'Celular: '+celular+'\n'+'Placa: '+placa;
            alerta(cuerpo,titulo,tipo,id,conductor,celular,rutadestino,rutallegada,placa,dirllegadaPush,dirdestinoPush);
            break;
        case 'nuevapromocion':
            alerta(mensaje,titulo,tipo,id);
            break;
    }


    //Llegada
    if(titulo == "ll"){
      navigator.notification.vibrate(1500);
      navigator.notification.alert(
        mensaje,
        null,
        nombreaplicacion,
        'Aceptar'
      );
    }

    //pago efectivo
    if(titulo == "pe"){
      navigator.notification.vibrate(1500);
      navigator.notification.confirm(
          "Gracias por usar " + nombreaplicacion + ", Te invitamos a calificar nuestro servicio",
          function(button){
            if(button == 1){
              var dd = new Array();
              dd['reserva'] = localStorage['reservaactiva'];
              mostrarpagina("factura", dd);
              $('#codigomovip').dialog("close");
            }else{
              $('.btn_menu').toggleClass('usuario');
              mostrarpagina("reserva");
              $('#codigomovip').dialog("close");
            }
          },
          nombreaplicacion,
          'Aceptar,Ahora no'
        );
      /*navigator.notification.alert(
        mensaje,
        null,
        nombreaplicacion,
        'Aceptar'
      );
      $('.btn_menu').toggleClass('usuario');
      mostrarpagina("reserva", dd);*/
    }

    //Inicio de servicio
    if(titulo == "ic"){

      idreservaglobal=0;
      reciveinfo=false;
      clearInterval(progressubicacion);
      navigator.notification.vibrate(1500);
      navigator.notification.alert(
        mensaje,
        null,
        nombreaplicacion,
        'Aceptar'
      );
    }

    //fin servicio
    if(titulo == "fc"){
      idreservaglobal=0;
      reciveinfo=false;
      clearInterval(progressubicacion);
      navigator.notification.vibrate(1500);
      navigator.notification.alert(
        mensaje,
        null,
        nombreaplicacion,
        'Aceptar'
      );
      var dd = new Array();
      dd["reserva"] = id;
      mostrarpagina("fin", dd);
    }

    // Push conductor

    //nuevo pedido
    if(titulo == "np"){
      var date = new Date();
      horanp=date.getTime();
      timeacept=true;
      idreservatoacept=id;
      navigator.notification.vibrate(1500);
      navigator.notification.confirm(
      mensaje,
      function(button){
          if(button == 1){
              var dd = new Array();
              dd["reserva"] = id;
              mostrarpagina('pedido', dd);
              //navigator.notification.alert("El tiempo de espera superó los 15 segundos este pedido fue asignado o otro Transportman" , null, "MOVIP", "Aceptar");
        }else if(button == 2){
            aceptarserviciodirecto(id);
        }
      },
      "Nuevo Servicio",
      'Ver,Aceptar,Cancelar'
      );
    }
    //nuevo pedido web
    if(titulo == "nps"){

      var date = new Date();
      horanp=date.getTime();
      timeacept=true;
      idreservatoacept=id;
      navigator.notification.vibrate(1500);
     // navigator.notification.alert(mensaje, function(){ enviocorreo(id); }, nombreaplicacion, "Aceptar");
        navigator.notification.confirm(
      mensaje,
      function(button){

        if(button == 1){
            aceptarserviciodirecto(id);
        }else if (button == 2){
         rechazarservicio(id);
        }
      },
      "Nuevo Servicio",
      'Aceptar,Rechazar'
      );
    }

    if(titulo == "swa"){
      var date = new Date();
      horanp=date.getTime();
      timeacept=true;
      idreservatoacept=id;
      navigator.notification.vibrate(1500);
      navigator.notification.confirm(
      mensaje,
      function(button){
        if(button == 1){
            aceptarserviciodirecto(id);
        }else if(button == 2){
            rechazarservicio(id);
        }
      },
      "Nuevo Servicio",
      'Aceptar,Rechazar'
      );
    }
    //servicio asignado
    if(titulo=="sa"){
        navigator.notification.alert(
          mensaje,
          null,
          nombreaplicacion,
          'Aceptar'
          );
        var dd = new Array();
        dd["reserva"] = id;
        mostrarpagina('pedido', dd);
    }
    //pedido cancelado
    if(titulo == "pc"){
      navigator.notification.vibrate(1500);
      navigator.notification.alert(
        mensaje,
        null,
        nombreaplicacion,
        'Aceptar'
      );
      var dd = new Array();
      dd["reserva"] = id;
      mostrarpagina("cancelado", dd);
    }
    //push vencimiento documentos
    if(titulo == "vd"){
        navigator.notification.vibrate(1500);
        navigator.notification.alert(
          mensaje,
          null,
          nombreaplicacion,
          'Aceptar'
        );
        mostrarpagina("documentosporvencer", null, id);
    }
    //push combinado
    if(titulo == "chat"){
        if(localStorage['rol'] == 3){
            if(id == $.get('id') && $.mobile.activePage.data('url') == 'chat'){
              navigator.notification.vibrate(1500);
              var mensajes = document.getElementById("mensajesusuario");
              var html = "<div class='el'> " + mensaje + "<div>";
              $(mensajes).append(html);
              $('html, body').animate({ scrollTop: $(document).height() }, 1500);
            }else{
              navigator.notification.confirm(chatuser+" dice:  "+chatmensaje,
                function(button){
                  if(button == 1)
                  {
                    var dd = new Array();
                    dd["reserva"] = id;
                    mostrarpagina('chat',dd);
                  }
                },
                "Nuevo mensaje",
                'Ver,Mas tarde'
              );
            }
        }else{
            if(id == $.get('id') && $.mobile.activePage.data('url') == 'chatc'){
                navigator.notification.vibrate(1500);
                var mensajes = document.getElementById("mensajesconductor");
                var html = "<div class='el'> " + mensaje + "<div>";
                $(mensajes).append(html);
                $('html, body').animate({ scrollTop: $(document).height() }, 1500);
            }else{
                navigator.notification.confirm(
                    chatconductor+" dice:  "+chatmensaje,
                    function(button){
                        if(button == 1){
                            var dd = new Array();
                            dd["reserva"] = id;
                            mostrarpagina('chatc',dd);
                        }
                    },
                "Nuevo mensaje",
                'Ver,Mas tarde'
                );
            }
        }
    }

    //envio de noticias
    if(titulo == "info"){
      navigator.notification.vibrate(1500);
      navigator.notification.alert(
        mensaje,
        null,
        nombreaplicacion,
        'Aceptar'
      );
    }

    if(titulo == "dpv"){
      navigator.notification.vibrate(1500);
      navigator.notification.confirm(
        mensaje,
        function(button){
          if(button == 1)
          {
            var dd = new Array();
            dd["reserva"] = id;
            mostrarpagina('documentosporvencer', dd);
          }
        },
        "Documentos a vencer",
        'Ver,Aceptar'
      );
    }

    if(titulo == "fac"){
        navigator.notification.vibrate(1500);
        navigator.notification.confirm(
            mensaje,
            function(button){
              if(button == 1)
              {
                mostrarpagina('reserva');
              }
            },
            nombreaplicacion,
            'Terminar'
        );
        $('.btn_menu').addClass('usuario');

        $('.btn_menu.usuario').click(function(){
            $('.section_menu.usuario').toggleClass('active');
            $('.section_content').toggleClass('to_right');
        });
    }
}
