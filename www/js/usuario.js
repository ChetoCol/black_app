var direccionservidor = "https://www.blacktransport.co/";
var nombreaplicacion = 'BLACK';

function detallePromocion(indice){
 /*var descripcion = $('#detalle_promo'+indice).text();
  var descuento = $('#descuento_promo'+indice).text();
  var codigo = $('#codigo_promo'+indice).text();
  var imagen = $('#img_promo'+indice).attr('src');
  localStorage['promocion'] = descripcion+"|"+descuento+"|"+codigo+"|"+imagen;*/
  $('#descipcion_promocion').text($('#detalle_promo'+indice).text());
  $('#imagen_promocion').attr('src',$('#img_promo'+indice).attr('src'));
  $('#codigo_promocion').text($('#codigo_promo'+indice).text());
  //localStorage['promocion'] = 'entrega';
  localStorage.ubicacion = 'detallepromo';
  $.mobile.changePage( "#detalle_promociones", { transition: "slide"});

}
function obtenerDatosUsuario(tipo){
    $('.cargando').show();
    var y = '{"userid" : [ "'+localStorage['idusuario']+'"] , "token" : ["'+localStorage['token']+'"] }';
    var url = direccionservidor + "web/app.php/userapi/datosusuario";
    //console.log(y);
    var objeto=$.ajax({
      type: "POST",
      url: url,
      data: y,
      cache: false,
      success: function()
      {
        $('.cargando').hide();
        var json = JSON.parse(objeto.responseText);
        console.log(objeto.responseText);
        if(json.msg == "")
        {
            if(tipo){
                localStorage.telefono = json.telefono;
                //alert(localStorage.telefono);
            }else{
                document.getElementById("nombreusuario").value = json.nombre;
                document.getElementById("emailusuario").value = json.email;
                document.getElementById("loginusuario").value = json.login;
                document.getElementById("numerotelefono").value = json.telefono;
                document.getElementById("pass1usuario").value = '';
                document.getElementById("pass2usuario").value = '';
                if(localStorage.rol == '4'){
                  $("#actualizar_tarjeta").show();
                }
            }
        }
        else
        {
            console.log('mostrará mapa');
            alerta(json.msg,nombreaplicacion);
        }
      },
      error: function()
      {
        $('.cargando').hide();
        alerta('Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente','Error');
      }
    });
}
function valCodigoProm(codigo,tipo,valor){
    $('.cargando').show();
    var val = null;
    var texto = null;
    var codigotexto = null;
    var y = '{"userid" : "'+localStorage['idusuario']+'" , "bandera" : "2" , "codigo" : "'+codigo+'" }';
    var url = direccionservidor + "web/app.php/movilapi/codigoPromociones";
    console.log(y);
    var objeto=$.ajax({
      type: "POST",
      url: url,
      data: y,
      cache: false,
      success: function()
      {
            $('.cargando').hide();
            var json = JSON.parse(objeto.responseText);
            console.log(json);
            if(json.error == ''){
                localStorage.idpromocion = json.idpromocion;
                    console.log('localStorage.valpagotemp null');
                    var valorp = $("#valviajes_billetera").val().split('.');
                    var valp = valorp[0]+valorp[1];
                    val = valp;
                console.log(val);
                console.log(valor);
                console.log((valor*parseInt(json.descuento))/100);
                if(parseInt(val) != (valor*parseInt(json.descuento))/100){
                    console.log('val diferente');
                    var total = (parseInt(val)*parseInt(json.descuento))/100;
                    console.log(parseInt(total));
                }else{
                    console.log('val igual');
                    var total = val;
                }

                if(tipo == 'tc'){
                    texto = 'val_pago_credito';
                    codigotexto = 'codigo_promocional_tc';
                }else if(tipo == 'ef'){
                    texto = 'val_pago';
                    codigotexto = 'codigo_promocional';
                }else{
                    texto = 'valviajes_billetera';
                    codigotexto = 'codigo_promocional';
                }
                $('#'+texto).text('$ '+format(total.toString()));
                $('#'+texto).css({color: "green"});
                //$('#'+texto).attr('disabled','disabled');
                console.log(codigo);
                localStorage.descuento = json.descuento;
                localStorage.codigoPromo = codigo;
                localStorage.valpagotemp = total;
                //localStorage.tipotarjetac = '1';
                alerta('Codigo exitoso, desea realizar el pago. ',nombreaplicacion,"codigoexitoso");
            }else{
                localStorage.valpagotemp = $('#valviajes_billetera').val();
                alerta('El codigo es invalido, intente de nuevo',nombreaplicacion,'Aceptar');
            }

      },
      error: function()
      {
        $('.cargando').hide();
        alerta('Error de conexion, valide e intente de nuevo. ',nombreaplicacion,'Aceptar');
        console.log('Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente');
      }
    });
}
function obtenerDatosPromociones(tipo){
    $('ul.justList').empty();
    $('.cargando').show();
    var y = '{"userid" : "'+localStorage['idusuario']+'" , "bandera" : "1" }';
    var url = direccionservidor + "web/app.php/movilapi/codigoPromociones";
    console.log(y);
    var objeto=$.ajax({
      type: "POST",
      url: url,
      data: y,
      cache: false,
      success: function()
      {
        $('.cargando').hide();
        var json = JSON.parse(objeto.responseText);
        if(json.length==0)
        {
          $("#notPromocion").css("display", "block");
        }
        else
        {
          for(var i=0;i<json.length;i++)
          {
            var text = '<a onclick="detallePromocion('+i+')"><img id="img_promo'+[i]+'" style="margin:10px;" src="'+json[i]['path']+'"><h2 id="detalle_promo'+[i]+'">'+json[i]['descripcion']+'</h2><p style="display:none;" id="descuento_promo'+[i]+'">'+json[i]['descuento']+'</p><p style="display:none;" id="codigo_promo'+[i]+'">'+json[i]['codigo']+'</p></a><br>'
            $('<li />', {
                      html: text
                  }).appendTo('ul.justList');
            $('ul.justList').listview('refresh');
          }
          //abrirPopup('popupDatos');
        }localStorage.ubicacion = 'promociones';
        if(tipo == 'muenu'){
            localStorage.accionUbicacion = 'menu';
        }else{
           localStorage.accionUbicacion = 'normal';
        }
      },
      error: function()
      {
        $('.cargando').hide();
        alerta('Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente','Error');
      }
    });
}
function actualizardatos()
{
  var pass1 = document.getElementById("pass1usuario").value;
  var pass2 = document.getElementById("pass2usuario").value;
  var nombre = document.getElementById("nombreusuario").value;
  var email = document.getElementById("emailusuario").value;
  var login = document.getElementById("loginusuario").value;
  var telefono= document.getElementById("numerotelefono").value;

  if(nombre == "" || email == "" || login == "" || telefono=="")
  {
    alerta('No debes dejar campos en blanco',nombreaplicacion);
    //navigator.notification.alert("No debes dejar campos en blanco");
  }
  else
  {

    if(pass1 == pass2)
    {
      $('.cargando').show();
      var y = '{"userid" : [ "'+localStorage['idusuario']+'"] , "token" : ["'+localStorage['token']+'"], "nombre" : ["'+nombre+'"], "email" : ["'+email+'"], "login" : ["'+login+'"], "password" : ["'+pass1+'"],"telefono": ["'+telefono+'"]}';
      var url = direccionservidor + "web/app.php/userapi/actualizardatos";
      var objeto=$.ajax({
        type: "POST",
        url: url,
        data: y,
        cache: false,
        success: function()
        {
          $('.cargando').hide();
          var json = JSON.parse(objeto.responseText);
          if(json.msg == "correcto")
          {
            alerta('Cambios realizados con exito',nombreaplicacion,'Aceptar');
            localStorage.telefono = telefono;
            //mostrarpagina("homec");
          }
          else
          {
              alerta(json.msg,nombreaplicacion,'Aceptar');
          }
        },
        error: function()
        {
            alerta('Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente',nombreaplicacion,'Aceptar');
            $('.cargando').hide();

        }
      });
    }
    else
    {
        alerta('Las Contrase\u00F1as no coinciden',nombreaplicacion,'Aceptar');
      //navigator.notification.alert("Las Contrase\u00F1as no coinciden", null, nombreaplicacion, "Aceptar");
    }
  }
}
function tarjetaC()
{
    localStorage.ubicacion = 'tarjetacredito';
    document.getElementById('payer_id').value = localStorage['idusuario'];
    if(localStorage['tt'] == "" || localStorage['tt'] == undefined || localStorage['tt'] == null)
    {
        $('#volvertarjeta').hide();
        $('#volvertarjetaingreso').show();

     // $('#btn_ingresar_tarjeta').show();
    }
    else
    {
        $('#volvertarjeta').show();
        $('#volvertarjetaingreso').hide();
        document.getElementById("valorartarjeta").value ="1";
        //$('#btn_ingresar_tarjeta').hide();
    }
    //$('.cargando').toggleClass('active');
}
function envioComentarios(tipo){
    var comentario = null;
    if(tipo == 'usuario'){
        comentario = $("#comentario_quepaso_usuario").val();
    }else if(tipo == 'conductor'){
        comentario = $("#comentario_quepaso").val();
    }
    var url = direccionservidor + "web/app.php/movilapi/envioComentarios";
    var datos = '{"idusuario" : "'+localStorage.idusuario+'","comentario" : "'+comentario+'"}';
        var objeto=$.ajax({
            type: "POST",
            url: url,
            data: datos,
            cache: false,
            success: function()
            {
                var json = JSON.parse(objeto.responseText);
                if(json.error == "")
                {
                    alerta("Gracias por comunicarte, tu opinion es muy importante para nosotros",nombreaplicacion,"Aceptar");
                }
            },
            error: function()
            {
                alerta("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente",nombreaplicacion,"Aceptar");
                //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
                $('.cargando').hide();

            }
        });
}
