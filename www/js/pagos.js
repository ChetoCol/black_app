var nombreaplicacion = 'BLACK';
var direccionservidor = "https://www.eurekadms.co/Black/";
var horasentrada = null;
function cambioInput(tipo,caso){
    var diadespues = null;
    var hoy = new Date();//fecha de hoy
    var manianas = hoy.setTime(hoy.getTime() + (1*24*60*60*1000))
    var maniana = new Date(manianas);//fecha de mañana
    if (maniana.getDate() < 10){ diadespues = '0'+maniana.getDate()}else{diadespues = maniana.getDate()}
    var despues =  hoy.getFullYear()+"-" + (hoy.getMonth()+1) + "-" +diadespues;
    if(tipo == 'focus' && caso == 'mayor'){  
        console.log(despues);
        $("#diarecogidamayor").attr('type','date');
        $('#diarecogidamayor').attr('min',despues); 
        $('#diarecogidamayor').click();
    }else if(tipo == 'focus' && caso == 'menor'){
        $('#diarecogidamenor').attr('min',despues);
        $("#diarecogidamenor").attr('type','date');
        $('#diarecogidamenor').click();
    }else if(tipo == 'blur' && caso == 'mayor'){
//        $('#diarecogidamayor').val('');        
        $("#diarecogidamayor").attr('type','text');        
    }else if(tipo == 'blur' && caso == 'menor'){
//        $('#diarecogidamenor').val('');
        $("#diarecogidamenor").attr('type','text');
    }
}

//esta funcion llena el select horaentrada con los intervalos de hora que tiene
function obtenerValServicio(tipo,val){
    
    var url = direccionservidor + "web/app.php/pagosapi/pagacodigo";
    var objeto=$.ajax({
        type: "POST",
        url: url,
        cache: false,
        success: function()
        {
            var json = JSON.parse(objeto.responseText);
            console.log(json);
            $("#horaentrada").empty();
            $("#hora_entrega_edit").empty();
            //localStorage.valpago = json.valor;
            localStorage.valservicio = json.valor;
            horasentrada = json.horarios;
            $("#hora_entrega_edit").append('<option value="0">Hora ingreso a trabajo</option>');
            $("#horaentrada").append('<option value="0">Hora ingreso a trabajo</option>');
            if(tipo == 'inicio'){
                for(var i = 0; i<json.horarios.length; i++){
                    var horainicio = json.horarios[i]['horainicio'];
                    var horafin = json.horarios[i]['horafinal'];
                    var idhora = json.horarios[i]['id'];
                    $("#horaentrada").append("<option value='"+idhora+"'>"+horainicio+" / "+horafin+"</option>");
                    $("#hora_entrega_edit").append("<option value='"+idhora+"'>"+horainicio+" / "+horafin+"</option>");
                    $('.cargando').hide();
                }
            }else{  
                calcViajes(val);
            }
        },
        error: function()
        {
            alerta('Error de conexion 2, valide he intente nuevamente.',nombreaplicacion,"Aceptar");
            console.log("Ocurri\u00f3 un error obtener el valor del servicio");
            //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
            $('.cargando').hide();

        }
    });
}
function calcViajes(val){
    //var total = val*localStorage.valpago;
    var total = val*localStorage.valservicio;
    $("#valviajes_billetera").val(format(total.toString()));
}
function cancelarInscripcionTarjeta(){
    if(localStorage.tarjetaPendiente != undefined){
        localStorage.ubicacion = 'pagos'; 
        $('#medio').val(0).change();
    }else{
        localStorage.ubicacion = 'perfil';
        valTelefonoUser();
    }
}
function valTelefonoUser(tipo){
    console.log(tipo);
    if(tipo == 'reservar'){
        if(localStorage.telefono == 0 || localStorage.telefono == undefined || localStorage.telefono == null || localStorage.telefono =="" || localStorage.telefono == 'null'){
            console.log('tarjetacredito');       
            alerta("Por favor actualice su número telefónico para brindarle un mejor servicio", nombreaplicacion, "actualizarperfil");
        }else{
            abrirReserva();
        }
    }else{
        if(localStorage.telefono == 0 || localStorage.telefono == undefined || localStorage.telefono == null || localStorage.telefono =="" || localStorage.telefono == 'null'){
            console.log('tarjetacredito');       
            alerta("Por favor actualice su número telefónico para brindarle un mejor servicio", nombreaplicacion, "actualizarperfil");
        }
    }
    
}
function validarpagoefectivo()
{          
    if(localStorage.tarjetaPendiente != undefined){
        localStorage.ubicacion = 'pagos';
        history.back();
        $('#medio').val(2).change(); //trigger a change instead of click
        localStorage.removeItem('tarjetaPendiente');
    }else{
        localStorage['tt']="null";
        alerta("Tus servicios se pagar\u00E1n en efectivo",nombreaplicacion,'pagoefectivo');
    }
//  navigator.notification.alert("Tus servicios se pagar\u00E1n en efectivo", function(){
//        mostrarpagina("reserva");
//      }, nombreaplicacion, "Aceptar");

//      $('.btn_menu').toggleClass('usuario');
//
//      $('.btn_menu.usuario').click(function(){
//        $('.section_menu.usuario').toggleClass('active');
//        $('.section_content').toggleClass('to_right');
//        
//      });
     
}

function validarTarjeta()
{
  var number = document.getElementById("number").value;
  var name_card = document.getElementById("name_card").value;
  var payer_id = document.getElementById("payer_id").value;
  var exp_month = document.getElementById("exp_month").value;
  var exp_year = document.getElementById("exp_year").value;
  var method = document.getElementById("method").value;
  var cvc = document.getElementById("cvc").value;
  var documento = document.getElementById("document").value;
  var continuar = true;
  if(isNaN(documento) || documento <= 0 || documento.length <5 || documento.length >30)
  {
    alerta("Verifica el n\u00famero de tu documento", nombreaplicacion, "Aceptar");
    continuar = false;
  }

  if(method == 0)
  {
    alerta("Debes indicar la franquicia de tu tarjeta", nombreaplicacion, "Aceptar");
    continuar = false;
  }

  if(method == "AMEX")
  {
    if(cvc.length != 4 || isNaN(cvc))
    {
      alerta("El c\u00f3digo de seguridad(cvc) debe ser un n\u00famero de cuatro d\u00edgitos", nombreaplicacion, "Aceptar");
      continuar = false;
    }
  }
  else
  {
    if(cvc.length != 3 || isNaN(cvc))
    {
      alerta("El c\u00f3digo de seguridad(cvc) debe ser un n\u00famero de tres d\u00edgitos", nombreaplicacion, "Aceptar");
      continuar = false;
    }
  }

  var f = new Date();
  var mes = (f.getMonth() +1);
  var anio =  f.getFullYear();
  if(isNaN(exp_month) || (exp_month.length != 2 && exp_month.length != 1))
  {
    alerta("Verifica el mes de vencimiento", nombreaplicacion, "Aceptar");
    continuar = false;
  }
  else
  {
    if(isNaN(exp_year) || exp_year.length != 4)
    {
      alerta("Verifica el a\u00F1o de vencimiento", nombreaplicacion, "Aceptar");
      continuar = false;
    }
    else
    {
      if(exp_year < anio)
      {
        alerta("La tarjeta ingresada ya expir\u00f3", nombreaplicacion, "Aceptar");
        continuar = false;
      }
      else
      {
        if(exp_year == anio)
        {
          if(exp_month <= mes)
          {
            alerta("La tarjeta ingresada ya expir\u00f3", nombreaplicacion, "Aceptar");
            continuar = false;
          }
        }
      }
    }
  }

  if(isNaN(number))
  {
    alerta("Verifica el n\u00famero de la tarjeta", nombreaplicacion, "Aceptar");
    continuar = false;
  }
  else
  {
    if((method == "VISA" || method == "MASTERCARD") && number.length != 16)
    {
      alerta("Verifica el n\u00famero de la tarjeta", nombreaplicacion, "Aceptar");
      continuar = false;
    }
    if(method == "AMEX" && number.length != 15)
    {
      alerta("Verifica el n\u00famero de la tarjeta", nombreaplicacion, "Aceptar");
      continuar = false;
    }
    if(method == "DINERS" && number.length != 14)
    {
      alerta("Verifica el n\u00famero de la tarjeta", nombreaplicacion, "Aceptar");
      continuar = false;
    }
  }

  if(continuar)
  {
    $('.cargando').hide();

    localStorage['mt'] = method;
    var url = direccionservidor + "web/app.php/pagosapi/payutokentarjeta";
    var datos = '{"idusuario" : "'+localStorage.idusuario+'","number" : "'+number+'","method" : "'+method+'","name_card" : "'+name_card+'","payer_id" : "'+payer_id+'","exp_month" : "'+exp_month+'","exp_year" : "'+exp_year+'","cvc" : "'+cvc+'","documento" : "'+documento+'"}';
    console.log(datos) ;   
        var objeto=$.ajax({
        type: "POST",
        url: url,
        data: datos,
        cache: false,
        success: function()
        {
            var json = JSON.parse(objeto.responseText);
            console.log(json);
            if(json.error=="")
            {
                
              
                $("#number").val('');
                $("#name_card").val('');
                $("#payer_id").val('');
                $("exp_month").val('');
                $("#exp_year").val('');
                $("#method").val;
                $("cvc").val('');
                $("#document").val('');
                
              localStorage['tt'] = json.creditCardTokenId;
              localStorage.franquiciatc = method;
              localStorage.numerotc = number;
              localStorage.cedulatc = documento;
              localStorage.vencetc = exp_month+'/'+exp_year;
              localStorage.clientetc = name_card;
              alerta("Tus datos fueron guardados correctamente", 'tokenizado', "Aceptar");
              if(localStorage.tarjetaPendiente != undefined){
                  localStorage.ubicacion = 'pagos';
                  //alert(localStorage.ubicacion)
                  history.back();
                  $('#medio').val(1).change();
              }else{
                    localStorage.ubicacion = 'main';
                    $.mobile.changePage( "#reserva", { transition: "slice", changeHash: true }, true, true);
                    if(localStorage.telefono == 0 || localStorage.telefono == undefined || localStorage.telefono == null || localStorage.telefono =="" || localStorage.telefono == 'null'){
                        console.log('tarjetacredito');
                        alerta("Por favor actualice su numero telefonico para brindarle un mejor servicio", nombreaplicacion, "actualizarperfil");
                    }
                }
            }
        },
        error: function()
        {
            alerta("Error en la conexion, valide e intente nuevamente.",nombreaplicacion,"Aceptar");
            console.log("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente");
            //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
            $('.cargando').hide();

        }
    });
    
  }
}

function responseHandlertarjeta(response)
{
    $('.cargando').hide();

    if(response.error)
    {
        console.log(response.error);
        alerta(response.error, nombreaplicacion, "Aceptar");
    }
    else
    {
        var token = response.token;
        localStorage['tt'] = token;
        alerta("Tus datos fueron guardados correctamente", 'tokenizado', "Aceptar");
        if(document.getElementById("valorartarjeta").value == "0"){
//
//          $('.btn_menu').toggleClass('usuario');
//
//              $('.btn_menu.usuario').click(function(){
//                $('.section_menu.usuario').toggleClass('active');
//                $('.section_content').toggleClass('to_right');
//              });

        }
    }
}
function format(input)
{
    var num = input.replace(/\./g,'');
    if(!isNaN(num)){
    num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
    num = num.split('').reverse().join('').replace(/^[\.]/,'');
    input = num;
    }

    else{ alert('Solo se permiten numeros');
    input = input.replace(/[^\d\.]*/g,'');
    }
    console.log(input);
    return input;
}
function mediopago(){
  var x = document.getElementById("medio").value;
//  var valorp = $("#valviajes_billetera").val().split('.');
//  var val = valorp[0]+valorp[1];
//  console.log(val);
//  localStorage.valpagotemp = val.toString();
    console.log(x+' tt '+localStorage.tt);
  if(x == 0)
  {
     $("#pagoefectivo").css("display", "none");
     $("#pagotarjeta").css("display", "none");
  }
  else if(x == 1 && (localStorage.tt != 'null' && localStorage.tt != undefined))
  {
      console.log('11111 diferente y diferente');
    var url = direccionservidor + "web/app.php/pagosapi/comprobarTokenCard";
    var datos = '{"idusuario" : "'+localStorage.idusuario+'","tt" : "'+localStorage.tt+'", "idpago" : "'+localStorage.idpago+'"}';
    console.log(datos);
    var objeto=$.ajax({
        type: "POST",
        url: url,
        data: datos,
        cache: false,
        success: function()
        {
            var json = JSON.parse(objeto.responseText);
            console.log(json);
            if(json.error=="")
            {
              var selectobject=document.getElementById("tarjetaSelect");
              for(var i=1; i<selectobject.length; i++){
                 selectobject.remove(i);
              }

              $('#tarjetaSelect').append($('<option>',
              {
                  value: json.creditCardTokenId,
                  text : json.maskedNumber
              }));
              console.log(localStorage.valpagotemp);
              localStorage['method'] = json.paymentMethod;
              $("#pagoefectivo").css("display", "none");
              $("#pagotarjeta").css("display", "block"); 
              $('#val_pago_credito').text('$ '+format(localStorage.valpagotemp));
              
              //localStorage.valpagotemp = localStorage.valpago;
              localStorage.formapago = 'tc';
            }
        },
        error: function()
        {
            alerta("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente",nombreaplicacion,"Aceptar");
            //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
            $('.cargando').hide();

        }
    });
  }
  else if(x == 1 && (localStorage.tt == 'null' || localStorage.tt == undefined)){
      $.mobile.changePage( "#tarjeta", { transition: "slide", changeHash: true });
      localStorage.tarjetaPendiente = 'reservando';
      $("#medio").val(0).change();
  }
  else if(x==2)
  {
    $("#pagoefectivo").css("display", "block");
    $("#pagotarjeta").css("display", "none");
    console.log(localStorage.valpagotemp);
    console.log('$ '+format(localStorage.valpagotemp));
    $("#val_pago").text('$ '+format(localStorage.valpagotemp));
  }
}
function recargarBilletera(tipo){
    localStorage.ubicacion = 'pago';       
        console.log(tipo);
        if(!tipo){
            var codigo = $("#codigo_promocional").val();
            var valor = $("#valviajes_billetera").val();
            console.log(codigo+' recoge el codigo digitado');
            if(valor != '' || valor != 0){
                if(codigo != ''){
                    console.log('codigo diferente a vacio');
                    var valorp = $("#valviajes_billetera").val().split('.');
                    var val = valorp[0]+valorp[1];
                    valCodigoProm(codigo,null,val);
                }else{
                    console.log('codigo con informacion');
                    $.mobile.changePage( "#pagos", { transition: "slide"});
                    var valorp = $("#valviajes_billetera").val().split('.');
                    var valor = valorp[0]+valorp[1];
                    console.log(valor);
                    localStorage.valpagotemp = valor;
                    localStorage.valpago = valor;
                    console.log(localStorage.formapago);                
                    $("#val_pago_credito").text(valor);
                    $("#val_pago").text(valor);
                }     
            }else{
                alerta('El valor de la recarga no puede estar vacio',nombreaplicacion,'Aceptar');
            }
        }else{
            console.log('existe codigo en localstorage codigo');            
            $.mobile.changePage( "#pagos", { transition: "slide"});
//            var valorp = $("#valviajes_billetera").val().split('.');
//            var valor = valorp[0]+valorp[1];
//            localStorage.valpago = valor;
            console.log(valor);
            console.log(localStorage.formapago);  
            console.log(localStorage.valpagotemp);  
            $("#val_pago_credito").text(localStorage.valpagotemp);
            $("#val_pago").text(localStorage.valpagotemp);
        }
        $("#medio").val(0).change();
        $("#pagoefectivo").css("display", "none");
        $("#pagotarjeta").css("display", "none"); 
        $("#tarjetaSelect").val(0).change();
        $("#efectivoSelect").val(0).change();
}
function pagoTarjetaCredito(tipo){
    $('.cargando').show();
    var datos = null;
    if(tipo == 'recargar'){
        var tokencard = localStorage.tt;
        var valviajes = $("#valviajes_billetera").val();
        var franquicia = 'VISA';
        var codigopromo = '0';
        datos = '{"idusuario" : "'+localStorage.idusuario+'","metodo" : "1", "valpago" : "'+valviajes+'","codigo" : "'+codigopromo+'","tokencard" : "'+tokencard+'", "franquicia": "'+franquicia+'"}';
        envioPagoTarjetaCredito(datos);
    }else{
        var tarjeta = $("#tarjetaSelect").val();
        var codigopromo = null;
        if(localStorage.codigoPromo == 'null'){
            codigopromo = '0';
        }else{
            codigopromo = localStorage.codigoPromo;
        }
        if(tarjeta != 0){
            datos = '{"idusuario" : "'+localStorage.idusuario+'","idpago" : "'+localStorage.idpago+'","metodo" : "1", "valpago" : "'+localStorage.valpagotemp+'", "valpagosin" : "'+localStorage.valpago+'", "codigo" : "'+codigopromo+'", "idpromocion" : "'+localStorage.idpromocion+'","tokencard" : "'+localStorage.tt+'", "franquicia": "'+localStorage.method+'"}';
            envioPagoTarjetaCredito(datos);
        }else{
            alerta("Porfavor seleccione una tarjeta de credito de la lista.",nombreaplicacion,"Aceptar");
            $('.cargando').hide();
        }
    } 
}

function envioPagoTarjetaCredito(datos){
    var url = direccionservidor + "web/app.php/pagosapi/payucredito";    
    console.log(datos);
    var objeto=$.ajax({
        type: "POST",
        url: url,
        data: datos,
        cache: false,
        success: function()
        {
            var json = JSON.parse(objeto.responseText);
            console.log(json);
            if(json.error=="")
            {
                if(json.estado == "Aprovado"){                       
                    finalizarPago(json,'tc');
                }else{
                    alerta(json.estado+', intente denuevo con otra tarjeta de credito.',nombreaplicacion,"Aceptar");
                } 
            }
            $('.cargando').hide();
        },
        error: function()
        {
            alerta('Error de conexion, valide he intente nuevamente.',nombreaplicacion,"Aceptar");
            console.log("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente");
            //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
            $('.cargando').hide();

        }
    });
}

function finalizarPago(json,tipo){    
    if(localStorage.tipotarjetac == '1'){
        $('#url_pago').hide();
    }else{
        $('#url_pago').show();
    }
    $('#url_pago').attr('href', json.urlweb);            
    localStorage.ubicacion = 'estadopago';
    localStorage.codigoPromo = 'null';
    localStorage.valpagotemp = 'null';
    localStorage.descuento = 'null';
    localStorage.tipotarjetac = 'null';
    localStorage.idpromocion = 'null';
    localStorage.valpago = 'null';
    localStorage.removeItem('tarjetaPendiente');
    console.log(tipo);
    if(tipo == 'tc'){
        history.go(-2);
        $("#viajes_billetera").val('');
        $("#valviajes_billetera").val('');
        $("#codigo_promocional").val('');
        obtenerValServicio('inicio');
        saldoActual();
    }else{
        $.mobile.changePage("#pagos_estado", "slide", true, true);
        document.getElementById("estado_pago").innerHTML = json.estado;
        document.getElementById("orden_pago").innerHTML = "Orden # "+json.orderid;
    }
//    $("#horaentrada").val(0).change();
//    $("#diarecogidamayor").val('');
//    $("#diarecogidamaenor").val('');
//    $("#direccionllegada").val('');
//    
//    $("#latitudllegada").val('');
//    $("#longitudllegada").val('');
//    $("#mayorllegada").val('');
    //resetContinuarReserva();
}
function payuEfectivo(){
  $('.cargando').show();
  var metodo = document.getElementById("efectivoSelect").value;
  var url = direccionservidor + "web/app.php/pagosapi/payuefectivo";
//  datos = '{"idusuario" : "'+localStorage.idusuario+'","idpago" : "'+localStorage.idpago+'","metodo" : "1", "valpago" : "'+localStorage.valpagotemp+'", "valpagosin" : "'+localStorage.valpago+'", "codigo" : "'+codigopromo+'", "idpromocion" : "'+localStorage.idpromocion+'","tokencard" : "'+localStorage.tt+'", "franquicia": "'+localStorage.method+'"}';
  var datos = '{"idusuario" : "'+localStorage.idusuario+'","idpago" : "'+localStorage.idpago+'","metodo" : "'+metodo+'", "valpago" : "'+localStorage.valpagotemp+'", "valpagosin" : "'+localStorage.valpago+'", "codigo" : "'+localStorage.codigoPromo+'", "idpromocion" : "'+localStorage.idpromocion+'"}';
  console.log(datos);
    var objeto=$.ajax({
      type: "POST",
      url: url,
      data: datos,
      cache: false,
      success: function()
      {
            var json = JSON.parse(objeto.responseText);
            if(json.error=="")
            {
                $('.cargando').hide();
                $('#medio').val(0).change();
                finalizarPago(json);
            }
      },
      error: function()
      {
          alerta("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente",nombreaplicacion,"Aceptar");
          //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
          $('.cargando').hide();

      }
  });
}