var nombreaplicacion = 'Black';
var direccionservidor = "https://www.blacktransport.co/";
var iconBase = 'https://maps.google.com/mapfiles/ms/micons/';
var refreshIntervalId;
var nuevalat = '';
var nuevalong = '';
var us = '';
var interval = null;
var circulo = null;
var infowindow = null;
var mapa, markerYO, i;
var datos = null;
var conductorpush = null;
var celularpush = null;
var rutadestinopush = null;
var rutallegadapush = null;
var placapush = null;
var idrutapush = null;
var tipomapa = null;
var polilinea=null;//guarda la polilinea que el pasajero recibio de su ruta
var centerControlDiv = '';

//funcion que trae los servicios del usuario
function traerServiciosUsuario(){
    $('.cargando').show();
    var y = '{ "userid" : "'+localStorage['idusuario']+'", "bandera" : "pasajero" }';
    var url = direccionservidor + "web/app.php/movilapi/HistoralServicios";
    var objeto=$.ajax({
    type: "POST",
    url: url,
    data: y,
    cache: false,
    success: function(data){
        $('.cargando').hide();
        var json = JSON.parse(objeto.responseText);
        console.log(json);
    },
    error: function(){
        $('.cargando').hide();
        console.log("Ocurrio un error al traer los servicios del usuario", nombreaplicacion, "Aceptar");
      }
    });
}

//funcion que trae todos mis servicios
function traerServicios(){
    $('.cargando').show();
    //clearinitservicedata();
    var y = '{ "userid" : ["'+localStorage['idusuario']+'"], "token" : ["'+localStorage['token']+'"] }';
    var url = direccionservidor + "web/app.php/driverapi/cargaPedidos";
    var objeto=$.ajax({
    type: "POST",
    url: url,
    data: y,
    cache: false,
    success: function(){
        $('.cargando').hide();
        var json = JSON.parse(objeto.responseText);
        console.log(json.error);
        if(json.error == ""){
          //var status = json.status;
          //alert(localStorage.disponible);
            if(localStorage.disponible == '1'){
                $('#statusconductor').val(localStorage.disponible).slider("refresh");
                cambiostatus(1);
                localStorage.disponible = '2';
            }
            validarGps('recogida','solodatos');
            //obtenerMiruta('recogida','solodatos');
        }else if(json.error == "Servicio activo"){
            //alert('servicioactivo');
              var dd = new Array();
              dd["reserva"] = json.id;
              dd['curso'] = "si";
              dd['temp'] = json.temp;
              //mostrarpagina("iniciarcarrera", dd);
          }else{
              alerta(json.error, nombreaplicacion, "Aceptar");
          }
      },
      error: function(){
        $('.cargando').hide();
        alerta("Ocurrio un error al traer los servicios del conductor", nombreaplicacion, "Aceptar");
      }
    });
    //intervaloservicios = setInterval(actualizarservicios, 60000);
  }

//funcion que detecta el cambio de estado del conductor.
function cambiostatus(status){
    //alert(status);
    console.log(status);
    if(status == 0){status = 1;}else if(status == 1){status = 0;}
    var y = '{ "userid" : "'+localStorage['idusuario']+'", "token" : "'+localStorage['token']+'", "status" : "' + status + '" }';
    var url = direccionservidor + "web/app.php/api/cambiostatus";
    console.log(y);
    var objeto=$.ajax({
        type: "POST",
        url: url,
        data: y,
        cache: false,
        success: function()
        {
            console.log("status: "+status);
          if(status == 1)
          {
            console.log('inicia interval de envio posicion conductor');
            //alerta('Su estado actual es activo, si desea cambiarlo, deslice el boton switch de la parte superior y cambielo a off.',nombreaplicacion,'Aceptar');
            localStorage.disponible = 1;
            if(navigator.platform == "Win32"){

            }else{
                //bgGeo.start();
                //crearIntervalo();
            }
            $(".status").attr('id',1);
            $("#statusconductor_text").text('Disponible');
          }
          else
          {
            $(".status").attr('id',0);
            localStorage.disponible = 0;
            console.log('se detiene interval de envio posicion conductor');
            //bgGeo.stop();
                //limpiarIntervalo();
            $("#statusconductor_text").text('Desconectado');
          }
        },
        error: function()
        {
            $('#statusconductor').val(0).slider("refresh");
            $("#statusconductor_text").text('Desconectado');
            console.log('se detiene interval de envio posicion conductor');
            console.log(refreshIntervalId);
            //bgGeo.stop();
            //limpiarIntervalo();
            console.log("Error de conexion por favor reactive el boton de estado de off a on, gracias. ",nombreaplicacion,'Aceptar');
            console.log("Ocurrio un error al cambiar el estado del conductor en el servidor.");
            $('.cargando').hide();

        }
    });
}
function crearIntervalo(){//qué carajos es esto....

    refreshIntervalId = setInterval(EjecutarCurrentPlugin, 10000);
}
function limpiarIntervalo(){
    clearInterval(refreshIntervalId);
}
//funcion para aceptar un servicio
function aceptarServicio(id){
    $('.cargando').show();
    localStorage.idreserva = id;
    var y = '{ "userid" : ["'+localStorage['conductor']+'"], "idreserva" : ["'+id+'"] }';
    var url = direccionservidor + "web/app.php/movilapi/aceptarPush";
    console.log(JSON.stringify(y));
    var objeto=$.ajax({
        type: "POST",
        url: url,
        data: y,
        cache: false,
        success: function(){
            var json = JSON.parse(objeto.responseText);
            //alert(JSON.stringify(json));
            console.log(JSON.stringify(json));
            if(json.error == ''){
                $('.cargando').hide();
                var fuec = json.idfuec;
                //alert(fuec);
                localStorage.fuec = fuec;
                //$("#fuec").show();
                //$("#btn_fuec").attr('href',direccionservidor+"web/ArchivosDescarga/Pdf/pdf.php?id="+fuec);
                //$("#fuec_inicio_ruta").attr('href',direccionservidor+"web/ArchivosDescarga/Pdf/pdf.php?id="+fuec);
                alerta('La ruta fue aceptada', nombreaplicacion, 'Aceptada');
                obtenerMiruta('recogida','solodatos','aceptado');
                //traerServicios();
            }else{
                $('.cargando').hide();
                alerta('Lo sentimos, esta ruta ya fue aceptada por otro conductor.', nombreaplicacion,'Error');
            }
        },error: function(){
            $('.cargando').hide();
            console.log('Error al aceptar la ruta', nombreaplicacion, 'Error');
        }
    });
}

//funcion que trae el historial de mis servicios del conductor.
function historialCarreras()
{
  var url = direccionservidor + "web/app.php/reservaapi/historialserviciosapp";
  var y = '{ "usuario" : "'+localStorage['idusuario']+'" , "token" : "'+localStorage['token']+'"}';
  console.log(url);
  console.log(y);
  $('.cargando').toggleClass('active');
  var objeto = $.ajax({
    type: "POST",
    url: url,
    data: y,
    cache: false,
    success: function()
    {

      var json = JSON.parse(objeto.responseText);
      var tabla = document.getElementById("tabladinamica");
      var HTML = "<table border='0' align='center' width='100%'><thead><tr><th height='50px'>Historial del viajes</th></br></tr></thead><tbody>";
      //console.log(json.length);
      console.log(json);
      var j = 0;
      if(json.error != ""){
      while(json[j] != null)
      {
        //HTML+="<tr><td align='center'><div onclick='despliegueViajes("+j+")' data-corners='true' data-shadow='true' data-iconshadow='true' data-wrapperels='span' data-theme='ass' data-disabled='false' class='ui-btn ui-shadow ui-btn-corner-all ui-btn-up-b' aria-disabled='false'><span class='btn' ><span class='ui-btn-text'>Tu viaje del "+json[j].fechainicial+"</span></span><button id='' data-role='button' data-direction='reverse' data-transition='slideup' onclick='despliegueViajes("+j+")' class='ui-btn-hidden' data-disabled='false'></button></div><br>";
        //HTML+="<tr style='padding: 50px;'><td align='center'><div onclick='despliegueViajes("+j+")' data-corners='true' data-shadow='true' data-iconshadow='true' data-wrapperels='span' data-theme='ass' data-disabled='false' class='ui-btn ui-shadow ui-btn-corner-all ui-btn-up-b' aria-disabled='false'><span class='btnhistorial' ><span class='ui-btn-text'>Tu viaje del "+json[j].fechainicial+"</span></span></div><br>";
        HTML+="<tr style='padding: 50px;'><td align='center'><div onclick='despliegueViajes("+j+")' data-corners='true' data-shadow='true' data-iconshadow='true' data-wrapperels='span' data-theme='ass' data-disabled='false' class='btnhistorial' aria-disabled='false'><span class='ui-btn-text'>Tu viaje del "+json[j].fechainicial+"</span></div><br>";
        HTML+="<div id='historia"+j+"' style='display:none;'><b>Nombre Usuario:</b> "+json[j].pasajeronombre+"<br>";
        HTML+="<hr style='height:3px; background-color:#089805; border:0; margin-top:12px; margin-bottom:12px;'>";
        HTML+="<b>Fecha Inicial Viaje:</b> "+json[j].fechainicial+"<br>";
        HTML+="<hr style='height:3px; background-color:#089805; border:0; margin-top:12px; margin-bottom:12px;'>";
        HTML+="<b>Fecha Final Viaje:</b> "+json[j].fechafinal+"<br>";
        HTML+="<hr style='height:3px; background-color:#089805; border:0; margin-top:12px; margin-bottom:12px;'>";
        HTML+="<b>Total del viaje:</b> "+json[j].valor+"<br>";
        HTML+="<hr style='height:3px; background-color:#089805; border:0; margin-top:12px; margin-bottom:12px;'>";
        HTML+="<b>Distancia recorrida:</b> "+json[j].distancia+"<br>";
        HTML+="<hr style='height:3px; background-color:#089805; border:0; margin-top:12px; margin-bottom:12px;'>";
        HTML+="<b>Tiempo:</b> "+json[j].tiempo+"</div></td></tr><tr><td></br></td></tr>";
        j++;
      }
      HTML += "</tbody></table>";
      tabla.innerHTML = HTML;
      $('.cargando').toggleClass('active');
     }
     else{$('.cargando').toggleClass('active');}

    },
    error: function()
    {
        $('.cargando').toggleClass('active');
        navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexi\u00f3n e intentelo nuevamente"  , null, nombreaplicacion, "Aceptar");
    }
  });
}

//funcion que trae el histrorial de mis servicios pasajero
function historialServiciosPasajeros()
{
  var url = direccionservidor + "web/app.php/reservaapi/historia";
  var y = '{ "usuario" : "'+localStorage['idusuario']+'" , "token" : "'+localStorage['token']+'"}';
  console.log(url);
  console.log(y);
  $('.cargando').toggleClass('active');
  var objeto = $.ajax({
    type: "POST",
    url: url,
    data: y,
    cache: false,
    success: function()
    {

      var json = JSON.parse(objeto.responseText);
      var tabla = document.getElementById("tabladinamica");
      var HTML = "<table border='0' align='center' width='100%'><thead><tr><th height='50px'>Historial del viajes</th></br></tr></thead><tbody>";
      //console.log(json.length);
      console.log(json);
      var j = 0;
      if(json.error != ""){
      while(json[j] != null)
      {
        //HTML+="<tr><td align='center'><div onclick='despliegueViajes("+j+")' data-corners='true' data-shadow='true' data-iconshadow='true' data-wrapperels='span' data-theme='ass' data-disabled='false' class='ui-btn ui-shadow ui-btn-corner-all ui-btn-up-b' aria-disabled='false'><span class='btn' ><span class='ui-btn-text'>Tu viaje del "+json[j].fechainicial+"</span></span><button id='' data-role='button' data-direction='reverse' data-transition='slideup' onclick='despliegueViajes("+j+")' class='ui-btn-hidden' data-disabled='false'></button></div><br>";
        //HTML+="<tr style='padding: 50px;'><td align='center'><div onclick='despliegueViajes("+j+")' data-corners='true' data-shadow='true' data-iconshadow='true' data-wrapperels='span' data-theme='ass' data-disabled='false' class='ui-btn ui-shadow ui-btn-corner-all ui-btn-up-b' aria-disabled='false'><span class='btnhistorial' ><span class='ui-btn-text'>Tu viaje del "+json[j].fechainicial+"</span></span></div><br>";
        HTML+="<tr style='padding: 50px;'><td align='center'><div onclick='despliegueViajes("+j+")' data-corners='true' data-shadow='true' data-iconshadow='true' data-wrapperels='span' data-theme='ass' data-disabled='false' class='btnhistorial' aria-disabled='false'><span class='ui-btn-text'>Tu viaje del "+json[j].fechainicial+"</span></div><br>";
        HTML+="<div id='historia"+j+"' style='display:none;'><b>Nombre Usuario:</b> "+json[j].pasajeronombre+"<br>";
        HTML+="<hr style='height:3px; background-color:#089805; border:0; margin-top:12px; margin-bottom:12px;'>";
        HTML+="<b>Fecha Inicial Viaje:</b> "+json[j].fechainicial+"<br>";
        HTML+="<hr style='height:3px; background-color:#089805; border:0; margin-top:12px; margin-bottom:12px;'>";
        HTML+="<b>Fecha Final Viaje:</b> "+json[j].fechafinal+"<br>";
        HTML+="<hr style='height:3px; background-color:#089805; border:0; margin-top:12px; margin-bottom:12px;'>";
        HTML+="<b>Total del viaje:</b> "+json[j].valor+"<br>";
        HTML+="<hr style='height:3px; background-color:#089805; border:0; margin-top:12px; margin-bottom:12px;'>";
        HTML+="<b>Distancia recorrida:</b> "+json[j].distancia+"<br>";
        HTML+="<hr style='height:3px; background-color:#089805; border:0; margin-top:12px; margin-bottom:12px;'>";
        HTML+="<b>Tiempo:</b> "+json[j].tiempo+"</div></td></tr><tr><td></br></td></tr>";
        j++;
      }
      HTML += "</tbody></table>";
      tabla.innerHTML = HTML;
      $('.cargando').toggleClass('active');
     }
     else{$('.cargando').toggleClass('active');}

    },
    error: function()
    {
        $('.cargando').toggleClass('active');
        navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexi\u00f3n e intentelo nuevamente"  , null, nombreaplicacion, "Aceptar");
    }
  });
}

function generarQR(){
    var datos='{"userid":["'+localStorage.idusuario+'"], "token":["'+localStorage.token+'"]}';
    $.ajax({data:datos,
    url:direccionservidor+"web/app.php/userapi/datosusuario",
    type:"POST",
    success:servicios_mostrarQR,
    }).fail( function(){
        alerta("QR no disponible ",nombreaplicacion,"Aceptar");}
    );
}

function servicios_mostrarQR(response){
    $.mobile.changePage( "#code-qr", { transition: "slice", changeHash: false});
    $('#imgQR').attr('src',response.qr);
    $('#linkQR').attr('href',response.qr);
}

/*function generarQR(code,tipo){
    //alert('code '+code+' tipo: '+tipo);
    if(code && (tipo == 'primero')){
        localStorage.ubicacion = 'confirmarqr';
        var separar = code.split("|");
        var url = direccionservidor + "web/app.php/movilapi/envioPushMovil";
        var datos = '{"device" : "","titulo" : "'+nombreaplicacion+'","mensaje": "Se ha notificado al pasajero de tu lleagada", "code" : "'+separar[1]+'", "bandera" : "notificado"}';
        var objeto=$.ajax({
            type: "POST",
            url: url,
            data: datos,
            cache: false,
            success: function()
            {
                $.mobile.changePage( "#code-qr", { transition: "slice", changeHash: false});
                var codigo = code+"|"+localStorage.token;
                $('#imgQR').attr('src',"https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl="+codigo);
            },
            error: function()
            {
                console.log("Ocurri\u00f3 un error de al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente",nombreaplicacion,"Aceptar");
                //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
                $('.cargando').hide();

            }
        });
    }else if((code != 'undefined') && (tipo == 'confirmar')){
        localStorage.ubicacion = 'confirmarqr';
        $.mobile.changePage( "#code-qr", { transition: "slice"});
        var codigo = code+"|"+localStorage.token;
        $('#imgQR').attr('src',"https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl="+codigo);
    }else{
        alerta("QR no disponible, el conductor aun no ha iniciado ruta ",nombreaplicacion,"Aceptar");
    }
}*/

function manejadorDeError(error){
    console.log(error.message);
    switch(error.code)
        {
            case error.PERMISSION_DENIED: console.log("El usuario no permite compartir datos de geolocalizacion");
            break;

            case error.POSITION_UNAVAILABLE: console.log("Imposible detectar la posicio actual");
            break;

            case error.TIMEOUT: console.log("La posicion debe recuperar el tiempo de espera servicios");
               createMapa();
            break;

            default: console.log("Error desconocido");
                createMapa();
            break;
        }
}

function calificacion(id) {
    console.log('calificacion ' + id);
    localStorage["idrutaCalificacion"] = id;
    var url = direccionservidor + "web/app.php/movilapi/infoServicio";
    var datos = '{"idreserva" : "' + id + '"}';
    $.ajax({
        type: "POST",
        url: url,
        data: datos,
        cache: false,
        success: exito_calificacion,

    }).fail(function () {
        console.log("Ocurri\u00f3 un error de al conectarse con el servidor 1, valide su conexion e int\u00e9ntelo nuevamente", nombreaplicacion, "Aceptar");
        //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
        $('.cargando').hide();
    }
    );
}

function exito_calificacion(json) {
    //var json = JSON.parse(objeto.responseText);
    console.log(json);
    if (json.error == "") {
        var fechi, dia, mes;
        //localStorage["idrutaCalificacion"] = id;
        //document.getElementById("horarecogida").innerHTML = json.horarecogida;
        //document.getElementById("horallegada").innerHTML = json.horallegada;
        console.log('json.horallegada: ' + json.horallegada);
        if (localStorage.precio) {
            $('#fichareloj').hide();
            $('#precio').html('$'+localStorage.precio);
            $('.vip').show();
        }
        if (json.horallegada != null) {
            fechi = new Date(json.horallegada);
            //var fechi=new Date('2018-07-04T16:14:00-0500');
            dia = fechi.getDate();
            mes = fechi.getMonth() + 1;
            $("#horallegada").html(fechi.getHours() + ':' + fechi.getMinutes() + ' - ' + dia + '/' + mes + '/' + fechi.getFullYear());
        }
        console.log('json.horarecogida: ' + json.horarecogida);
        if (json.horarecogida != null) {
            fechi = new Date(json.horarecogida);
            dia = fechi.getDay() + 1;
            mes = fechi.getMonth() + 1;
            $("#horarecogida").html(fechi.getHours() + ':' + fechi.getMinutes() + ' - ' + dia + '/' + mes + '/' + fechi.getFullYear());
        }

        localStorage.ubicacion = 'calificacion';
        $.mobile.changePage("#calificacion", { transition: "slide", changeHash: false });
        if (circulo){circulo.setMap(null);}
        if (camino){camino.setMap(null);}

        //$.mobile.changePage( $("#calificacion"), "slide", true, false);
    }
}
function datosConductorEnRuta(){
    //alert('datosconductor en ruta');
    var salida,entrada = null;
    console.log('entro a datosConductorenRuta');
    conductorpush = localStorage.conductornotificado;
    celularpush = localStorage.celularnotificado;
    rutadestinopush = localStorage.rutadestinonotificado;
    rutallegadapush = localStorage.rutallegadanotificado;
    placapush = localStorage.placanotificado;
    idrutapush = localStorage.idrutanotificado;
    if(localStorage.dirllegadaPasajero != 'null' && localStorage.dirdestinoPasajero != 'null'){
        salida = localStorage.dirllegadaPasajero;
        entrada =  localStorage.dirdestinoPasajero;
    }else{
        salida = localStorage.dirSalida;
        entrada = localStorage.dirLlegada;
    }
    var horaentrada = 'pendiente';
    //alert('funcion datosconducroenruta');
    //alert(conductorpush);
    $("#dato_mi_cheto").text(conductorpush+' - '+placapush);
    $("#recogida_mi_cheto").empty();
    $("#entrega_mi_cheto").empty();
    $("#recogida_mi_cheto").append(salida);
    $("#entrega_mi_cheto").append(entrada);
//    $('#nom_ruta_tabl').text(rutallegadapush);
//    $('#nombre_ruta_user').text(rutallegadapush);
//    $('#nombre_conductor_user').text(conductorpush);
//    $('#placa_ruta_user').text(placapush);
    $('#cel_conductor_user').attr("href","tel:"+celularpush);
}

/*esta función revisa si el conductor ya fue a recogerlo
    si no:llama funciones para mostrar home con mapa tipo PRINCIPAL */
function createMapaant(tipo) {
    //localStorage.idrutanotificado = 341;
    console.log('entro a createMapaant y ls_idrutanotificado: ' + localStorage.idrutanotificado);
    if (localStorage.idrutanotificado) {
        $('.cargando').show();
        horaRecogida();
        //$.mobile.changePage( "#page-seguimiento", { transition: "slide"});
        //$('#tiempo_cheto').show();
        if (navigator.platform == 'Win32') {
            setTimeout(function () { createMap(); }, 1000);
        } else {
            setTimeout(function () {
                console.log('validagps y acorta mapa');
                validarGps();
                //$("#map-canvas-seguimiento").attr("style", "height: 60vh !important");
                //google.maps.event.trigger(mapa, 'resize');
            }, 1000);
        }


    } else {
        //alert('tipo no existe idrutanotificado: '+tipo);
        console.log(tipo);
        if (tipo == 'principal') {
            $('.cargando').show();
            $('#tiempo_cheto').show();
            //alert('calificado si :'+localStorage.calificado);
            if (localStorage.calificado == 'si') {
                //$("#map-canvas-seguimiento").css({height: "400px"});
                //alerta("Gracias por utilizar nustros servicios, esperaremos atendelo de nuevo",nombreaplicacion,'Aceptar');
                localStorage.calificado = 'no';
                setTimeout(function () {
                    if (navigator.platform == 'Win32') {
                        setTimeout(function () { createMap(tipo); }, 1000);
                    } else {
                        validarGps(tipo);
                    }
                    //createMap(tipo);
                    //                        $("#cronometro").empty();
                    $('#tiempo_cheto').show();
                    $("#cronometro").text('00:00');
                    //$("#map-canvas-seguimiento").attr("style", "height: 92vh !important");
                }, 1000);
                $('#tiempo_cheto').hide();
                $("#conten_ruta_asignada_seguimiento").hide();
                $("#conten_sin_ruta").show();
                localStorage.removeItem('idConductorCheto');
                localStorage.removeItem('idrutanotificado');
            } else {
                //alert('calificado no :');
                $('#tiempo_cheto').hide();
                $("#conten_ruta_asignada_seguimiento").hide();
                $("#conten_sin_ruta").show();
                console.log('ira a tiempo');
                if (navigator.platform == 'Win32') {
                    setTimeout(function () { createMap(tipo); }, 1000);
                } else {
                    setTimeout(function() {validarGps(tipo);},1000);
                }
                //setTimeout(function(){createMap(tipo);},1000);
                horaRecogida();
                setTimeout(function () {
                    //$("#map-canvas-seguimiento").attr("style", "height: 92vh !important");
                }, 1000)

            }
        } else {
            alerta('El conductor aún no ha iniciado servicio.', nombreaplicacion, 'Aceptar');
            if (navigator.platform == 'Win32') {
                //setTimeout(function(){createMap('principal');},1000);
                setTimeout(function() {createMap('principal');},1000);
            } else {
                setTimeout(function() {validarGps('principal');},1000);
            }
        }
    }
}

    /*Esta función crea un mapa usando las APIS de google maps,
    borra el mapa anterior
     */
function createMap(tipo){
    tipomapa = tipo;
    if (mapa==undefined || mapa==null||!mapa) {
        $("#map-canvas-seguimiento").empty();
    }
    console.log('entro a createMap, tipo: '+tipo);
    if(tipo != 'principal'){//si no está en la pantalla principal y normal
        //setTimeout(function(){datosConductorEnRuta();},200);
        datosConductorEnRuta();
    }
    //solo si todavia no hay mapa
    if (mapa==undefined || mapa==null||!mapa) {
        var opciones = {
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            streetViewControl: false,
            fullscreenControl: false,
            mapTypeControl: false
        };
        mapa = new google.maps.Map(document.getElementById('map-canvas-seguimiento'), opciones);
        console.log(mapa);
    }
    if (navigator.geolocation)
    {
        //Hago el CallBack a mostrarLocalizacion
       watch_id = navigator.geolocation.getCurrentPosition(mostrarLocalizacionS,manejadorDeError,{ maximumAge: 60000, timeout: 60000, enableHighAccuracy: true});
    }
    else{
        //Caso contraio muestro error
        alerta("Su dispositivo no soporta Geolocalización",nombreaplicacion,"Aceptar");
    }
}
function CenterControl(centerControlDiv,mapa,pos){
    // Set CSS for the control border.
    var position;
    position = document.createElement('img');
    position.src = 'img/iconos/gps.png';
    position.style.height = '40px';
    position.style.width = '40px';
    position.style.marginBottom = '20px';
    position.style.cursor = 'pointer';
    position.title = 'Click to recenter the map';
    centerControlDiv.appendChild(position);
    // setea el mapa para volver a la posicion actual del cliente
    position.addEventListener('click', function() {
        //createMap();
        mapa.setCenter(pos);
    });
}

/*mostrarLocalizacionS crea un nuevo marcador para mostrar 'posicion'*/
//ademas lanza el seguimiento de conductor si hay idrutanotificado y mapa!=principal
function mostrarLocalizacionS(posicion){
    //window.clearInterval(interval);
    var lat = posicion.coords.latitude;
    var long = posicion.coords.longitude;
    console.log('mostrar mi ubicacion: '+lat+','+long);

    var pos = new google.maps.LatLng(lat,long);
    infowindow = new google.maps.InfoWindow();
    if (!markerYO) {
        markerYO = new google.maps.Marker({
            position: pos,//new google.maps.LatLng(lat, long),
            map: mapa
        });
        google.maps.event.addListener(markerYO, 'click', (function (markerYO, i) {
            return function () {
                infowindow.setContent('Tú');
                infowindow.open(mapa, markerYO);
            };
        })(markerYO, i));

    }else{
        markerYO.setPosition(pos);
    }

    mapa.setCenter(pos);
    us = pos;
    if (centerControlDiv == '') {
        centerControlDiv = document.createElement('div');
        new CenterControl(centerControlDiv, mapa, pos);
        centerControlDiv.index = 1;
        centerControlDiv.id = 'controles';
        mapa.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);
    }
    $('.cargando').hide();
    console.log(tipomapa);
    if(tipomapa != 'principal' && localStorage.idrutanotificado){
        //alert('inicia seguimiento');
        //console.log('seguimiento del conductor');
        //interval = setInterval(function(){seguimiento(lat,long);},6000);
    }
}

//Diana
function servicios_seguimientoC(poscond) {
    //faltaria de pronto dividir poscond
    console.log('entroa seguimientoC');
    var info=poscond.split(',');
    us = {lat: parseFloat(info[0]), lng: parseFloat(info[1])};
    console.log(us);
    //us = new google.maps.LatLng(poscond);
    if (circulo) {
        //console.log('seteando nuevalat: '+nuevalat+' nuevalong: '+nuevalong);
        //circulo.setMap(us);
        circulo.setMap(null);
        circulo.setPosition(us);
    }//else{
    circulo = new google.maps.Marker({
        position: us,
        icon: iconBase + 'cabs.png',
        map: mapa
    });
    mapa.setCenter(us);
    //distanciaDestino(nuevalat, lat, nuevalong, long, 'pasajero');
}


function servicios_obtenerPoli(idruta){
    var datis='{"idruta":"'+idruta+'","idusuario":"'+localStorage.idusuario+'"}';
    console.log('entro a obtenerPoli '+ datis);
    $.ajax({data:datis,
        url:direccionservidor+"web/app.php/movilapi/obtenerpoli",
        type:"POST",
        success:serv_mostrarPoli,
        }).fail( function(){
            alerta("Error conectandose al servidor para obtenerPoli",nombreaplicacion,"Aceptar");
            //$('.cargando').hide();
        }
        );
}

function serv_mostrarPoli(response){
    console.log('entro a mostrarPoli '+response);
    if (response.polilinea!=null){
        polilinea=response.polilinea;
    var marcador='';
    var orden=parseInt(response.orden);
    var aksruta=response.aksruta;
    console.log(aksruta);
    for (var ind = 0; ind < aksruta.length; ind++) {
        var info=aksruta[ind].split(',');
        var pa = {lat: parseFloat(info[0]), lng: parseFloat(info[1])};
        if (ind+1==orden) {
            console.log('el usuario tiene orden: '+orden);
            marcador=iconBase + 'rangerstation.png';
        }else{marcador='http://maps.google.com/mapfiles/kml/pal2/icon10.png';}
        console.log('esta en el for');
        markpasajero = new google.maps.Marker({
            position: pa,
            icon: marcador,
            map: mapa
        });

    }
    var caminocoords=google.maps.geometry.encoding.decodePath(polilinea);
    camino = new google.maps.Polyline({
        path: caminocoords,
        geodesic: true,
        strokeColor: '#0c9c58',
        strokeOpacity: 1.0,
        strokeWeight: 3
      });
      camino.setMap(mapa);
    }else{
        console.log('polilinea es null');
    }

}
//parece ser: esta función pide al servidor la posición del conductor que está haciendo la
//ruta, compara las distancias entre el pasajero y el conductor
//pone un marcador en donde está el conductor actualmente
/*function seguimiento(lat,long){
    var ruta = parseInt(localStorage.idrutanotificado)+1;
    var iconBase = 'https://maps.google.com/mapfiles/ms/micons/';
    var url =  direccionservidor + "web/app.php/movilapi/seguimientoRuta";
    var dato = '{"idruta" : "'+ruta+'"}';
    console.log(dato);
    //console.log(url);
    var ob1 =$.ajax ({
        url: url,
        type: "POST",
        data: dato,
        success: function() {
            var json = JSON.parse(ob1.responseText);
            //console.log(ob1.responseText);
            console.log(JSON.stringify(json));
            nuevalat = json.latitud;
            nuevalong = json.longitud;
            us = new google.maps.LatLng(nuevalat,nuevalong);
            if(circulo){
              //console.log('seteando nuevalat: '+nuevalat+' nuevalong: '+nuevalong);
                //circulo.setMap(us);
                circulo.setMap(null);
                circulo.setPosition(us);
            }//else{
                circulo = new google.maps.Marker({
                        position: us,
                        icon: iconBase + 'cabs.png',
                        map: mapa
                    });
                   mapa.setCenter(us);
               // }
        distanciaDestino(nuevalat,lat,nuevalong,long,'pasajero');
        },
        error: function(data){
            console.log('error'+data);
        }
    });
}*/


function valDispositivo(tipo){
    if(navigator.platform == "Win32"){
        obtenerMiruta(tipo,'recogida');
    }else{
        validarGps(tipo,'recogida');
    }
}
