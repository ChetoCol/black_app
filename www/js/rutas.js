var direccionservidor = "https://www.blacktransport.co/";
var rutasDomicilios = [];
var rutasOficina = [];
var direccion = "";
var telefono = "";
var devicePasajero = "";
var idreserva = "";
var nombrePasajero = "";
var latitudConductor = "";
var longitudConductor = "";
var calculoDistancia = false;
var posicionActual = "";
var watchId  = null;
var traerDireccion = [];
var estado = true;
var a = null;
var nombreaplicacion = "BLACK";
var pagina = null;
var intervalC = null;
var autoincremento = 1;
var alertamostrada = false;
var servicioiniciado = false;
var veces=0; //contador usado para espaciar las veces que el conductor reporta su posicion al servidor.
//ya no se usa veces, eso era antes de usar mauron
var ubicaciones_pasaj=[];
var orden_realpasaj=[];
var intcalificacion=6;
var ultimaHora=0;
//funcion que muestra los datos de la ruta armada que puede o no ser aceptada por
//el conductor
//todos los datos de la ruta, incluyendo las direcciones y paradas se muestran en una página
//val es si esta en la pagina dos (ver lista de pasajeros) o en la principal de 'mis rutas'
function mostrarMiruta(tipo, val) {
    servicioiniciado = val;//pagina dos
    $('.cargando').show();
    //console.log('nombreaplicacion: '+nombreaplicacion);
    console.log('entro en mostrarMiruta tipo: ' + tipo + ' val: ' + val);
    var nombreruta = '';
    console.log(rutas);
    if (rutas.length > 0) {//ahí está toda la ruta guardada
        //$("#iniciotecorrido").css("display", "block");
        //alert('entro a : '+tipo);
        if (tipo == 'aceptado') {
            //datos cargados a pantalla principal conductor
            //$('#aceptada').hide();
            if (localStorage.serivioIniciado!='si') {
                $('#ver_ruta_asignada').show();
                $('#ruta_asignada').show();
            }
            $('#foot_ruta_aceptada').show();
            $('#sin_aceptar').hide();
            $('#ruta_iniciada').hide();
            $('#info_googlemaps').hide();
            //$('#foot_ruta').hide();

            //datos cargados a pantalla de ver ruta conductor
            $("#item_rutaasignada").empty();
            $("#hroa_rutaasignada").empty();
            $("#dir_rutaasignada").empty();
            $("#tickets_rutaasignada").empty();
            $("#nombre_usuario_rutaasignada").empty();
            $("#total_paradas").text('');
            $("#fecha_ruta_asignada").text('');
            $('#nombre_ruta_asignada').text('');


            //alert('longitud para crear usuarios ruta: '+rutas.length)
            //console.log(rutas);
            for (var i = 0; i < rutas.length; i++) {
                for (var j = 0; j < rutas[i].length; j++) {
                    nombreruta = rutas[i][j]['nombreruta'];
                    var nombreusuario = rutas[i][j]['pasajero'];
                    var numpasajeros = rutas[i][j]['numpasajeros'];
                    var telefono = rutas[i][j]['telefono'];
                    var horaSplit = rutas[i][j]['hora'].split(' ');
                    var horaSplit2 = horaSplit[1].split(':');
                    var hora = horaSplit2[0] + ':' + horaSplit2[1];
                    var direccion = rutas[i][j]['direccionfisica'];
                    var fechaSplit = rutas[i][j]['fechareserva'].split(' ');
                    var fecha = fechaSplit[0];
                    var count = rutas[i].length;
                    ubicaciones_pasaj[j] = rutas[i][j]['direccion'];
                    orden_realpasaj[j] = parseInt(rutas[i][j]['orden']);
                    var item = j + 1;
                    console.log('se asigno ubicacionespasaj y ordenreal en mostrarmiruta');
                    //datos cargados a pantalla de ver ruta conductor
                    $("#item_rutaasignada").append("<label class='texto-10'>" + item + "</label>");
                    $("#hroa_rutaasignada").append("<label class='texto-10'>" + hora + "</label>");
                    $("#dir_rutaasignada").append("<label class='texto-10'>" + direccion + "</label>");
                    $("#tickets_rutaasignada").append("<label class='texto-10'>" + numpasajeros + "</label>");
                    $("#nombre_usuario_rutaasignada").append("<label class='texto-10'>" + nombreusuario + "</label>");
                    //datos cargados a pantalla principal conductor
                    //                    if(j==0){
                    //                        $('#telefono_pasajero_ruta_aceptada').attr("href",telefono);
                    //                        $('#telefono_pasajero_ruta_aceptada').attr("href",telefono);
                    //                    }
                    //                    var fila = '<tr><td align="left">'+nombreusuario+'</td><td align="center">'+telefono+'</td><td align="center">'+numpasajeros+'</td></tr>';
                    //                    $('#lista_pasajeros').append(fila);
                }
            }
            $("#total_paradas").text(count);
            $("#fecha_ruta_asignada").text(fecha);
            localStorage.nombreRuta = nombreruta;
            $('#nombre_ruta_asignada').text(nombreruta);
            if (!localStorage.serivioIniciado || localStorage.serivioIniciado == 'no') {
                $('#iniciar_ruta').show();
            }
            var transition = null;
            if (localStorage.reiniciada == 'si') {
                transition = 'fade';
            } else {
                transition = 'slide';
            }
            console.log('redireccionara a rutasignada');
            $.mobile.changePage("#page_rutaasignada", { transition: transition });
        } else if (tipo == 'iniciado') {
            localStorage.serivioIniciado = 'si';
            if (val == 'paginados') {
                history.back();
            }
            $('#map-canvas-conductor').removeClass('map-canvas-conductor').addClass('map-canvas-conductor_aceptado');
            $('#ruta_iniciada').show();
            $("#sin_aceptar").hide();
            console.log('ruta_asignada hide');
            $('#ruta_asignada').hide();
            $('#info_googlemaps').show();

            $('#aceptada').show();
            $('#foot_ruta_aceptada').hide();
            $('#iniciar_ruta').hide();
            $("#ver_ruta_asignada").hide();
            iniciarrecorrido();
            //tal vez haya que poner ls.serivioiniciado=si, si la funcion iniciarrecorrido
            //tuvo exito
        }
    } else {
        $("#sin_aceptar").show();
        if (tipo != 'aceptado') {
            console.log('se remueven todaas las variables del conductor para iniciar de nuevo');
            localStorage.removeItem('numpasajeros');
            localStorage.removeItem('conteopasajero');
            localStorage.removeItem('tipoRuta');
            localStorage.removeItem('idruta');
            localStorage.removeItem('idnombreruta');
            localStorage.removeItem('horarecogida');
            localStorage.removeItem('idrutaCalificacion');
            localStorage.removeItem('idreserva');
            localStorage.setItem('idrutaLocation', 'null');
            //$("#iniciotecorrido").css("display", "none");
        }
    }
    //estadoBtnConductor();
    $('.cargando').hide();
}

function estadoBtnConductor(){
    console.log(localStorage.disponible);
    if(localStorage.disponible == '1'){
        $("#statusconductor_text").text('Disponible');
        $(".status").attr("id",1);
        //$('#statusconductor').val(localStorage.disponible).slider("refresh");
        cambiostatus(0);
    }else{
        $("#statusconductor_text").text('Desconectado');
        $(".status").attr("id",0);

    }
}

//funcion para obtener del servidor la información completa de la ruta
function obtenerMiruta(tipo,caso,aceptado){
    console.log('obtener mi ruta: tipo: '+tipo+' caso: '+caso+ ' aceptado: '+aceptado);
    //$("#mypanel").panel( "close" );
    //alert('resultado para obtener mi ruta : tipo '+tipo+' caso '+caso);
    //alert(localStorage.tipoRuta);
    if(localStorage.tipoRuta){
        tipo = localStorage.tipoRuta;
        if(tipo == 'entrega'){
            console.log('se metio a obtenermiruta tipo Entrega');
            $("#div_revisar_QR").css('display','none');
            $("#div_dejar_pasajero").css('display','block');
            //$("#btn_cambiar_rango").attr("onchange", "valPasajeroEntregado();");
            $("#tipo_estado_ruta").text('En ruta Entrega');
        }
    }
    if(caso === 'mipos'){
        console.log('obtenermiruta caso mi pos crearMaba');
        crearMapa(caso);
    }else{
        var url = direccionservidor + "web/app.php/movilapi/serviciosconductor";
        var datos = '{"idconductor" : "'+localStorage.conductor+'","tipo" : "'+tipo+'","pasajerosCount": "", "idruta" : ""}';
        console.log("datos");
        console.log(datos);
        var objeto=$.ajax({
            type: "POST",
            url: url,
            data: datos,
            cache: false,
            success: function (datos) {
                //alert('respuesta de api: '+objeto.responseText);
                console.log(datos);
                console.log(objeto.responseText);
                if (objeto.responseText != "") {
                    var json = JSON.parse(objeto.responseText);
                    //alert('longitud de respuesta api: '+json.length);
                    console.log(json);
                    if (json.length != 0) {
                        //alert(localStorage.idruta);
                        if (!localStorage.idruta) {

                            console.log('creando variables de recogida');
                            //localStorage.numpasajeros = json.length;
                            if (json[0]['totales']) {
                                localStorage.numpasajeros = parseInt(json[0]['totales']);
                            } else {
                                localStorage.numpasajeros = json.length;
                            }
                            //localStorage.conteopasajero = 1;
                            localStorage.conteopasajero = json[0]['orden'];
                            //localStorage.tipoRuta = 'recogida';
                            localStorage.tipoRuta = tipo;
                            id_ruta = parseInt(json[0]['idnombreruta']);
                            localStorage.idruta = id_ruta;
                            if (json[0]['estado'] == '29') {
                                localStorage.setItem('serivioIniciado', 'si');
                                //localStorage.serivioIniciado='si';
                            }
                            if(tipo == 'entrega'){
                                console.log('se metio a obtenermiruta tipo Entrega2');
                                $("#div_revisar_QR").css('display','none');
                                $("#div_dejar_pasajero").css('display','block');
                                //$("#btn_cambiar_rango").attr("onchange", "valPasajeroEntregado();");
                                $("#tipo_estado_ruta").text('En ruta Entrega');
                            }
                        }
                        console.log(json);
                        console.log(caso);
                        //alert('valido el caso es = '+caso);
                        if (caso === 'solodatos') {
                            console.log('limpiara el watchposition en solodatos');
                            clearInterval(intervalC);
                            //navigator.geolocation.clearWatch(watchId );
                            rutas = [];
                            rutas.push(json);
                            //alert('reiniciara los pasajeros');
                            //mostrarMiruta();
                            console.log(aceptado);
                            //alert(aceptado);
                            //if(aceptado == 'aceptado'){
                            rutas_extractTotal();
                            if (localStorage.idruta) {
                                crearMapa('principal', 'solodatos', aceptado);
                                console.log('entro a aceptado');
                                if (tipo=='recogida') {
                                    localStorage.setItem('idrutaLocation', parseInt(localStorage.idruta) + 1);
                                }else{
                                localStorage.setItem('idrutaLocation', parseInt(localStorage.idruta));
                                }
                            } else {
                                alert('no hay idruta');
                                estadoBtnConductor();
                                crearMapa('principal', 'solodatos');
                            }
                        } else if (caso == 'singps') {
                            console.log('singps activa el interval');
                            //alert('obtenermi ruta, singps');
                            //bgGeo.start();
                            //crearIntervalo();
                            //refreshIntervalId = setInterval(EjecutarCurrentPlugin, 10000);
                            $('.cargando').hide();
                        } else {
                            console.log(localStorage.ubicacion);
                            //alert('me envia a crear mapa');
                            crearMapa(json, tipo);
                        }
                    } else {
                        alerta('Error al consultar la información de la ruta', nombreaplicacion, "Aceptar");
                    }
                } else {
                    //bgGeo.stop();
                    if (tipo == 'recogida') {
                        obtenerMiruta('entrega','solodatos');
                    } else {
                        rutas = [];
                        console.log(tipo);
                        console.log(caso);
                        if (caso == 'recogida') {
                            history.back();
                            localStorage.ubicacion = 'main';
                            alerta('En el momento no tiene ruta activa.', nombreaplicacion, "Aceptar");
                        } else if (caso == 'solodatos') {
                            estadoBtnConductor();
                            crearMapa('principal', 'solodatos');
                        } else {
                            mostrarMiruta();
                        }
                        alerta('En el momento no tiene ruta activa.', nombreaplicacion, "Aceptar");
                        console.log('no existen datos para mostrar ruta.');
                    }

                }
            },
            error: function()
            {
                //bgGeo.stop();
                //alerta('Error de conexion, por favor valide he intente de nuevo.',nombreaplicacion,"Aceptar");
                console.log("Ocurri\u00f3 un error al obtener la ruta");
                //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
                $('.cargando').hide();

            }
        });
    }
}
//esta funcion extrae todas las posiciones de la ruta recibida para usarlas en waze
//ademas extrae el orden
function rutas_extractTotal(){
    for (var i = 0; i < rutas.length; i++) {
        for (var j = 0; j < rutas[i].length; j++) {
            nombreruta = rutas[i][j]['nombreruta'];
            ubicaciones_pasaj[j] = rutas[i][j]['direccion'];
            orden_realpasaj[j] = parseInt(rutas[i][j]['orden']);
        }
    }
    console.log('ubicacionespasaj '+ubicaciones_pasaj);
    console.log('ordenreal: '+orden_realpasaj);
}

function rutas_sihayEntrega(){
    if(localStorage.tipoRuta){
        tipo = localStorage.tipoRuta;
        if(tipo == 'entrega'){
            $("#div_revisar_QR").css('display','none');
            $("#div_dejar_pasajero").css('display','block');
            //$("#btn_cambiar_rango").attr("onchange", "valPasajeroEntregado();");
            $("#tipo_estado_ruta").text('En ruta Entrega');
        }
    }
}

//funcion para reservar servicios
function reserva(){
    if(localStorage['device'] == undefined || localStorage['device'] == null || localStorage['device'] == 'no'){
          $('#imagenprogramar').attr('onclick', "$('#linkprogramar').click();");
          $('#imagensrapido').attr('onclick', "$('#linksrapido').click();");
    }else{
          $('#imagenprogramar').attr('onclick', "$('#linkprogramar').click();");
          $('#imagensrapido').attr('onclick', "$('#linksrapido').click();");
    }

    //trae la mayor capacidad de pasajeros
//    var urll='http://' + direccionservidor + 'web/app.php/cargaMayor';
//    var objeto=$.ajax({
//        type: "POST",
//        url: urll,
//        data: null,
//        cache: false,
//        success: function(){
//            var json = JSON.parse(objeto.responseText);
//            document.getElementById("mayorsalida").value = json["mayor"];
//            document.getElementById("mayorllegada").value = json["mayor"];
//        },
//        error: function(){
//            document.getElementById("mayorsalida").value = 20;
//            document.getElementById("mayorllegada").value = 20;
//        }
//    });

//    document.getElementById('pasajeros').value = '';
//    document.getElementById('hora').value = '';
//    document.getElementById('fecha').value = '';
//    document.getElementById('carro').value = '';
//    document.getElementById('pasajerosviaje').value = '';
//    document.getElementById('fechaviaje').value = '';
//    document.getElementById('horaviaje').value = '';
//    document.getElementById('destinoviaje').value = '';
//    document.getElementById('celularviaje').value = '';
//    document.getElementById('fechacelegido').value = '';
//    document.getElementById('horacelegido').value = '';

//    var y = '{ "userid" : ["'+localStorage['idusuario']+'"] , "token" : ["'+localStorage['token']+'"]}';
//    var url = direccionservidor + "web/app.php/customerapi/cargaReservas";
//    var reservas=$.ajax({
//        type: "POST",
//        url: url,
//        data: y,
//        cache: false,
//        success: function(){
//            var jsonreservas = JSON.parse(reservas.responseText);
//            if(jsonreservas.error != ""){
//                if(jsonreservas.error == "Falta pago"){
//                    var dd = new Array();
//                    dd['reserva'] = jsonreservas.id;
//                    $('.cargando').show();
//                    mostrarpagina('fin', dd);
//                }
//            }else{
//                if(localStorage['convenio'] != 0 && localStorage['convenio'] != undefined && localStorage['convenio'] != null){
//                    $('#empresarial').parent().show();
//                    convenioreserva = localStorage['convenio'];
//                    tiposervicio = "EMP";
//                }
//                console.log('mostrará mapa');
//                var opt= { maximumAge: 3000, timeout: 10000, enableHighAccuracy: true };
//                console.log("entro a  latitud longitud");
//                navigator.geolocation.getCurrentPosition(cd, onError, opt);
//            }
//        },
//        error: function(){
//            navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente", null, nombreaplicacion, "Aceptar");
//            $('.cargando').toggleClass('active');
//        }
//    });
}
function iniciarrecorrido(){
    clearInterval(intervalC);
//    var estado = $('#statusconductor').val();
//    console.log('estado actual del boton switche: '+estado);
//    if(estado == 0){
//        console.log('cambiando estado del boton switche.');
//        cambiostatus(1);
//        //$('#statusconductor').val(1).slider("refresh");
//    }else{
//        console.log('no se cambio estado del boton switche');
//    }
    //alert('iniciar recorrido');
    if (actualPos!=null) {
        console.log('se fue a datos pasajero desde iniciarrecorrido');
        datosPasajero(actualPos);
    }else{
        console.log('hubo que esperar para iniciarrecorrido');
        setTimeout(function(){ iniciarrecorrido(); }, 3000);
    }

    //navigator.geolocation.getCurrentPosition(datosPasajero,error,{ maximumAge: 60000, timeout: 60000, enableHighAccuracy: true});
}
function gps(){
    console.log('boton de waze');
    navigator.geolocation.getCurrentPosition(function(pos){
            var posicionActual = pos.coords.latitude+","+pos.coords.longitude;
            console.log(direccion);
            console.log(posicionActual);
            var cadena='';
            for (var i = 0; i < ubicaciones_pasaj.length; i++) {
                if (orden_realpasaj[i]>=localStorage.conteopasajero) {
                    if (i==ubicaciones_pasaj.length-1) {
                        cadena = cadena+ubicaciones_pasaj[i];
                    }else{
                        cadena = cadena+ubicaciones_pasaj[i]+'+to:';
                    }
                }
            }
            console.log('cadena '+cadena);
            launchnavigator.appSelection.userChoice.clear();
            launchnavigator.navigate(cadena, {
                start: posicionActual
            });

            //var url = 'http://maps.google.com/maps?saddr='+posicionActual+'&daddr='+direccion;
            //$('#wazemaps').attr('href',url);
            //document.getElementById("wazemaps").click();
        }, error);
}
function marcarNumero(){
    $('#marcartelefono').attr('href',"tel:+"+telefono);
    document.getElementById("marcartelefono").click();
}
function datosPasajero(position){
    //alert('datos pasajero');
    $('.cargando').show();
    //var posConductor = position.coords.latitude+","+position.coords.longitude;
    var posConductor = position;
    //var idruta = localStorage['idruta'];
    //localStorage['idrutaLocation'] = idruta;
    var currentdate = new Date();
    var hora = currentdate.getFullYear()+"-"+(parseInt(currentdate.getMonth())+1)+"-"+currentdate.getDate()+" "+currentdate.getHours()+":"+currentdate.getMinutes()+":00";
    var url = direccionservidor + "web/app.php/movilapi/serviciosconductor";
    var datos = '{"idconductor" : "'+localStorage.conductor+'","tipo" : "'+localStorage.tipoRuta+'","pasajerosCount": "'+localStorage.conteopasajero+'", "idruta" : "'+localStorage.idruta+'", "posConductor" : "'+posConductor+'", "hora" : "'+hora+'", "iniciado" : "'+servicioiniciado+'"}';
    console.log(datos);
    var objeto=$.ajax({
        type: "POST",
        url: url,
        data: datos,
        cache: false,
        success: function()
        {
            console.log(objeto.responseText);
            var json = JSON.parse(objeto.responseText);
            //console.log(json);
            //$.mobile.changePage( "#map-page1", {transition: "slice", changeHash: true});
            //console.log(pagina);
            console.log('redirecciono');
            localStorage.ubicacion = 'ruta';
            comienzoRuta(json);
            //$('.cargando').hide();
        },
        error: function()
        {
            cambiostatus(0,1);
            //$('#statusconductor').val(0).slider("refresh");
            alerta("Error de conexion, por favor valide he intente de nuevo.",nombreaplicacion,"Aceptar");
            console.log("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente");
            $('.cargando').hide();

        }
    });
}
function comienzoRuta(obj){
    //alert('comienzo ruta');
    //$('.cargando').show();
    console.log(obj);
    //localStorage['idrutaLocation'] = localStorage.idruta;
    localStorage['idnombreruta'] = obj[0]['idnombreruta'];
    devicePasajero = obj[0]['device'];
    idreserva = obj[0]['idreserva'];
    localStorage['idreserva']=idreserva;
    nombrePasajero = obj[0]['pasajero'];
    localStorage['codep'] = obj[0]['cedula'];
    var numpasajeros = obj[0]['numpasajeros'];
    //document.getElementById("titulo-inicio-servicio").innerHTML = "Pasajero No "+obj[0]['orden'];
    $("#parada_ruta_aceptada").text('Parada: '+localStorage.conteopasajero);
    document.getElementById("nombre_pasajero_ruta_aceptada").innerHTML = nombrePasajero;
    telefono = obj[0]['telefono'];
    traerDireccion = obj[0]['direccion'].split(",");
    direccion = traerDireccion[0]+","+traerDireccion[1].trim();
    document.getElementById("dir_recogida_pasajero").innerHTML = obj[0]['direccionfisica'];
    var taerHora = obj[0]['hora'].split(" ");
    var coordenadas = obj[0]['direccion'];
    document.getElementById("hora_recogida_pasajero").innerHTML = taerHora[1];
    $("#telefono_pasajero_ruta_aceptada").attr("href","tel:"+telefono);
    //$("#tickets_rutainiciada").text(numpasajeros); MODIFICADO
    //crearMapa('onlyPoint', direccion);
    console.log(traerDireccion);
    var lat = coordenadas.split(',')[0];
    var long = coordenadas.split(',')[1];
    latLng = new google.maps.LatLng(lat,long);
    // CREACION DEL MARCADOR DE LA PROXIMA PARADA
    console.log(latLng);
    marker.setPosition(latLng);
    map.setCenter(latLng);
    validarDistancias();
}
function validarDistancias(){
    if(localStorage.tipoRuta == 'recogida'){
        $('#avisar_llegada').css("display", "block");
        $('#escanear_qr').css("display", "block");
        $('#pasajero_entregado').css("display", "none");
    }
    //alert('cargo datos');
    $('.cargando').hide();
    //alert('cargo datos');
    //var options = { maximumAge: 60000, timeout: 60000, enableHighAccuracy: true };

    //intervalC = setInterval(function(){getPosition();},16000); Dianaback
}
function getPosition(){
    var options = {timeout: 60000};
    navigator.geolocation.getCurrentPosition(successGetPosi,error, options);
}
function rutas_successGetPosi(lati,longi){
    //estado = true;
    //alert("interval");
    latitudConductor = lati;
    //latitudConductor = pos.coords.latitude;
    longitudConductor =  longi;
    //longitudConductor =  pos.coords.longitude;
    veces=veces+1;
    console.log(latitudConductor+","+longitudConductor);
    console.log(traerDireccion[0]+","+traerDireccion[1]);
    console.log('se obtuvo posicion del conductor ahora se calculara la dist al pasajero');
    if (veces==1) {
        //rutas_reportarUbicacionC(latitudConductor+","+longitudConductor);
    }
    if (veces==11) {veces=0;}
    distanciaDestino(latitudConductor,traerDireccion[0],longitudConductor,traerDireccion[1]);
}
/*function successGetPosi(pos){ANTERIOR
    //estado = true;
    //alert("interval");
    latitudConductor = pos.coords.latitude;
    longitudConductor =  pos.coords.longitude;
    veces=veces+1;
    console.log(latitudConductor+","+longitudConductor);
    console.log(traerDireccion[0]+","+traerDireccion[1]);
    console.log('se obtuvo posicion del conductor ahora se calculara la dist al pasajero');
    if (veces==1) {
        //rutas_reportarUbicacionC(latitudConductor+","+longitudConductor);
    }
    if (veces==11) {veces=0;}
    distanciaDestino(latitudConductor,traerDireccion[0],longitudConductor,traerDireccion[1]);
}*/
function error(error) {
    console.log(error.message);
    switch (error.code) {
        case error.PERMISSION_DENIED: console.log("El usuario no permite compartir datos de geolocalizacion");
            break;

        case error.POSITION_UNAVAILABLE: console.log("Imposible detectar la posicio actual");
            break;

        case error.TIMEOUT: console.log("Se agoto el tiempo de espera de geolocalizacion");
            if (actualPos != null) {
                var posicionActual = actualPos;
                console.log('entro a errorTIMEOUT de waze');
                console.log(posicionActual);
                var cadena = '';
                for (var i = 0; i < ubicaciones_pasaj.length; i++) {
                    if (orden_realpasaj[i] >= localStorage.conteopasajero) {
                        if (i == ubicaciones_pasaj.length - 1) {
                            cadena = cadena + ubicaciones_pasaj[i];
                        } else {
                            cadena = cadena + ubicaciones_pasaj[i] + '+to:';
                        }
                    }
                }
                console.log('cadena ' + cadena);
                launchnavigator.appSelection.userChoice.clear();
                launchnavigator.navigate(cadena, {
                    start: posicionActual
                });

            }
            //iniciarrecorrido();
            break;

        default: console.log("Error desconocido de geolocalizacion");
            //iniciarrecorrido();
            break;
    }
}

//esta función envia la posicion del conductor al servidor (Diana)
function rutas_reportarUbicacionC(posCond){
    var idruti = null;
    var currentdate = new Date();
    //var hora = currentdate.getFullYear()+"-"+(parseInt(currentdate.getMonth())+1)+"-"+currentdate.getDate()+" "+currentdate.getHours()+":"+currentdate.getMinutes()+":00";
    var datis;
    //var datos = '{"idconductor" : "'+localStorage.conductor+'","tipo" : "'+localStorage.tipoRuta+'","pasajerosCount": "'+localStorage.conteopasajero+'", "idruta" : "'+localStorage.idruta+'", "posConductor" : "'+posConductor+'", "hora" : "'+hora+'", "iniciado" : "'+servicioiniciado+'"}';
    if (localStorage.serivioIniciado) {
        var milis=currentdate.getTime();
        var resta=milis-ultimaHora;
        if (resta>120000) {//revisa si ya pasaron 2 min
            datis='{"idconductor":"'+localStorage.conductor+'","tipo":"'+localStorage.tipoRuta+'", "pasajerosCount":"'+localStorage.conteopasajero+'","idruta":"'+localStorage.idruta+'","idrutauni":"'+localStorage.idrutaLocation+'","posConductor":"'+posCond+'","flag" : "'+1+'"}';
            ultimaHora=milis;
        }else{
            datis='{"idconductor":"'+localStorage.conductor+'","tipo":"'+localStorage.tipoRuta+'", "pasajerosCount":"'+localStorage.conteopasajero+'","idruta":"'+localStorage.idruta+'","idrutauni":"'+localStorage.idrutaLocation+'","posConductor":"'+posCond+'","flag" : "'+0+'"}';
        }
    }else{
        idruti=0;
        datis='{"idconductor":"'+localStorage.conductor+'","idrutauni":"'+idruti+'","posConductor":"'+posCond+'","creado" : "'+hora+'"}';

    }
    $.ajax({data:datis,
        url:direccionservidor+"web/app.php/movilapi/reportarubicacionconductor",
        type:"POST",
        success:actualizarDuracionRuta,
        }).fail( function(){
            alerta("Error conectandose al servidor para Reportar",nombreaplicacion,"Aceptar");
            //$('.cargando').hide();
        }
        );
        console.log('entro a reportarUbicacionC'+datis);
}

function actualizarDuracionRuta(response){
    if (localStorage.serivioIniciado) {
        console.log(response);
        if (response!=undefined){
        console.log('apiexitoso cambiando #hora_real_recogida y seg');
        $('#hora_real_recogida').text(response.texto);
        $('#seg_real_recogida').text(response.valor);
        }else{
            console.log('bien, pero no han pasado 2 minutos para llamar a google');
        }
    }else{
        console.log('se reporto ubicacion exitosamente');
    }
}

//funcion para obtener los radianes.
function toRad(Value) {
    /** Convierte nuemors en grados a radianees */
    return Value * Math.PI / 180;
}

//funcion que calcula la distancia entre dos puntos lat,long
//el 'usuario' es de donde se hace la peticion, pasajero o conductor
//si la distancia es muy pequeña muestra alertas dependiendo del caso: hemos llegado
function distanciaDestino(lat2,lat1,lon2,lon1,usuario){
    //alert('latitud2= '+lat2+' latitud1= '+lat1+' longitud2= '+lon2+' longitud1'+lon1);
    var R = 6371; // km
    var dLat = toRad(lat2-lat1);
    var dLon = toRad(lon2-lon1);
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    //alert(d);
    //console.log(d+' < '+0.06);
    if(d<0.06){
        //alert(localStorage['tipoRuta']);
        //console.log('buscando');
        if(localStorage['tipoRuta'] === 'recogida')
        {
            //clearInterval(IntervalPosition);
            console.log('llegada a recoger');
            //navigator.geolocation.clearWatch(IntervalPosition);
            //alert(usuario);
            console.log(us);
            console.log(map);
            if(usuario == 'pasajero'){
                mapa.setCenter(us);
                clearInterval(interval);
                clearInterval(intervalC);
//                localStorage.removeItem('tipoRuta');
//                $('#ubicar_cheto').removeClass('oculta');
                //alert(alertamostrada);
                if(!alertamostrada){
                    alerta("El conductor ha llegado.",nombreaplicacion,"Aceptar");
                    alertamostrada = true;
                    setTimeout(function(){alertamostrada = false},30000);
                }
            }else{
                //clearInterval(intervalC);
                if(navigator.platform == "Win32"){

                }else{
                    navigator.vibrate(2000);
                    cordova.plugins.backgroundMode.isActive(function(){
                        cordova.backgroundapp.show();
                    });
                }
                avisarllegada();
            }
        }
        else
        {
            console.log('no es recogida');
             //clearInterval(IntervalPosition);
             //clearInterval(intervalC);
             //alerta('encontro ubicacion');
             //navigator.geolocation.clearWatch(watchId );
             if(!alertamostrada){
                alerta("hemos llegado al destino de "+nombrePasajero,nombreaplicacion,"Aceptar");
                alertamostrada = true;
                setTimeout(function(){alertamostrada = false},30000);
            }
             //pasajeroEntregado();
        }
    }else{
        //console.log(intervalC);
        //console.log('aun no llegamos');
    }
    //return calculoDistancia;
}

//envia una notificación al pasajero de que llegó el conductor
function avisarllegada(){
    var mensaje = localStorage.token+"|"+idreserva+"|"+nombrePasajero;
    var url = direccionservidor + "web/app.php/movilapi/envioPushMovil";
    var datos = '{"device" : "'+devicePasajero+'","titulo" : "'+nombreaplicacion+'","mensaje": "El conductor ha llegado a tu domicilio", "code" : "'+mensaje+'", "bandera" : "recogido"}';
    console.log(datos);
    var objeto=$.ajax({
            type: "POST",
            url: url,
            data: datos,
            cache: false,
            success: function()
            {
                if(navigator.platform == "Win32"){
                    //console.log(watchId);
                    alerta("Hemos llegado a la ubicación de "+nombrePasajero,nombreaplicacion,"Aceptar");
                    console.log(objeto.responseText);
                }else{
                    //escanearqr();
                    if(!alertamostrada){
                        alerta("Hemos llegado a la ubicación de "+nombrePasajero,nombreaplicacion,"Aceptar");
                        console.log(objeto.responseText);
                        alertamostrada = true;
                        setTimeout(function(){alertamostrada = false},30000);
                    }
                }
            },
            error: function()
            {
                alerta("Ocurri\u00f3 un error al enviar la notificacion.",nombreaplicacion,"Aceptar");
                //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
                $('.cargando').hide();

            }
        });
}
var movido = false;
function valPasajeroEntregado() {
    $('.cargando').show();
    var slide = $('.ui-slider-handle').attr('title');
    console.log(slide);
    clearInterval(intervalC);
    console.log(movido);
    if (movido == false) {
        console.log('salio el pasajero');
        pasajeroEntregado();
        movido = true;
    } else {
        $('.cargando').hide();

    }
}
function pasajeroEntregado(){
    var currentdate = new Date();
    var horaentrega = currentdate.getFullYear()+"-"+(parseInt(currentdate.getMonth())+1)+"-"+currentdate.getDate()+" "+currentdate.getHours()+":"+currentdate.getMinutes()+":00";
    var url = direccionservidor + "web/app.php/movilapi/envioPushMovil";
        var datos = '{"device" : "'+devicePasajero+'","titulo" : "'+nombreaplicacion+'", "pos" : "'+actualPos+'","mensaje": "El servicio ha finalizado, te invitamos a calificarlo", "code" : "'+idreserva+'", "bandera" : "entrega", "horaentrega":"'+horaentrega+'"}';
        console.log(datos);
    $.ajax({
        data: datos,
        url: url,
        type: "POST",
        cache: false,
        success: exito_pasajeroEntregado,
    }).fail(function () {
        alerta("Ocurri\u00f3 un error en la conexión envioMovil, valide e intente de nuevo", nombreaplicacion, "Aceptar");
        //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
        $('.cargando').hide();
        $("#btn_cambiar_rango").val(0).change();
    }
    );
}

function exito_pasajeroEntregado(response) {
    //var json = JSON.parse(objeto.responseText);
    console.log(response);
    var contador_pasajeros = parseInt(localStorage.conteopasajero);
    var total_pasajeros = parseInt(localStorage.numpasajeros);
    console.log(localStorage.numpasajeros);
    console.log(contador_pasajeros + ' < ' + total_pasajeros);
    if (contador_pasajeros < total_pasajeros) {
        //clearInterval(intervalC);
        console.log('si es menor');
        localStorage['conteopasajero'] = contador_pasajeros + 1;
        iniciarrecorrido();
        //$("#btn_cambiar_rango").val(0).change(); Diana
        setTimeout(function () { $('.cargando').hide(); movido = false; }, 400);
    }
    else {
        if (response.precio) {
            console.log('es un servicio vip');
            alerta("Ha finalizado el servicio VIP, el costo es de $"+response.precio+" pesos", nombreaplicacion, "Aceptar");
        }
        cambioEstadoRuta();
        $("#btn_cambiar_rango").val(0).change();
        setTimeout(function () { $('.cargando').hide(); movido = false; }, 400);
    }
}

function escanearqr(){//ya no se usa
    //alert("entre a qr");
    cordova.plugins.barcodeScanner.scan(
      function (result) {
          //$('escanear_qr').hide();
            validarQR(result.text);
      },
      function (error) {
          //$('escanear_qr').show();
          alerta("No pudimos escanear el codigo.",nombreaplicacion,"Aceptar");
          $('.cargando').hide();
      },
      {
          preferFrontCamera : true, // iOS and Android
          showFlipCameraButton : true, // iOS and Android
          showTorchButton : true, // iOS and Android
          torchOn: true, // Android, launch with the torch switched on (if available)
          saveHistory: true, // Android, save scan history (default false)
          prompt : "Place a barcode inside the scan area", // Android
          resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
          formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
          orientation : "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
          disableAnimations : true, // iOS
          disableSuccessBeep: false // iOS and Android
      }
   );
}

function validarQR(codigo){//ya no se usa
    $('.cargando').show();
    var currentdate = new Date();
    var horarecogida = currentdate.getFullYear()+"-"+(parseInt(currentdate.getMonth())+1)+"-"+currentdate.getDate()+" "+currentdate.getHours()+":"+currentdate.getMinutes()+":00";
    localStorage['horarecogida'] = horarecogida;
    var texto = codigo.split('|');
    var url = direccionservidor + "web/app.php/movilapi/validaQR";
        var datos = '{"token1" : "'+texto[0]+'","token2" : "'+texto[1]+'","token3": "'+texto[2]+'","token4": "'+texto[3]+'","horarecogida": "'+horarecogida+'"}';
        var objeto=$.ajax({
            type: "POST",
            url: url,
            data: datos,
            cache: false,
            success: function()
            {
                var json = JSON.parse(objeto.responseText);
                if(json.error == "")
                {
                    //alert('entramos');
                    $('.cargando').hide();
                    //alert("QR"+localStorage.conteopasajero);
                    //alert("QR"+localStorage.numpasajeros);
                    var contador_pasajeros = parseInt(localStorage.conteopasajero);
                    var total_pasajeros = parseInt(localStorage.numpasajeros);
                    console.log(localStorage.numpasajeros);
                    if(contador_pasajeros<total_pasajeros)
                    {
                        //alert("es menor a 2");
                        localStorage['conteopasajero'] = contador_pasajeros + 1;
                        calculoDistancia = false;
                        console.log('iniciara recorrido de nuevo');
                        iniciarrecorrido();
                    }
                    else
                    {
                        //alert("no es igaul a 2");
                        cambioEstadoRuta();
                    }
                }
                else
                {
                   $('.cargando').hide();
                  alerta(json.error, nombreaplicacion, "Aceptar");
                }
            },
            error: function()
            {
                alerta("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente",nombreaplicacion,"Aceptar");
                //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
                $('.cargando').hide();

            }
        });
}

//Funcion hecha por Diana
function rutas_validarRecogidaPasajero(cedula){
    //$('.cargando').show();
    var currentdate = new Date();
    var horarecogida = currentdate.getFullYear()+"-"+(parseInt(currentdate.getMonth())+1)+"-"+currentdate.getDate()+" "+currentdate.getHours()+":"+currentdate.getMinutes()+":00";
    localStorage['horarecogida'] = horarecogida;
    //alert('localstorage: '+localStorage.idreserva);
    //alert(idreserva);
    if(localStorage.idreserva){
        idreserva = localStorage.idreserva;
    }
        var datos='{"idreserva":["'+idreserva+'"],"cedula":["'+cedula+'"],"idruta":["'+localStorage.idrutaLocation+'"],"idruta":["'+localStorage.idrutaLocation+'"],"idcond":["'+localStorage.conductor+'"], "pos":["'+actualPos+'"]}';
        $.ajax({data:datos,
        url:direccionservidor+"web/app.php/movilapi/validaQR",
        type:"POST",
        cache:false,
        success:rutas_QRvalido,
        }).fail( function(){
            alerta("Error al conectarse al servidor, verifique su conexión e intente de nuevo",nombreaplicacion,"Aceptar");
            $('.cargando').hide();
        }
        );
        console.log(datos);
}

//Funcion hecha por Diana
function rutas_QRvalido(response) {
    console.log(response);
    if (response.error == "") {

        $('.cargando').hide();

        var contador_pasajeros = parseInt(localStorage.conteopasajero);
        var total_pasajeros = parseInt(localStorage.numpasajeros);
        clearInterval(intervalC);
        console.log(localStorage.numpasajeros);
        if (contador_pasajeros < total_pasajeros) {
            deslizante = false;
            localStorage['conteopasajero'] = contador_pasajeros + 1;
            calculoDistancia = false;
            console.log('iniciara recorrido hacia el siguiente');
            iniciarrecorrido();
            //$("#btn_cambiar_rango").val(0).change();
        }
        else {
            deslizante = false;
            //alert("no es igaul a 2");
            //$("#btn_cambiar_rango").val(0).change();
            cambioEstadoRuta();
        }
    }
    else {
        deslizante=false;
        //$("#btn_cambiar_rango").val(0).change();
        $('.cargando').hide();
        alerta(response.error, nombreaplicacion, "Aceptar");
    }
}

/*function rutas_validarRecogidaPasajero(){
    //$('.cargando').show();
    var currentdate = new Date();
    var horarecogida = currentdate.getFullYear()+"-"+(parseInt(currentdate.getMonth())+1)+"-"+currentdate.getDate()+" "+currentdate.getHours()+":"+currentdate.getMinutes()+":00";
    localStorage['horarecogida'] = horarecogida;
    //alert('localstorage: '+localStorage.idreserva);
    //alert(idreserva);
    if(localStorage.idreserva){
        idreserva = localStorage.idreserva;
    }
    var url = direccionservidor + "web/app.php/movilapi/validaQR";
        var datos = '{"token2" : "'+idreserva+'","horarecogida": "'+horarecogida+'"}';
        console.log(datos);
        var objeto=$.ajax({
            type: "POST",
            url: url,
            data: datos,
            cache: false,
            success: function()
            {
                var json = JSON.parse(objeto.responseText);
                console.log(json);
                if(json.error == "")
                {
                    //alert('entramos');
                    $('.cargando').hide();
                    //alert("QR"+localStorage.conteopasajero);
                    //alert("QR"+localStorage.numpasajeros);
                    var contador_pasajeros = parseInt(localStorage.conteopasajero);
                    var total_pasajeros = parseInt(localStorage.numpasajeros);
                    console.log(localStorage.numpasajeros);
                    if(contador_pasajeros<total_pasajeros)
                    {
                        //alert("es menor a 2");
                        setTimeout(function(){
                            deslizante = false;
                        },1000);
                        localStorage['conteopasajero'] = contador_pasajeros + 1;
                        calculoDistancia = false;
                        console.log('iniciara recorrido de nuevo');
                        iniciarrecorrido();
                        $("#btn_cambiar_rango").val(0).change();
                    }
                    else
                    {
                        setTimeout(function(){
                            deslizante = false;
                        },1000);
                        //alert("no es igaul a 2");
                        $("#btn_cambiar_rango").val(0).change();
                        cambioEstadoRuta();
                    }
                }
                else
                {
                    $("#btn_cambiar_rango").val(0).change();
                    $('.cargando').hide();
                    alerta(json.error, nombreaplicacion, "Aceptar");
                }
            },
            error: function()
            {
                console.log('errorr');
                alerta("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexión e int\u00e9ntelo nuevamente",nombreaplicacion,"Aceptar");
                //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
                $('.cargando').hide();
                $("#btn_cambiar_rango").val(0).change();

            }
        });
}*/

function cambioEstadoRuta(){
        //alert("estoy en cambio estado");
        $('.cargando').show();
        var currentdate = new Date();
        var hora = currentdate.getFullYear()+"-"+(parseInt(currentdate.getMonth())+1)+"-"+currentdate.getDate()+" "+currentdate.getHours()+":"+currentdate.getMinutes()+":00";
        var url = direccionservidor + "web/app.php/movilapi/cambioEstadoRuta";
        var datos = '{"idnombreruta" : "'+localStorage.idnombreruta+'", "tiporuta" : "'+localStorage.tipoRuta+'","hora" : "'+hora+'"}';
        console.log(datos);
        var objeto=$.ajax({
            type: "POST",
            url: url,
            data: datos,
            cache: false,
            success: function()
            {
                $('.cargando').hide();
                var json = JSON.parse(objeto.responseText);
                if(json.error == "")
                {
                    if(localStorage['tipoRuta'] === 'recogida')
                    {
//                        $("#avisar_llegada").css("display", "none");
//                        $("#escanear_qr").css("display", "none");
//                        $("#pasajero_entregado").css("display", "block");
                        $("#div_revisar_QR").css('display','none');
                        $("#div_dejar_pasajero").css('display','block');
                        //$("#btn_cambiar_rango").attr("onchange", "valPasajeroEntregado();");
                        $("#tipo_estado_ruta").text('En ruta Entrega');
//                        $("#btn_cambiar_rango").attr('type','hidden');
//                        $("#btn_cambiar_rango_entrega").attr('type','range');
                        localStorage['tipoRuta'] = 'entrega';
                        $idruta=parseInt(localStorage['idruta']);
                        localStorage['idruta'] = $idruta + 1;
                        localStorage['conteopasajero'] = 1; //tiene que ser 1 para que comienze con el primero
                        //alert("vuelve y empieza");
                        obtenerMiruta('entrega','solodatos');
                        console.log("vuelve y empieza pra dejar pasajeros");
                        alerta("Ha recogido a todos sus pasajeros, a continuación inicia la ruta para dejarlos en su destino"+localStorage.tipoRuta,nombreaplicacion,"Aceptar");
                        iniciarrecorrido();
                    }
                    else
                    {
                        //"ya acabe");
                        rutas=[]; //de pronto tambien se requiera al finalizar recogida
                        clearInterval(intervalC);
                        //navigator.geolocation.clearWatch(watchId);
                        limpiarIntervalo();
                        resetRutaConductor();
                    }
                }
                else
                {
                    clearInterval(intervalC);
                    navigator.notification.alert("la hicimos eureka" , null, nombreaplicacion, "Aceptar");
                }

            },
            error: function()
            {
                clearInterval(intervalC);
                alerta('Error de conexion por favor valide e intente nuevamente.',nombreaplicacion,"Aceptar");
                console.log("Ocurri\u00f3 un error al cambiar el estado del conductor",nombreaplicacion,"Aceptar");
                //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
                $('.cargando').hide();

            }
        });
}

function rutas_estrellitas(rate){
    var idestrella='estrella';
    intcalificacion=rate;
    for (var index = 1; index <=5; index++) {
        idestrella='#estrella'+index;
        if (index<=rate) {
            $(idestrella).attr("src","img/recursos/star-golden2.png");

        }else{
            $(idestrella).attr("src","img/recursos/star-gray.png");
        }
        $(idestrella).toggleClass("rotate-now");
    }
    console.log(intcalificacion);
}

function rutas_envioCalificacion(){
    var calificacion=document.getElementById("cajaObservaciones").value;
    //alert(calificacion);
    console.log('el rate es: '+intcalificacion);
    //funcion medio-vieja
    clearInterval(interval);//no me acuerdo que es este interval, averiguar
    var url = direccionservidor + "web/app.php/movilapi/calificacionRuta";
    var datos = '{"calificacion" : "'+intcalificacion+'","idreserva" : "'+localStorage.idrutaCalificacion+'","comentarios" : "'+calificacion+'"}';
    var objeto=$.ajax({
        type: "POST",
        url: url,
        data: datos,
        cache: false,
        success: function()
        {
            var json = JSON.parse(objeto.responseText);
            if(json.error == "")
            {

                console.log('se guardo calificacion correctamente');
                interval = 0;
                document.getElementById("cajaObservaciones").value='Observaciones:';
                rutas_estrellitas(0);
                intcalificacion=6;
                resetRutaUsuario(); //comentar para pruebas
                parar(); //comentar para pruebas
                //clearInterval(interval);
                localStorage.calificado = 'si';
                //$('#ubicar_cheto').addClass('oculta');
                //$.mobile.changePage( "#reserva", { transition: "slide", changeHash: false });
                accionesPasajero(); //comentar para pruebas
            }else{
                alerta('Error de calificación',nombreaplicacion,"Aceptar");
            }
        },
        error: function()
        {
            alerta("Ocurrió un error al enviar la calificación",nombreaplicacion,"Aceptar");
            //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
            $('.cargando').hide();

        }
    });
}

function resetRutaUsuario(){
    localStorage.removeItem('tipoRuta');
    localStorage.removeItem('idrutanotificado');
    localStorage.removeItem('idConductorCheto');
    localStorage.removeItem('conductornotificado');
    localStorage.removeItem('celularnotificado');
    localStorage.removeItem('rutadestinonotificado');
    localStorage.removeItem('rutallegadanotificado');
    localStorage.removeItem('placanotificado');
    localStorage.removeItem('dirllegadaPasajero');
    localStorage.removeItem('dirdestinoPasajero');
    localStorage.removeItem('idrutaCalificacion');
    localStorage.removeItem('cronometro');
    $("#cronometro").text('00:00');
    //$("#map-canvas-seguimiento").attr("style", "height: 92vh !important");
    $('#tiempo_cheto').hide();
    $("#conten_ruta_asignada_seguimiento").hide();
    $("#conten_sin_ruta").show();
    $("#fichareloj").show();
    $(".vip").hide();
    if (circulo){circulo.setMap(null);}
    if (camino){camino.setMap(null);}
}
function resetRutaConductor(){
    console.log('se remueven todaas las variables del conductor para iniciar de nuevo');
    localStorage.removeItem('numpasajeros');
    localStorage.removeItem('conteopasajero');
    localStorage.removeItem('tipoRuta');
    localStorage.removeItem('idruta');
    localStorage.removeItem('idnombreruta');
    localStorage.removeItem('horarecogida');
    localStorage.removeItem('idrutaCalificacion');
    localStorage.removeItem('idreserva');
    localStorage.removeItem('codep');//diana nuevo
    localStorage.setItem('idrutanotificado',undefined);//diana nuevo
    localStorage.setItem('idrutaLocation', 'null');
    autoincremento = 1;

    $("#item_rutaasignada").empty();
    $("#hroa_rutaasignada").empty();
    $("#dir_rutaasignada").empty();
    $("#tickets_rutaasignada").empty();
    $("#nombre_usuario_rutaasignada").empty();
    $("#total_paradas").text('');
    $("#fecha_ruta_asignada").text('');
    localStorage.nombreRuta = '';
    $('#nombre_ruta_asignada').text('');
    $('#iniciar_ruta').hide();

    localStorage.serivioIniciado = 'no';
    $('#map-canvas-conductor').removeClass('map-canvas-conductor_aceptado').addClass('map-canvas-conductor');
    $('#ruta_iniciada').hide();
    $('#info_googlemaps').hide();
    $('#foot_ruta_iniciada').hide();
    $('#aceptada').hide();
    $('#foot_ruta_aceptada').hide();
    $('#iniciar_ruta').hide();

    $('#aceptada').hide();
    $('#foot_ruta_aceptada').hide();
    $('#sin_aceptar').show();
    $('#foot_ruta').show();

    //datos cargados a pantalla de ver ruta conductor
    $("#item_rutaasignada").empty();
    $("#hroa_rutaasignada").empty();
    $("#dir_rutaasignada").empty();
    $("#tickets_rutaasignada").empty();
    $("#nombre_usuario_rutaasignada").empty();
    $("#total_paradas").text('');
    $("#fecha_ruta_asignada").text('');
    $('#nombre_ruta_asignada').text('');

    $("#div_revisar_QR").css('display','block');
    $("#div_dejar_pasajero").css('display','none');

    ubicaciones_pasaj=[];
    orden_realpasaj=[];
    nombrePasajero = "";
    servicioiniciado = false;
    traerDireccion = [];
    idreserva = "";
    direccion = "";
    telefono = "";
    devicePasajero = "";
    //$("#btn_cambiar_rango").attr("onchange", "valRecogida();");
//    $("#btn_cambiar_rango").attr('type','range');
//    $("#btn_cambiar_rango_entrega").attr('type','hidden');
    clearInterval(intervalC);
    servicioFinalizado();

}
function servicioFinalizado(){
    //$.mobile.changePage( "#homeconductor", { transition: "slide", changeHash: false });
    //alert('ira a accionesconductor');
    localStorage.removeItem('idreserva');
    localStorage.removeItem('serivioIniciado');
    clearInterval(intervalC);
    alerta("El servicio ha finalizado",nombreaplicacion,"Aceptar");
    restaurarEstadoConductor();
    //accionesConductor();
}
function restaurarEstadoConductor(){//usa un api para cambiar el estado del carro a desocupado en la bd
    console.log('entro a restaurarestadoconductor api finalizarServicio');
    var url = direccionservidor + "web/app.php/movilapi/finalizarServicio";
    var datos = '{"idconductor" : "'+localStorage.conductor+'"}';
    console.log(datos);
    var objeto=$.ajax({
        type: "POST",
        url: url,
        data: datos,
        cache: false,
        success: function()
        {
            var json = JSON.parse(objeto.responseText);
            if(json.error == "")
            {
            }
        },
        error: function()
        {
            alerta("Error de conexion por favor valide e intente de nuevo.",nombreaplicacion,"Aceptar");
            console.log('error al intentar cambiar el estado del conductor en la bd');
            //navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
            $('.cargando').hide();

        }
    });
}

function rutas_volver(){
    if (localStorage.serivioIniciado=='si') {
        $('#ruta_iniciada').show();
        $('#info_googlemaps').show();
    }
}
