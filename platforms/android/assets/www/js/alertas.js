var tipoDevice,idrecibido;//variable global que guarda el tipo de dialogo que se mostrara en el dispositivo movil.
var conductor = null;
var celular = null;
var rutadestino = null;
var rutallegada = null;
var placa = null;
var conductornotificado = null;
var celularnotificado = null;
var rutadestinonotificado = null;
var rutallegadanotificado = null;
var placanotificado = null;
var idrutanotificado = null;
var direccionservidor = "https://www.blacktransport.co/";
//funcion que recibe los mensajes y tipos de alertas que se mostraran.
function alerta(mensaje,titulo,tipo,id,conductor,celular,rutadestino,rutallegada,placa,dirllegada,dirdestino,fuec){
    localStorage.idConductorCheto = id;
    console.log('entro a alerta con mensaje: '+mensaje+' titulo: '+titulo+' tipo:'+tipo+' id: '+id+' conductor: '+conductor+' celular: '+celular+' rutadestino: '+rutadestino+' rutallegada: '+rutallegada+' placa: '+placa+' dirllegada: '+dirllegada+' dirdestino: '+dirdestino+' fuec: '+fuec);
    if(navigator.platform == 'Win32'){
        console.log('plataforma pc');
        dialogoPc(mensaje,tipo);
    }else{
        console.log('enviara notificacion de dispositivo dialogodevice');
        console.log(mensaje,titulo,tipo);
        dialogoDevice(mensaje,titulo,tipo,id,conductor,celular,rutadestino,rutallegada,placa,dirllegada,dirdestino,fuec);
    }
}
//funcion  que genera un dialogo para mostrar en el pc.
function dialogoPc(mensaje,tipo){
    console.log('tipo'+tipo);
    //if(tipo){
        switch(tipo){
            case 'actualizarperfil':
                    var r = alert(mensaje);
                    console.log('redireccionactualizarperfil');
                    $.mobile.changePage( "#perfil", { transition: "slide", changeHash: true });
                    localStorage.ubicacion = 'perfil';
                    obtenerDatosUsuario();
                break;
            case 'codigoexitoso':
                    var r = confirm(mensaje);
                    validarConfirm(r,tipo);
                break;
            case 'inscribirtarjeta':
                    var r = confirm(mensaje);
                    validarConfirm(r,tipo);
                break;
            case 'pagoefectivo':
                    $.mobile.changePage( "#reserva", { transition: "slice", changeHash: true });
                    if(localStorage.telefono == 0 || localStorage.telefono == undefined || localStorage.telefono == null || localStorage.telefono =="" || localStorage.telefono == 'null'){
                        console.log('tarjetacredito');
                        alerta("Por favor actualice su numero telefonico para brindarle un mejor servicio", nombreaplicacion, "actualizarperfil");
                    }
                break;
            case 'codigomovip':
                    var r = confirm(mensaje);
                    validarConfirm(r,tipo);
                break;
            case 'tokenizado':
                    var r = alerta(mensaje,nombreaplicacion,'Aceptar');
                    $.mobile.changePage( "#reserva", { transition: "slice", changeHash: true }, true, true);
                break;
            case 'nuevapromocion':
                    $.mobile.changePage( "#page_promociones", { transition: "slice", changeHash: true }, true, true);
                break;
            default:
                console.log('entro a alerta defoult');
                    alert(mensaje);
                break;
        }
    //}
}
//funcion que valida el resutado de la confirmacion y ejecuta una accion.
function validarConfirm(r,tipo){
    switch(tipo){
        case 'codigoexitoso':
                if(r == true){
//                    if(localStorage.tipotarjetac == '1'){
//                        pagoTarjetaCredito();
//                    }else{
//                        payuEfectivo();
//                        localStorage.ubicacion = 'pagos_estado';
//                    }
                    console.log('recargara billetera');
                    localStorage.ubicacion = 'pago';
                    recargarBilletera('codigo');
                }else{
                    var texto = null;
                    var codigotexto = null;
                    if(localStorage.tipotarjetac == '1'){
                        texto = 'val_pago_credito';
                        codigotexto = 'codigo_promocional_tc';
                    }else{
                        texto = 'val_pago';
                        codigotexto = 'codigo_promocional';
                    }
                    console.log('algo');
                    $('#'+codigotexto).val('');
                    $('#'+texto).attr('enabled','enabled');
//                    var totalactual = localStorage.valpagotemp;
//                    var descuento = localStorage.descuento;
//                    console.log((parseInt(totalactual)+'/'+parseInt(descuento))+'*'+100);
//                    var total = ((parseInt(totalactual)/parseInt(descuento))*100);
                    //$('#'+texto).text(total);
                    $('#'+texto).css({color: "red"});
                    localStorage.codigoPromo = 'null';
                    localStorage.valpagotemp = $('#valviajes_billetera').val();
                    localStorage.descuento = 'null';
                    console.log(localStorage.valpagotemp);
                }
            break;
        case 'inscribirtarjeta':
                if(r == true){
                    $.mobile.changePage( "#tarjeta", { transition: "fade", changeHash: true }, true, true);
                    localStorage.ubicacion = 'tarjeta';
                }else{
                    validarpagoefectivo();
                }
            break;
        case 'codigomovip':
                if(r == true){
                    var dd = new Array();
                    dd['reserva'] = localStorage['reservaactiva'];
                    mostrarpagina("factura", dd);
                    $('#codigomovip').dialog("close");
                }else{
                    $('.btn_menu').toggleClass('usuario');
                    mostrarpagina("reserva");
                    $('#codigomovip').dialog("close");
                }
            break;
    }
}
//funcion quue genera un dialogo para los dispositivos moviles.
function dialogoDevice(mensaje,titulo,tipo,id,conductor,celular,rutadestino,rutallegada,placa,dirllegada,dirdestino,fuec){
    //alert(tipo);
    console.log('entro a dialogodevice: tipo: ' +tipo+ ' id: '+id);
    if(tipo){
        tipoDevice = tipo;
        idrecibido = id;
        localStorage.fuec = fuec;
        if(tipo != 'Aceptar' && tipo != 'llegadaconductor' && tipo != 'llegadaoficina' || tipo == 'primerAviso' ){
            localStorage.conductornotificado = conductornotificado = conductor;
            console.log('ls.conductornotificado'+localStorage.conductornotificado);
            localStorage.celularnotificado = celularnotificado = celular;
            localStorage.rutadestinonotificado = rutadestinonotificado = rutadestino;
            localStorage.rutallegadanotificado = rutallegadanotificado = rutallegada;
            localStorage.placanotificado = placanotificado = placa;
            localStorage.dirllegadaPasajero = dirllegadaPasajero = dirllegada;
            localStorage.dirdestinoPasajero = dirdestinoPasajero = dirdestino;
//            alert('estoy en alerta');
//            alert(localStorage.conductornotificado);
            console.log('id de ruta que llego a dialogodevice '+id);
            //alert(id);
            if(id != null){
                localStorage.idrutanotificado = idrutanotificado = id;
            }
            //alert(localStorage.idrutanotificado);
            console.log('ls.idrutanotificado: '+localStorage.idrutanotificado);
        }
        switch(tipo){
            case 'actualizarperfil':
                    navigator.notification.alert(
                        mensaje,  // mensaje
                        null, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                    console.log('redireccionactualizarperfil');
                    $.mobile.changePage( "#perfil", { transition: "slide"});
                    localStorage.ubicacion = 'perfil';
                    obtenerDatosUsuario();
                break;
            case 'codigoexitoso': //relacionado con PAGOS
                    navigator.notification.confirm(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar,Cancelar' // nombre de boton
                    );
                break;
            case 'notificadoconductor':
                    navigator.notification.alert(
                        mensaje,  // mensaje
                        null, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                break;
            case 'nuevoservicio':
                    navigator.notification.confirm(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                break;
            case 'primerAviso'://cuando el conductor le da a iniciar servicio, llegaen pasajero
                    navigator.notification.alert(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar,Ahora no' // nombre de boton
                    );
                break;
            case 'avisollegada':
                    navigator.notification.alert(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                break;
            case 'QRexitoso':
                    navigator.notification.confirm(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                break;
            case 'llegadaconductor':
                    navigator.notification.confirm(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                break;
            case 'llegadaoficina':// llega a pasajero,el conductor presiona el chulo y lo deja en la oficina
            console.log('conductor='+conductor);
            if (conductor=='vip') {
                        localStorage.setItem('precio',celular);
                    }
                    navigator.notification.confirm(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                break;
            case 'inscribirtarjeta':
                    navigator.notification.confirm(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar,Ahora no' // nombre de boton
                    );
                break;
            case 'pagoefectivo':
                    navigator.notification.alert(
                        mensaje,  // mensaje
                        null, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                    if(localStorage.telefono == 0 || localStorage.telefono == undefined || localStorage.telefono == null || localStorage.telefono =="" || localStorage.telefono == 'null'){
                        console.log('tarjetacredito');
                        alerta("Por favor actualice su numero telefonico para brindarle un mejor servicio", nombreaplicacion, "actualizarperfil");
                    }
                break;
            case 'codigomovip':
                    navigator.notification.confirm(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar,Ahora no' //Nombre botones
                    );
                break;
            case 'exit':
                    navigator.notification.alert(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                break;
            case 'tokenizado':
                    navigator.notification.alert(
                        mensaje,  // mensaje
                        null, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                break;
            case 'nuevapromocion':
                    navigator.notification.alert(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );   horaRecogida()
                break;
            case 'servicioaceptado'://se asigno un carro a la reserva del pasajero--llega en pasajero
                    navigator.notification.alert(
                        mensaje,  // mensaje
                        callback, // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                break;
            default:
                    console.log('entro a la notificacion por defecto');
                    navigator.notification.alert(
                        mensaje,  // mensaje
                        null,     // callback
                        titulo,   // titulo
                        'Aceptar' // nombre de boton
                    );
                break;
        }
    }

}
//funcion callback de la funcion anterior, que valida el tipo de dialogo generado y redirecciona a la funcion correspondiente.
function callback(button){
    switch(tipoDevice){
        case 'avisollegada':
                validacionCallback(button);
            break;
        case 'primerAviso':
                validacionCallback(button);
            break;
        case 'nuevoservicio':
                validacionCallback(button);
            break;
        case 'llegadaconductor':
                validacionCallback(button);
            break;
        case 'llegadaoficina':
                validacionCallback(button);
            break;
        case 'QRexitoso':
                validacionCallback(button);
            break;
        case 'inscribirtarjeta':
                validacionCallback(button);
            break;
        case 'pagoefectivo':
                mostrarpagina("reserva");
            break;
        case 'codigomovip':
                validacionCallback(button);
            break;
        case 'nuevapromocion':
                validacionCallback(button);
            break;
        case 'codigoexitoso':
                validacionCallback(button);
            break;
        case 'servicioaceptado':
                validacionCallback(button);
            break;
        case 'exit':
                navigator.app.exitApp();
            break;
    }
}
//funcion pra dispositivos moviles que valida el resultado de la confirmacion y ejecuta una accion.
function validacionCallback(button){
    console.log('entro a validation callback: '+tipoDevice);
    switch(tipoDevice){
        case 'codigoexitoso':
                if(button == 1){
//                    if(localStorage.tipotarjetac == '1'){
//                        pagoTarjetaCredito();
//                    }else{
//                        payuEfectivo();
//                        localStorage.ubicacion = 'pagos_estado';
//                    }
                    console.log('recargara billetera');
                    localStorage.ubicacion = 'pago';
                    recargarBilletera('codigo');
                }else{
                    console.log('cancela recargara billetera');
                    var texto = null;
                    var codigotexto = null;
                    if(localStorage.tipotarjetac == '1'){
                        texto = 'val_pago_credito';
                        codigotexto = 'codigo_promocional_tc';
                    }else{
                        texto = 'val_pago';
                        codigotexto = 'codigo_promocional';
                    }
                    console.log('algo');
                    $('#'+codigotexto).val('');
                    $('#'+texto).attr('enabled','enabled');
//                    var totalactual = localStorage.valpagotemp;
//                    var descuento = localStorage.descuento;
//                    console.log((parseInt(totalactual)+'/'+parseInt(descuento))+'*'+100);
//                    var total = ((parseInt(totalactual)/parseInt(descuento))*100);
                    //$('#'+texto).text(total);
                    $('#'+texto).css({color: "red"});
                    localStorage.codigoPromo = 'null';
                    localStorage.valpagotemp = $('#valviajes_billetera').val();
                    localStorage.descuento = 'null';
                    console.log(localStorage.valpagotemp);
                }
            break;
        case 'nuevapromocion':
                localStorage.ubicacion = 'promociones';
                $.mobile.changePage( "#page_promociones", { transition: "slide"});
                obtenerDatosPromociones();
            break;
        case 'avisollegada':
                localStorage.tipoRuta = "recogida"
                localStorage.idConductorCheto = idrecibido;
                //alert('avisollegada'+localStorage.idConductorCheto);
                if (localStorage.ubicacion!='seguimiento') {
                    $.mobile.changePage( "#page-seguimiento", { transition: "slide"});
                    setTimeout(createMapaant(), 1000);
                    //createMapaant();
                    localStorage.ubicacion='seguimiento';
                }
                console.log(conductor,celular,rutadestino,rutallegada,placa);
            break;
        case 'primerAviso':
                localStorage.tipoRuta = "recogida";
                localStorage.idConductorCheto = idrecibido;
                //alert('primerAviso'+localStorage.idConductorCheto);
                console.log(conductor,celular,rutadestino,rutallegada,placa);
                $.mobile.changePage( "#page-seguimiento", { transition: "slide"});
                createMapaant();
                localStorage.ubicacion='seguimiento';
            break;
        case 'nuevoservicio':
                if(button == 1){
                    localStorage.idrutaLocation = idrecibido;
                    localStorage.idConductorCheto = idrecibido;
                    localStorage.tipoRuta = "recogida";
                    localStorage.disponible = 1;
                    //alert('estoy aqui');
                    obtenerMiruta('recogida','solodatos','aceptado');
                    //aceptarServicio(idrecibido);
                }else{
                        localStorage.idrutaLocation = idrecibido;
                        localStorage.idConductorCheto = idrecibido;
                        localStorage.tipoRuta = "recogida";
                        localStorage.disponible = 1;
                        obtenerMiruta('recogida','solodatos','aceptado');
                        //aceptarServicio(idrecibido);
                    }
                //$("#fuec").show();
                //alert("http://www.eurekadms.co/CHETO/web/ArchivosDescarga/Pdf/pdf.php?id="+localStorage.fuec);
                //$("#btn_fuec").attr('href',direccionservidor+"web/ArchivosDescarga/Pdf/pdf.php?id="+localStorage.fuec);
                $("#fuec_inicio_ruta").attr('href',direccionservidor+"web/ArchivosDescarga/Pdf/pdf.php?id="+localStorage.fuec);
                alerta('La ruta fue aceptada', nombreaplicacion, 'Aceptada');
            break;
        case 'llegadaconductor':
                localStorage.tipoRuta = "recogida";
                localStorage.idConductorCheto = idrecibido;
                //alert('llegadaconductor'+localStorage.idConductorCheto);
                if(button == 1){
                    //generarQR(idrecibido,'primero');
                }
            break;
        case 'llegadaoficina':
                if(button == 1){
                    parar();
                    calificacion(idrecibido);
                }else{
                    parar();
                    calificacion(idrecibido);
                    //$.mobile.changePage( $("#reserva"), "slide", true, true);
                }
            break;
        case 'QRexitoso':
                if(button == 1){
                    $.mobile.changePage( "#reserva", { transition: "slide"});
                    //$.mobile.changePage( $("#reserva"), "slide", true, true);
                }
            break;
        case 'inscribirtarjeta':
                if(button == 1){
                    $.mobile.changePage( "#tarjeta", { transition: "fade"});
                    localStorage.ubicacion = 'tarjeta';

                }else{
                        validarpagoefectivo();
                    }
            break;
        case 'codigomovip':
                if(button == 1){
                    var dd = new Array();
                    dd['reserva'] = localStorage['reservaactiva'];
                    mostrarpagina("factura", dd);
                    $('#codigomovip').dialog("close");
                }else{
                        $('.btn_menu').toggleClass('usuario');
                        mostrarpagina("reserva");
                        $('#codigomovip').dialog("close");
                    }
            break
        case 'servicioaceptado':
                localStorage.tipoRuta = "recogida";
                localStorage.idConductorCheto = idrecibido;
                console.log(conductor,celular,rutadestino,rutallegada,placa);
                $.mobile.changePage( "#page-seguimiento", { transition: "slide"});
                localStorage.ubicacion='seguimiento';
                setTimeout(function(){
                    createMapaant();
                    //horaRecogida();
                },600);
                //alert('primerAviso'+localStorage.idConductorCheto);


            break
        default:

            break;
    }
}
