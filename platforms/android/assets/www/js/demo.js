var calendars = {};
var listado = [];
var reservasel = [];
var eventArray = [];
var detallereserva = [];
var direccionservidor = "https://www.blacktransport.co/";


function detalle_reservas(date){
    //document.getElementById("fecha").innerHTML = "fecha "+date;
    $('ul.justListHist').empty();
    $('.cargando').show();
    fecha_seleccionada = date+" 00:00:00";
    console.log(localStorage.conductor);
    var iduser = null;
    var rolusuario = localStorage.rol;
    if(rolusuario == '3'){
        iduser = localStorage.conductor;
        console.log(iduser);
    }else{
        iduser = localStorage.idusuario;
        console.log(iduser);
    }
    var rolusuario = localStorage.rol;
    var url = direccionservidor + "web/app.php/movilapi/HistoralServicios";
    var datos = '{"iduser" : "'+iduser+'","date" : "'+fecha_seleccionada+'","bandera" : "2", "rol" : "'+rolusuario+'"}';
    console.log(datos);
    var objeto=$.ajax({
      type: "POST",
      url: url,
      data: datos,
      cache: false,
      success: function()
      {
        $('.cargando').hide();
        var json = JSON.parse(objeto.responseText);
        console.log(json);
        for(var i=0;i<json.length;i++)
        {
            var numreserva = json[i]['id'];
            var estado = json[i]['estado'];
            //if(rolusuario == '3'){
                var horasalidacasa = json[i]['horasalidacasa'];
                var horaentradaoficina = json[i]['horaentradaoficina'];
                var horarecogidacasa = json[i]['horarecogidacasa'];
                var horallegadaoficina = json[i]['horallegadaoficina'];
                var nombreconductor = json[i]['nombreconductor'];
                var direccionsalida = json[i]['direccionsalida'];
                var direccionllegada = json[i]['direccionllegada'];
                var placa = json[i]['placa'];
                var precio = json[i]['precio'];
                var nombreruta = json[i]['nombreruta'];
                var tiporuta = null;
                var nombrerutain = null;
                if(nombreruta == null){nombrerutain = 'Pendiente';}else{nombrerutain = nombreruta;}
                if(i==0){tiporuta = 'Recogida';}else if(i == 1){tiporuta = 'Entrega';}
                detallereserva.push({estado: estado, fecha: fecha_seleccionada, horarecogidacasa: horarecogidacasa, horallegadaoficina: horallegadaoficina, horasalidacasa: horasalidacasa, horaentradaoficina: horaentradaoficina, nombreconductor: nombreconductor, direccionsalida: direccionsalida, direccionllegada: direccionllegada, placa: placa, precio: precio, nombreruta: nombreruta});
            //}
            var text = '<h2>'+tiporuta+'</h2>'+nombrerutain+'<a onclick="detalleReserva('+[i]+');"><h2 id="id_historico'+[i]+'">Número de Reserva: '+numreserva+'</h2><h2 id="estado_historico'+[i]+'">Estado: '+estado+'</h2></a><br>'
            $('<li />', {
                      html: text
                  }).appendTo('ul.justListHist');
            $('ul.justListHist').listview('refresh');
        }
        //console.log(detallereserva);
      },
      error: function()
      {
        $('.cargando').hide();
        alerta('Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente','Error');
      }
    });
}
function detalleReserva(id){
    var horasalidacasa,horaentradaoficina,horarecogidacasa,horallegadaoficina,nombreconductor,direccionsalida,direccionllegada,placa,precio,nombreruta,fecha,estado;
    $.mobile.changePage("#detalle_individual_reservas", "slide", true, true);
    console.log(detallereserva);
    $("#resultado_detalle").empty();
    if(detallereserva[id]['horasalidacasa'] != null){
        horasalidacasa = detallereserva[id]['horasalidacasa'];
    }else{horasalidacasa = "Pendiente";}

    if(detallereserva[id]['horaentradaoficina'] != null){
        horaentradaoficina = detallereserva[id]['horaentradaoficina'];;
    }else{horaentradaoficina = "Pendiente";}

    if(detallereserva[id]['horarecogidacasa'] != null){
        horarecogidacasa = detallereserva[id]['horarecogidacasa'];
    }else{horarecogidacasa = "Pendiente";}

    if(detallereserva[id]['horallegadaoficina'] != null){
        horallegadaoficina = detallereserva[id]['horallegadaoficina'];
    }else{horallegadaoficina = "Pendiente";}

    if(detallereserva[id]['nombreconductor'] != null){
        nombreconductor = detallereserva[id]['nombreconductor'];
    }else{nombreconductor = "Pendiente";}

    if(detallereserva[id]['direccionsalida'] != null){
        direccionsalida = detallereserva[id]['direccionsalida'];
    }else{direccionsalida = "Pendiente";}

    if(detallereserva[id]['direccionllegada'] != null){
        direccionllegada = detallereserva[id]['direccionllegada'];
    }else{direccionllegada = "Pendiente";}

    if(detallereserva[id]['placa'] != null){
        placa = detallereserva[id]['placa'];
    }else{placa = "Pendiente";}

    if(detallereserva[id]['precio'] != null){
        precio = detallereserva[id]['precio'];
    }else{precio = "Pendiente";}

    if(detallereserva[id]['nombreruta'] != null){
        nombreruta = detallereserva[id]['nombreruta'];
    }else{nombreruta = "Pendiente";}

    if(detallereserva[id]['fecha'] != null){
        fecha = detallereserva[id]['fecha'];
    }else{fecha = "Pendiente";}

    if(detallereserva[id]['estado'] != null){
        estado = detallereserva[id]['estado'];
    }else{estado = "Pendiente";}

    if(localStorage.rol == '3'){
        $("#resultado_detalle").append("\
            <td>"+fecha+"</td>\n\
            <td>"+nombreruta+"</td>\n\
            <td>"+horasalidacasa+"</td>\n\
            <td>"+horaentradaoficina+"</td>\n\
            <td>"+nombreconductor+"</td>\n\
            <td>"+placa+"</td>\n\
            <td>"+precio+"</td>");
        $( "#tabla_detalle" ).table( "refresh" );

    }else{
        $("#resultado_detalle").append("\
            <td>"+fecha+"</td>\n\
            <td>"+nombreruta+"</td>\n\
            <td>"+horasalidacasa+"</td>\n\
            <td>"+horaentradaoficina+"</td>\n\
            <td>"+nombreconductor+"</td>\n\
            <td>"+placa+"</td>\n\
            <td>"+precio+"</td>");
        $( "#tabla_detalle" ).table( "refresh" );
    }
}

function crearCalendario(){
    // Assuming you've got the appropriate language files,
    // clndr will respect whatever moment's language is set to.
    // moment.locale('ru');
    moment.locale('es');
    // Make sure that your locale is Working correctly
    console.log(moment().format('L'));
    // Here's some magic to make sure the dates are happening this month.
    var thisMonth = moment().format('YYYY-MM');
    console.log(thisMonth);

   // var fechas = listado;
    console.log(localStorage.conductor);
    var iduser = null;
    var rolusuario = localStorage.rol;
    if(rolusuario == '3'){
        iduser = localStorage.conductor;
        console.log(iduser);
    }else{
        iduser = localStorage.idusuario;
        console.log(iduser);
    }
    var url = direccionservidor + "web/app.php/movilapi/HistoralServicios";
    var datos = '{"iduser" : "'+iduser+'","bandera" : "1", "rol" : "'+rolusuario+'"}';
    console.log(datos);
    var objeto=$.ajax({
        type: "POST",
        url: url,
        data: datos,
        cache: false,
        success: function()
        {
            var json = JSON.parse(objeto.responseText);
            for(var i = 0; i < json.length; i++ ){
                var  dato = {date: json[i]['date'], title:'nada', id: i};
                  eventArray.push(dato);
            }
            console.log(eventArray);
            calendars.clndr1 = $('.cal1').clndr({
            events: eventArray,
            daysOfTheWeek: ['Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa','Do'],
            clickEvents: {
                click: function (target) {
                    //$.mobile.changePage( "listado.html", { transition: "", changeHash: true });
                    reservasel = target;
                    if(reservasel['events'].length == 1)
                    {
                        eventArray = [];
                        localStorage.ubicacion = 'detallereserva';
                        $.mobile.changePage( $("#detalle_reservas"), "slide");
                        detalle_reservas(reservasel['events'][0]['date']);
                    }else{
                        alerta('No existen resultados para la selección actual.',nombreaplicacion,'Aceptar');
                    }
                },
                today: function () {
                    console.log('Cal-1 today');
                },
                nextMonth: function () {
                    console.log('Cal-1 next month');
                },
                previousMonth: function () {
                    console.log('Cal-1 previous month');
                },
                onMonthChange: function () {
                    console.log('Cal-1 month changed');
                },
                nextYear: function () {
                    console.log('Cal-1 next year');
                },
                previousYear: function () {
                    console.log('Cal-1 previous year');
                },
                onYearChange: function () {
                    console.log('Cal-1 year changed');
                },
                nextInterval: function () {
                    console.log('Cal-1 next interval');
                },
                previousInterval: function () {
                    console.log('Cal-1 previous interval');
                },
                onIntervalChange: function () {
                    console.log('Cal-1 interval changed');
                }
            },
            multiDayEvents: {
                singleDay: 'date',
                endDate: 'endDate',
                startDate: 'startDate'
            },
            showAdjacentMonths: true,
            adjacentDaysChangeMonth: false
        });
        },
        error: function()
        {
            alerta("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente",nombreaplicacion,"Aceptar");
        }
    });
    //var eventArray  = [{date: '2017-09-26', title: 'Rhonald', id: '1'},{date: '2017-05-25', title: 'Interior y exterior', id: '2'},{date: '2017-05-25', title: 'Polichado nuevo', id: '3'}];
//    for(var i = 0; i < fechas.length; i++ ){
//        dato = {date: fechas[i]['date'],title: fechas[i]['title']};
//        //eventArray.push(dato);
//    }
    //console.log(listado);
    // The order of the click handlers is predictable. Direct click action
    // callbacks come first: click, nextMonth, previousMonth, nextYear,
    // previousYear, nextInterval, previousInterval, or today. Then
    // onMonthChange (if the month changed), inIntervalChange if the interval
    // has changed, and finally onYearChange (if the year changed).

    // Bind all clndrs to the left and right arrow keys
    $(document).keydown( function(e) {
        // Left arrow
        if (e.keyCode == 37) {
            calendars.clndr1.back();
            //calendars.clndr2.back();
            //calendars.clndr3.back();
        }

        // Right arrow
        if (e.keyCode == 39) {
            calendars.clndr1.forward();
            //calendars.clndr2.forward();
            //calendars.clndr3.forward();
        }
    });
}
