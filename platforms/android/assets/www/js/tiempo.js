var centesimas = 0;
var segundos = 0;
var minutos = 0;
var horas = 0;
var minutosR = 0;
var horasR = 0;
var minutofin = '';
var minutopasado = 0;
var horafin = '';
var contadormin = 0;
var contadorhoras = 0;
var nombreaplicacion = 'BLACK';
var direccionservidor = "https://www.eurekadms.co/Black/";
var alertamodal = 0;
var control = false;

function horaRecogida(){//esta función muestra los datos de conductor en pasajero
    var url =  direccionservidor + "web/app.php/movilapi/horaRecogida";
    var dato = '{"iduser" : "'+localStorage.idusuario+'","idruta" : "'+localStorage.idrutanotificado+'"}';
    console.log('entro a horaRecogida: '+dato);
    //alert('localStorage.idrutanotificado '+localStorage.idrutanotificado);
    if(localStorage.idrutanotificado != undefined){
        console.log(control);
        //if(!control){//si no hay cronometro corriendo
        //if(localStorage.cronometro == 'no'){
            //alert(localStorage.cronometro);
        //if(localStorage.cronometro != 'si' || localStorage.cronometro == undefined){
            var ob1 =$.ajax ({
            url: url,
            type: "POST",
            data: dato,
            success: function() { 
                var hora, minutos, fechasalidacasa = null;
                var json = JSON.parse(ob1.responseText);
                //alert(ob1.responseText);
                //alert('json error en horarecogida: '+json.error);
                if(json.error == ''){
                    //alert('succes api horaRecogida');
                    //if(localStorage.cronometro != 'si'){
                        //$("#map-canvas-seguimiento").css({height: "65vh"});
                    //}
                    $("#conten_ruta_asignada_seguimiento").show();
                    $("#conten_sin_ruta").hide();
                    $('#ubicar_cheto').removeClass('oculta');//este elemento no existe
                    console.log(json);
                    var horasplit = json.horasalidacasa.split(':');
                    fechasalidacasa = json.fechasalidacasa;
                    //hora = horasplit[0];
                    //minutos = horasplit[1];
                    console.log(localStorage.cronometro);               
                    //inicio(fechasalidacasa,hora,minutos);                
                    localStorage.conductornotificado = json.conductor;
                    localStorage.celularnotificado = json.celular;
                    localStorage.rutadestinonotificado = json.rutaentrega;
                    localStorage.rutallegadanotificado = json.rutarecogida;
                    localStorage.placanotificado = json.placa;
                    localStorage.dirllegadaPasajero = json.direccionsalida;
                    localStorage.dirdestinoPasajero = json.direccionllegada;
                    localStorage.horasalidacasa = json.horasalidacasa;
                    localStorage.cronometro = 'si';              
                    $("#hora_recogida_casa").text(json.horasalidacasa);
                }else{
                        localStorage.cronometro = 'no';
                        //$("#map-canvas-seguimiento").css({height: "400px"});
                        $("#conten_ruta_asignada_seguimiento").hide();
                        $("#conten_sin_ruta").show();
                        $('#ubicar_cheto').addClass('oculta');
                    }
            },
            error: function(data){
                alerta('Error de conexión horarecogida, intente mas tarde.',nombreaplicacion,'Aceptar');
                console.log('error'+data);
            }
        });   
    //        }else if(localStorage.cronometro == 'si' && localStorage.idrutanotificado){
    //            $("#map-canvas-seguimiento").css({height: "300px"});
    //            $("#conten_ruta_asignada_seguimiento").show();
    //            $('#ubicar_cheto').removeClass('oculta');
    //        }
        //}else{
        //    console.log('el tiempo esta corriendo');
        //}
    }else{
        //alert('no cronometro');
        localStorage.cronometro = 'no';
        //$("#map-canvas-seguimiento").css({height: "400px"});
        $("#conten_ruta_asignada_seguimiento").hide();
        $("#conten_sin_ruta").show();
        //$('#ubicar_cheto').addClass('oculta');
        //$("#cronometro").empty();
        parar();
    }
}

function inicio (fechasalidacasa,hora,minutos) {
    //cordova.plugins.backgroundMode.enable();
//    cordova.plugins.backgroundMode.on('activate', function() {
//        //cordova.plugins.backgroundMode.disableWebViewOptimizations(); 
//        cordova.plugins.backgroundMode.configure({
//        text:'Esta ejecutandose en segundo plano.',
//        resume: true,
//        hidden: true,
//        });
//     });
    var restanteh = null;
    var restantem = null;
    var currentdate = new Date(); 
    var horaactual = currentdate.getHours();
    var minutosactual = currentdate.getMinutes();
    
    var month = ('0' + (currentdate.getMonth() + 1)).slice(-2);
    var date = ('0' + currentdate.getDate()).slice(-2);
    //var date = '28';
    var year = currentdate.getFullYear();
    var shortDate = year + '-' + month + '-' + date;
    
    console.log(fechasalidacasa+' == '+shortDate);
    if(fechasalidacasa == shortDate){
        console.log((hora-1)+' - '+horaactual);
        console.log((parseInt(hora))-parseInt(horaactual));
        restanteh = (parseInt(hora))-parseInt(horaactual);
        //console.log(minutosactual+' - '+minutos+' - '+'59');
        console.log((parseInt(minutos)-parseInt(minutosactual)));
        console.log(restanteh);
        if(restanteh == 0){
            if(minutos>minutosactual){
                console.log(parseInt(minutos)+' - '+parseInt(minutosactual));
                restantem = parseInt(minutos)-parseInt(minutosactual);
            }else{
                console.log(parseInt(minutosactual)+' - '+parseInt(minutos));
                restantem = parseInt(minutosactual)-parseInt(minutos);
            } 
        }else{
            restanteh = restanteh-1;
            restantem = 60-parseInt(minutosactual)+parseInt(minutos);
        }     
        console.log(restantem);        
        if(restantem > 60){
            restanteh = restanteh+1;
            restantem = restantem-60;
            //alert('midia: '+restanteh+':'+restantem);
        }
        console.log('entro a inicio cronometro');
    }else{
        restanteh = 23-parseInt(horaactual)+parseInt(hora);
        restantem = 60-parseInt(minutosactual)+parseInt(minutos);
        console.log('hora: '+restanteh+' min: '+restantem);
        if(restantem > 60){
            restanteh = restanteh+1;
            restantem = restantem-60;
            //alert(restanteh+':'+restantem);
        }
    }    
    console.log(currentdate);
    horasR = restanteh;
    minutosR = restantem;
    console.log(horasR);
    //alert(hora+':'+minutos);
    console.log(fechasalidacasa+' < '+shortDate);
    console.log(+horaactual+' > '+hora);
    if(fechasalidacasa < shortDate){
        accionesParar();
    }else if(fechasalidacasa == shortDate && horaactual > hora){
        accionesParar();
    }else{
        console.log(minutosactual+' == '+minutos);
        console.log(horaactual+' == '+hora);
        console.log(minutosR);
        if(fechasalidacasa == shortDate && horaactual == hora && minutosactual > minutos){
            accionesParar();
        }else{
            console.log('iniciara el cronometro');
            segundos = currentdate.getSeconds();
            console.log(segundos);
            control = setInterval(cronometro,10);
            //ejecuta el cronometro cada 10ms
        }
    }
//	document.getElementById("inicio").disabled = true;
//	document.getElementById("parar").disabled = false;
//	document.getElementById("continuar").disabled = true;
//	document.getElementById("reinicio").disabled = false;
}
function accionesParar(){
    console.log('para el intervalo de la busqueda del conductor');
    $("#cronometro").text('00 min');
    clearInterval(interval);
    parar(); 
    resetRutaUsuario();
    localStorage.calificado = 'si';
    accionesPasajero();
}
function parar() {
    console.log('parando el intervalo control');
    if(control){
        clearInterval(control);
        control = false;
        centesimas = 0;
        segundos = 0;
        minutos = 0;
        horas = 0;
        minutosR = 0;
        horasR = 0;
        minutofin = '';
        minutopasado = 0;
        horafin = '';
        contadormin = 0;
        contadorhoras = 0;
    }
    //cordova.plugins.backgroundMode.disable();
//	document.getElementById("parar").disabled = true;
//	document.getElementById("continuar").disabled = false;
}
function reinicio () {
	clearInterval(control);
	centesimas = 0;
	segundos = 0;
	minutos = 0;
	horas = 0;
	Centesimas.innerHTML = ":00";
	Segundos.innerHTML = ":00";
	Minutos.innerHTML = ":00";
	Horas.innerHTML = "00";
	document.getElementById("inicio").disabled = false;
	document.getElementById("parar").disabled = true;
	document.getElementById("continuar").disabled = true;
	document.getElementById("reinicio").disabled = true;
}

//se encarga de contar hacia atrás para actualizar el cronómetro
function cronometro () {
	if (centesimas < 55) {
		centesimas++;
		if (centesimas < 10) { centesimas = "0"+centesimas }
		//Centesimas.innerHTML = ":"+centesimas;
	}
	if (centesimas == 55) {
		centesimas = -1;
	}
	if (centesimas == 0) {
        segundos ++;
        console.log('paso un segundo en cronometro');
		if (segundos < 10) { segundos = "0"+segundos }
		//Segundos.innerHTML = ":"+segundos;
	}
	if (segundos == 59) {
		segundos = -1;
	}
	if ( (centesimas == 0)&&(segundos == 0) ) {
		minutos++;
		if (minutos < 10) { minutos = "0"+minutos }
		//Minutos.innerHTML = ":"+minutos;
	}
	if (minutos == 59) {
		minutos = -1;
	}
	if ( (centesimas == 0)&&(segundos == 0)&&(minutos == 0) ) {
		horas ++;
		if (horas < 10) { horas = "0"+horas }
		//Horas.innerHTML = horas;
	}        
        if(minutos > contadormin){
                //console.log(horas+' > '+contadorhoras);
//                if(horas > contadorhoras){
//                    horasR = horasR-1;
//                    minutosR = 59;
//                    contadorhoras++;
//                }else 
                //console.log(horasR+' && '+minutosR);
                if(horasR == 0 && minutosR == 1){
                    parar(); 
                    localStorage.cronometro = 'no';
                    if(navigator.platform != 'Win32'){
                            navigator.vibrate(1500);
                    }else{
                        //console.log('vibrando');
                    }                     
                }else if(minutosR == 0){                    
                    horas++;
                    minutosR = 59;
                    horasR = horasR-1;
                    contadorhoras++;
                }else{
                    minutosR = minutosR-1;
                }
                 //console.log('paso un minuto');
                 contadormin++;
            }
        if(horasR<10){
                    horafin = '0'+horasR.toString();
                }else{
                    horafin = horasR;
                }
        if(minutosR<10){
            minutofin = '0'+minutosR.toString();
        }else{    
            minutofin = minutosR;
            //console.log(minutofin);
        }
        if(horasR > 0){
                    if(minutofin != minutopasado){
                        //console.log('cambio de minuto');
                    }
                    $("#cronometro").text(horafin+':'+minutofin).change();
                    minutopasado = minutofin;
                    //console.log(horafin+':'+minutofin+':'+segundos);
                }else{
                    //console.log('minutos actuales: '+minutosR);
                    //console.log('alertamodal == '+alertamodal);
                    if(minutosR == 30 || minutosR == 15 || minutosR == 5){
                        //if(navigator.platform != 'Win32'){
                            if(alertamodal == 0){
                                alertamodal = 1;
                                //alert(minutosR+':'+segundos+' alertamodal = '+alertamodal);
                                setTimeout(function(){
                                    //alert('seteando alertamodal = 0');
                                    alertamodal = 0;
                                },63000);
                                //console.log('entro al else 274 minuto fin = '+minutofin);
                                $("#cronometro").text(minutofin+' min');
                                navigator.vibrate(1500);
                                alerta('Faltan '+minutosR+' minutos.',nombreaplicacion,'Aceptar');
                            }
                            
                        //}
                    }else if(minutosR == 00){
                        console.log('dejando en ceros el contador');
                        $("#cronometro").text('00 min');
                        parar();                        
                    }else{
                        //console.log(horafin+':'+minutofin+':'+segundos);
                        //console.log('entro al else 284 minuto fin = '+minutofin);
                        $("#cronometro").text(minutofin+' min');
                    }                                
                    //console.log('00:'+minutofin+':'+segundos);
                }
}
document.addEventListener("deviceready", function() {
    document.addEventListener("resume", function(){
        console.log('volvio a forregroun');
        //horaRecogida();
    }, false);
    document.addEventListener("pause", function(){
        console.log('paso a background');
        //parar();
    }, false);
}, false);