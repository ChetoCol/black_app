var num = '';
var diasrecogida = new Array();
var y = [];
var latSalida,longSalida,latLlegada,longLlegada,horaentrada,diarecogida,otrosDias,cordSalida,cordLlegada,userid,horacompleta;
var nombreaplicacion = "BLACK";
var direccionservidor = "https://www.eurekadms.co/Black/";
function obtenerDatosReserva(){//funcion de 'confirmar reserva'
    $('.cargando').show();
    dirLLegada = localStorage.dirLlegada;
    dirSalida = localStorage.dirSalida;
    diarecogida = localStorage.fechaRecogida;
    horaentrada = localStorage.idhoraEntrada;
    cordSalida = localStorage.aksalida;
    cordLlegada = localStorage.akllegada;
    userid = localStorage.idusuario;
    horacompleta = '2017-01-01 '+horaentrada+':00';
    
    
//    latSalida = $('#latitudsalida').val();
//    longSalida = $('#longitudsalida').val();
//    latLlegada = $('#latitudllegada').val();
//    longLlegada = $('#longitudllegada').val();
//    cordSalida = latSalida+','+longSalida;
//    cordLlegada = latLlegada+','+longLlegada;
    
//    dirSalida = $('#direccionsalida').val();
//    dirLLegada = $('#direccionllegada').val();
//    horaentrada = $('#horaentrada').val();
//    diarecogida = $('#diarecogida').val();
//    otrosDias = $('#diasrecogida').val();
    
     
    
    y = {userid : ""+userid+"", aksalida: ""+cordSalida+"", akllegada: ""+cordLlegada+"", dirsalida: ""+dirSalida+"", dirllegada: ""+dirLLegada+"", npasajeros: 2, horaentradaoficina: ""+horaentrada+"",diasrecogida: []};
    confirmarReservaAction();
};

function confirmarReservaAction(){//si hay saldo abre popup de confirmar
    $('.cargando').hide();
    var pasajeros = $('#total_tickets').val();
    console.log(pasajeros);    
    if(sinsaldo == 1){
        alerta('Lo sentimos, usted no cuenta con saldo suficiente para continuar.',nombreaplicacion,'Aceptar');
    }else{
        sinsaldo = null;
        if(pasajeros > 0){
            abrirPopup('confirmar_resera');    
            $('#pasajes_descontar').text(pasajeros);
            localStorage.totalPasajes = pasajeros;
        }else{
            alerta('Por favor seleccione la cantidad de tickets a descontar',nombreaplicacion,"Aceptar");
        }
    }
}

function reserva_enviarReserva2() { //para black 08-2018
    var dia = null;
    $('.cargando').show();
    var o= latOrigen+','+longOrigen;
    var d= latDestino+','+longDestino;
    datis='{"userid":"'+localStorage.idusuario+'","app":"1","aksalida":"'+o+'","akllegada":"'+d+'","dirsalida":"'+dirOrigen+'","dirllegada" : "'+dirDestino+'","npasajeros" : "2","horaentradaoficina" : "'+horaServ+'","tiporuta" : "0","diasrecogida" : [[{"dia":"'+fechaServ+'","pasajeros":"'+n_pasajeros+'"}]]}';
    console.log('entro a reserva_enviarreserva2'+datis);
    //{"userid":"6523","idcliente":"2","idcentrocostos":"41","aksalida":"4.5885599,-74.15574403","akllegada":"4.6682441,-74.0999524","dirsalida":"Cl. 68","dirllegada":"Jardotanico","npasajeros":"2","horaentradaoficina":"1", "tiporuta": "0","diasrecogida":[[{"dia":"","pasajeros":"2"}]]}
    $.ajax({data:datis,
        url:direccionservidor+"web/app.php/movilapi/reservasmovil",
        type:"POST",
        success:exito_reservaCreada,
        }).fail( function(){
            alerta("Error 23: Pedir Servicio",nombreaplicacion,"Aceptar");
            $('.cargando').hide();
        }
        );   
}

function exito_reservaCreada(response){
    $('.cargando').hide();
    if (response!=undefined) {
        if (response.error=="") {
            alerta("Tu reserva fue creada con éxito",nombreaplicacion,"Aceptar");
        }else if (response.error=="22") {
            alerta("Aún no hemos encontrado un conductor para tu servicio, por favor espera",nombreaplicacion,"Aceptar");
            alerta("Tu reservación fue creada con éxito",nombreaplicacion,"Aceptar");
        }else if (response.error=="21") {
            alerta("No estás asociado todavía, por favor llama a soporte",nombreaplicacion,"Aceptar");
        }
    }
    //reset a todos los divs e inputs
    $('#latitudsalida').val("");
    $('#longitudsalida').val("");
    $('#direccionsalida').val("");
    $('#diarecogida1').val("");
    $('#horaservicio1').val("");
    $('#direccionllegada').val(""); 
    $('#latitudllegada').val(""); 
    $('#longitudllegada').val(""); 
    $('#numeropasajeros').val("");
    //$.mobile.changePage( "#page-seguimiento", { transition: "slide"});
    $("#dir_recogida_edit").text("");
    $("#dir_entrega_edit").text("");
    $("#hora_entrega_edit").text("");
    $("#pasa_entrega_edit").text("");
    //reset a variables globales
    longOrigen=null;
    latOrigen=null;
    longDestino=null;
    latDestino=null;
    dirOrigen=null;
    dirDestino=null;
    fechaServ=null;
    n_pasajeros=null;
    horaServ=null;

    //localStorage.ubiacion = 'main';
}

function enviarReserva(){ //da en descontar pasaje desde popup
    var dia = null;
    $('.cargando').show();
    console.log(diasrecogida);
    console.log(localStorage.fechaRecogida);
    if(diasrecogida.length == 0){
        var d = new Date(); 
        var mes = (parseInt(d.getMonth())+1);
        var diafecha = d.getDate();
        if(mes < 10){
            mes = '0'+mes;
        }else{
            mes = mes;
        }
        if(diafecha < 10){
            diafecha = '0'+diafecha;
        }else{
            diafecha = diafecha;
        }
        //var fecha = d.getFullYear()+"-"+(parseInt(d.getMonth())+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":00";
        var fecha = d.getFullYear()+"-"+mes+"-"+diafecha;
        console.log(fecha+' != '+localStorage.fechaRecogida);
        if(fecha == localStorage.fechaRecogida){
//            var despues = new Date(d.getTime() + 24*60*60*1000);
            var diasiguiente = parseInt(diafecha)+1;
            if(diasiguiente < 10){diasiguiente = '0'+diasiguiente;}
            var despues = d.getFullYear()+"-"+mes+"-"+diasiguiente;
            dia =  despues;
            console.log('igual');
            console.log(dia);
        }else{
            console.log('diferente');
            dia = localStorage.fechaRecogida; 
        } 
        console.log(dia);
        console.log(new Date(d.getTime() + 24*60*60*1000));
        var pasajeros = $('#total_tickets').val();
        diasrecogida = [{dia: ""+dia+"","pasajeros": ""+pasajeros+""}];
        console.log(diasrecogida);
    }
    console.log(diasrecogida);
    //var y = '{"userid" : "'+userid+'", "aksalida": "'+cordSalida+'", "akllegada": "'+cordLlegada+'", "npasajeros": 2, "horaentradaoficina": "'+horacompleta+'","diasrecogida": "'+diasrecogida+'"}';
    if(pasajeros == 0){
        alerta('Por favor seleccione la cantidad de tickets que desea reservar.',nombreaplicacion,'Aceptar');
        $('.cargando').hide();
        console.log(y);
    }else{
        var url = direccionservidor + "web/app.php/movilapi/reservasmovil";
        y.diasrecogida.push(diasrecogida);
        var datos = JSON.stringify(y);
        console.log(datos);
        console.log(url);
        console.log(y);
        var objeto=$.ajax
        ({
          type: "POST",
          url: url,
          data: datos,
          cache: false,
          success: function()
          {
              var json = JSON.parse(objeto.responseText);
              //var json = objeto.responseText;
              console.log(json);
              //alert(json.error);
              if(json.error == "")
                {
                    var idreserva = json.idreserva;
                    alerta('La reserva del servicio fue creada con éxito.',nombreaplicacion,'Aceptar');
                    localStorage.idreservaCreada = idreserva;
                    paginaConfirmarReserva();
//                    $.mobile.changePage( "#page_confirmar_reserva", { transition: "slice"});
                    //localStorage.idpago = json.idpago;
                    //iniciopago(json.precio);
                    //navigator.notification.alert("Parada guardada con \u00e9xito" , null, nombreaplicacion, "Aceptar"); 
                    $('.cargando').hide();
                }
                else
                {
                    alerta('Error al crear la reserva',nombreaplicacion,"Aceptar");
                  //navigator.notification.alert(json.error , null, nombreaplicacion, "Aceptar");
                  $('.cargando').hide();
                }            
          },
          error: function()
          {
              alerta('Error de conexión 7, por favor valide e intente de nuevo',nombreaplicacion,"Aceptar");
              console.log("Ocurrió un error al crear la reserva.");
              $('.cargando').hide();

          }
        });
    }
}
function paginaConfirmarReserva(tipo){
    console.log(tipo);
    console.log(localStorage.idrutanotificado);
    if(localStorage.idrutanotificado != undefined && localStorage.idrutanotificado != 'null'){
        $(".btncancelar_reserva").addClass('ui-disabled');
        //$("#ver_micheto_seguimiento").attr('href','#page_confirmacion_reserva');
    }
   if(tipo != 'home'){
       console.log(localStorage.totalPasajes);
       if(localStorage.totalPasajes != undefined && localStorage.totalPasajes != '' && localStorage.totalPasajes != 'null' && localStorage.totalPasajes != '0'){
            $.mobile.changePage( "#page_confirmar_reserva", { transition: "slide"});
            $('#reserva_recogida').text(localStorage.dirSalida);
            $('#reserva_entrega').text(localStorage.dirLlegada);
            $('#total_ticketes_comprados').text(localStorage.totalPasajes+' Tickets');
            $(".btncancelar_reserva").attr('id',localStorage.idreservaCreada);
        }else{
            alerta('Su reserva no esta finalizada, para continuar finalice, gracias.',nombreaplicacion,'Aceptar');
        }
   }
   
}
function iniciopago(total){    
    localStorage.ubicacion = 'pagos';
    localStorage.codigoPromo = "null";
    localStorage.valpago = total;
    $('#val_pago').text('$ '+format(total));
    $('.cargando').hide();

}
//funcion que ingresa el numeor uno en la cantidad de pasajeros cuando selecciono un dia de recogida
function numCero(check,num){    
    obtenerDatosReserva();     
    console.log(y);
    var cuentacheck = '';
    if( $('#'+check).prop('checked') ) { 
        //diasrecogida = [];
        //alert('Seleccionado: '+num);
        $('#'+num).text(1);
        $("input:checkbox:checked").each(function(e) {
            cuentacheck = e;
        });
        
        console.log(cuentacheck);
        console.log(diasrecogida);
        if(cuentacheck == 1){
            //diasrecogida = [];
            for(var i= 0; i < diasrecogida.length; i++){
                var numpas = diasrecogida[i]['pasajeros'];
                var diarec = diasrecogida[i]['dia'];
                var indice = i;
                console.log('indice: '+indice+' numeropasajeros: '+numpas+' dias: '+diarec);
            }
            console.log('check = 1');
            $("input:checkbox:checked").each(function(e) {
                var valor = $(this).val();
                console.log(e);
                console.log(diasrecogida);
                //console.log(diasrecogida.e.dia);
                //if(valor == diasrecogida[e]['dia']){console.log(valor)}
                if(indice != e){
                    var res = {dia: ""+valor+"","pasajeros": 1};
                    diasrecogida.push(res);
                    console.log(diasrecogida);
                }
            });
        }else{
            console.log('check != 1');
            var valor = $('#'+check).val();
            var res = {dia: ""+valor+"","pasajeros": 1};
            diasrecogida.push(res);        
        }
        console.log(diasrecogida);
//        $("input:checkbox:checked").each(function(e) {
//            var valor = $(this).val();
//            console.log(e);
//            console.log(diasrecogida);
//            //console.log(diasrecogida.e.dia);
//            //if(valor == diasrecogida[e]['dia']){console.log(valor)}
//            var res = {dia: ""+valor+"","pasajeros": 1};
//             diasrecogida.push(res);
//             console.log(diasrecogida);
//        });
    }else{
        //alert('no seleccionado num: '+num);
        var check = $('#'+check).val();
        $('#'+num).text(0);
        for(var i = 0; i<diasrecogida.length; i++){            
            console.log(diasrecogida[i]['dia']+' -- '+check);
            if(diasrecogida[i]['dia'] == check){
                console.log('quitando');
                diasrecogida.splice(i,1);
            }
        }        
        //console.log('quitar');
    }
    console.log(diasrecogida);    
    y.diasrecogida.push(diasrecogida);    
    console.log(y);
    //y = '{"userid" : "'+userid+'", "aksalida": "'+cordSalida+'", "akllegada": "'+cordLlegada+'", "npasajeros": 2, "horaentradaoficina": "'+horacompleta+'", "diasrecogida": "'+[]+'"}';
    //console.log(diasrecogida);
}
//funcion que adiciona cantidad de pasajeros al oprimir mas o menos
function cantidadPasajeros(tipo,id,check){
    obtenerDatosReserva(); 
    //alert(id);
    //console.log(id);
    num = $('#'+id).text();  
    //alert(num);
    num = parseInt(num);
    //console.log(num);
    //console.log(tipo);
    if(tipo == 'mas'){
         num += 1;
    }else if(tipo == 'menos'){
         num -= 1;
    } 
    //console.log(num);
    if(num < 0 ){
        num = 0;
    }
    console.log(diasrecogida);    
    //alert(check);
    if( $('#'+check).prop('checked') ) {
        //alert('Seleccionado');
        console.log(diasrecogida.length);
        var fecha = $('#'+check).val();
        $('#'+id).text(num);        
        if(diasrecogida.length > 0){
            for(var i = 0; i<diasrecogida.length; i++){            
                console.log(diasrecogida[i]['dia']+' -- '+fecha);
                if(diasrecogida[i]['dia'] == fecha){
                    console.log('operacion');
                    console.log(diasrecogida[i]['pasajeros']);
                    console.log(num);
                    diasrecogida[i]['pasajeros'] = num;
                    if(num == 0){
                        //alert('num0 #'+check);
                        $('#'+check).prop('checked',false);
                        diasrecogida.splice(i,1);
                    }
                    //diasrecogida.splice(i,1);
                }
            }            
        }else{
            console.log('entro');
            num = 2
            var res = {dia: ""+fecha+"","pasajeros": ""+num+""};
            diasrecogida.push(res);            
            console.log(diasrecogida);
        }
    }else{
        
        //alert('no seleccionado');
    }
    y.diasrecogida.push(diasrecogida);
    console.log(diasrecogida);
    console.log(y);
}
function valFechaReserva(res){
    var fecha = res+' 00:00:00'; 
    var datos = '{"userid" : "'+localStorage['idusuario']+'" , "fecha" : "'+fecha+'"}';
    var url = direccionservidor + "web/app.php/movilapi/validarFechaReserva";
    console.log(datos);
    var objeto=$.ajax
    ({
      type: "POST",
      url: url,
      data: datos,
      cache: false,
      success: function()
      {
          var json = JSON.parse(objeto.responseText);
          console.log(json);
          if(json.error == "")
            {
                //$('#btn_pagar_reserva').show();
                console.log('dia libre');
                localStorage.reservabloqueada = 'null';
                $('#continuarReserva').removeClass('ui-disabled');
                //diasRecogidas(res);
            }else{
                    $('#btn_pagar_reserva').hide();
                    alerta("Lo sentimos, usted ya tiene una reserva activa para la fecha seleccionada.",nombreaplicacion,"Aceptar");
                    localStorage.reservabloqueada = 'si';
                    //history.back();
                }
      },
      error: function()
      {
          alerta("Error en la conexion 4, valide e intente de nuevo", nombreaplicacion,"Aceptar");
          console.log("Ocurrio un error al validar fecha seleccionada para la reserva.");
          $('.cargando').hide('active');
      }
    });        
}
//funcion que me carga los siguientes 7 dias que puedo recoger un pasajero.
function diasRecogidas(fecha){
    $('#diasrecogidas').empty();  
    $('#totalpasajeros').empty();
    //alert(fecha);
    var arraydiarecogida = fecha.split('-');
    var diarecogidaint = parseInt(arraydiarecogida[2]);
    var mesrecogidaint = parseInt(arraydiarecogida[1]);
    var anorecogidaint = parseInt(arraydiarecogida[0]);
    var diarecogidastr = arraydiarecogida[2];
    var mesrecogidastr = arraydiarecogida[1];
    var anorecogidastr = arraydiarecogida[0];
    var contenido = '';
    var pasajeros = '';
    var diassiguientes = '';
    var diarecogidastr = '';
    var nuevomes = '';
    var dias=[31,29,31,30,31,30,31,31,30,31,30,31];
    for(var j=0; j<dias.length; j++){
        if(mesrecogidaint-1 == j){
           var maximodia = dias[j]; 
        }
    }
    console.log(maximodia);
    for(var i=0; i<7; i++){       
        diassiguientes = diarecogidaint++;
        if(diassiguientes == maximodia){
            diarecogidaint = 1;
            nuevomes = i+1;
        }
        console.log(nuevomes+' == '+i);
        console.log('mesactual: '+mesrecogidastr);
        if(nuevomes === i){ 
            mesrecogidastr = mesrecogidaint+1;
            mesrecogidastr = '0'+mesrecogidastr.toString();
            console.log('cambio mes: '+mesrecogidastr);
        }
        if(diassiguientes <= 9){
           diarecogidastr = '0'+diassiguientes.toString();
        }else{           
            diarecogidastr = diassiguientes.toString();
        }
        console.log(diarecogidastr);
        if(diassiguientes<10){diassiguientes = '0'+diassiguientes}
        var mas = '"mas"';
        var menos = '"menos"';
        var buscado = '"numero'+diassiguientes+'"';
        var checkbox = '"dia'+diassiguientes+'"';
        contenido += "<input type='checkbox' class='diasrecogida' name="+checkbox+" id="+checkbox+" value='"+diarecogidastr+"-"+mesrecogidastr+"-"+anorecogidastr+"' onclick='numCero(this.id,"+buscado+")'>"+diarecogidastr+"";    
        pasajeros += "<div class='vistalineal btnes_tot_pasajeros'><div class='vistalineal' id='mas"+diassiguientes+"' onclick='cantidadPasajeros("+mas+","+buscado+","+checkbox+");'><img src='img/iconos/mas.png' alt='mas'/></div><div class='vistalineal numero' id='numero"+diassiguientes+"'>0</div><div class='vistalineal menos' id='menos"+diassiguientes+"' onclick='cantidadPasajeros("+menos+","+buscado+","+checkbox+");'><img src='img/iconos/menos.png' alt='mas'/></div></div>";
    }
    $('#diasrecogidas').append(contenido);
    $('#totalpasajeros').append(pasajeros);
    var primerdia = "#dia"+arraydiarecogida[2];
    var primernum = "#numero"+arraydiarecogida[2];
    console.log(primerdia);
    console.log(primernum);
    $(primerdia).prop('checked', true);
    $(primernum).text(1);
    //console.log(arraydiarecogida[2]);
}
//funcion que encoge el mapa cuando voy a hacer la reserva.
/*function enviarReserva(){
    obtenerDatosReserva();
    //console.log(diasrecogida);
    if(diasrecogida.length == 0){
        var dia = $('.diasrecogida').val(); 
        diasrecogida = [{dia: ""+dia+"","pasajeros": 1}];
        //console.log(diasrecogida);
    }
    console.log(diasrecogida);
    //var y = '{"userid" : "'+userid+'", "aksalida": "'+cordSalida+'", "akllegada": "'+cordLlegada+'", "npasajeros": 2, "horaentradaoficina": "'+horacompleta+'","diasrecogida": "'+diasrecogida+'"}';
    
    var url = direccionservidor + "web/app.php/movilapi/reservasmovil";
    y.diasrecogida.push(diasrecogida);
    var datos = JSON.stringify(y);
    console.log(datos);
    console.log(url);
    console.log(y);
    var objeto=$.ajax
    ({
      type: "POST",
      url: url,
      data: datos,
      cache: false,
      success: function()
      {
          var json = JSON.parse(objeto.responseText);
          //var json = objeto.responseText;
          console.log(json);
          //alert(json.error);
          if(json.error == "")
            {
              alert('exitoso');
              //navigator.notification.alert("Parada guardada con \u00e9xito" , null, nombreaplicacion, "Aceptar"); 
            }
            else
            {
                alert('error');
              //navigator.notification.alert(json.error , null, nombreaplicacion, "Aceptar");
            }
      },
      error: function()
      {
          navigator.notification.alert("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente" , null, nombreaplicacion, "Aceptar");
          $('.cargando').toggleClass('active');

      }
    });
}*/



