var geocoder;
var marker;
var latLng;
var map;
var latitud = 0;
var longitud = 0;
var rutaconductor = '';
var ak = '';
var mapausuario = '';
var tiporuta = '';
var puntoDestino= '';
var coordenaActual = "";
var divMapa = "";
var datoGPS = "";
var casoGps = "";
var direccionservidor = "https://www.blacktransport.co/";
var nombreaplicacion = "BLACK";
var estadocond = null;
var ciudadUbicacion="";
//funcion para crear el mapa
function crearMapa(json,tipo,aceptado){
    //$('.cargando').show();
    console.log('entro a crearMapa: json: '+json+' tipo: '+tipo+' aceptado: '+aceptado);
    //localization();
    tiporuta = tipo;
    if(json === 'mipos'){
        mapausuario = 'si';
        insertarMapa();
    }else if(json === 'onlyPoint'){
        mapausuario = 'punto';
        puntoDestino = tipo;
        insertarMapa();
    }else if(json === 'principal'){
        insertarMapa();
        mapausuario = 'conductor';
        estadocond = aceptado;
    }else{
        mapausuario = 'no';
        rutaconductor = json;
        console.log(rutaconductor);
        var resheight=$( window ).height();
        console.log(resheight);
        insertarMapa();
        $('#footer-tablero').css("bottom", '-80px');
        $('.tablero_ruta').append('<p>'+localStorage.nombreRuta+'</p>');
    }
}

function insertarMapa() {
    //var opt = { maximumAge: 60000, timeout: 60000, enableHighAccuracy: true };
    console.log("entro a  insertarMapa");
    console.log("vista: " + localStorage.ubicacion);
    //navigator.geolocation.getCurrentPosition(cd, onError, opt);
    if (localStorage.rol == '4') {//si esta en pasajero usa la navegacion tradicional
        var opt = { maximumAge: 60000, timeout: 60000, enableHighAccuracy: true };
        navigator.geolocation.getCurrentPosition(cd, onError, opt);
    } else {//si esta en conductor

        if (actualPos != null) {
            cd(actualPos);
        } else {
            console.log('no hay actualpos');
            setTimeout(function () { insertarMapa(); }, 3000);
        }
    }
}
//calback e la geolocalizacion Diana
function cd(posicion) {
    console.log("entro a cd");
    if (localStorage.rol == '3') {
        latitud = posicion.split(',')[0];
        longitud = posicion.split(',')[1];
    } else {
        latitud = posicion.coords.latitude;
        longitud = posicion.coords.longitude;
    }
    //latitud = posicion.coords.latitude;
    //longitud = posicion.coords.longitude;
    console.log('mapausuario: '+mapausuario);
    if (mapausuario === 'si') {
        divMapa = document.getElementById("map-canvas-user");
        document.getElementById("latitudsalida").value = latitud;
        document.getElementById("longitudsalida").value = longitud;

        document.getElementById("latitudllegada").value = latitud;
        document.getElementById("longitudllegada").value = longitud;

    }
    else if (mapausuario === 'punto') {
        divMapa = document.getElementById("map-canvas1");
        var coordenada = puntoDestino.split(",");
        latitud = coordenada[0];
        longitud = coordenada[1];
        rutaconductor = '';
    } else if (mapausuario == 'conductor') {
        divMapa = document.getElementById("map-canvas-conductor");
    }
    setTimeout(function () { initialize(); }, 1000);

}

//funcion de error de la geolocalizacion
function onError(error){
   //alert('code: ' +error.code+ '\n' + 'message: ' +error.message+ '\n');
   console.log(error.message);
    switch(error.code)
        {
            case error.PERMISSION_DENIED: alert("El usuario no permite compartir datos de geolocalizacion");
            break;

            case error.POSITION_UNAVAILABLE: alert("Imposible detectar la posicio actual");
            break;

            case error.TIMEOUT: alert("La posicion debe recuperar el tiempo de espera mapas");
                insertarMapa();
            break;

            default: console.log("Error desconocido");
                insertarMapa();
            break;
        }
//  if(navigator.platform == 'Win32'){
//      alert('Es necesario habilitar los servicios de ubicaci\u00f3n para un correcto funcionamiento');
//  }else{
//      //alert('Es necesario habilitar los servicios de ubicaci\u00f3n para un correcto funcionamiento');
//    window.location.reload();
    //crearMapa('mipos');
     /*navigator.notification.vibrate(1000);
     navigator.notification.alert(
    'Es necesario habilitar los servicios de ubicaci\u00f3n para un correcto funcionamiento',
    function(){
        //mostrarpagina("homec");
        },
        nombreaplicacion,
        'Aceptar'
    );*/
  //}
}

// INICiALIZACION DE MAPA
function initialize(){
    console.log("entro a initialize");
    console.log("ini vista: "+localStorage.ubicacion);
//    var mapcanvas = document.createElement('div');
//    mapcanvas.id = 'mapcanvas';
//    var altura = ($( window ).height());
//    mapcanvas.style.height = '100%'; //+ 'px';
//    mapcanvas.style.width = '100%';
//    $('#mapa').empty();
//    document.querySelector('#mapa').appendChild(mapcanvas);

    geocoder = new google.maps.Geocoder();
    console.log(rutaconductor);
    //alert('entro a initialize');
    if(rutaconductor && rutaconductor.length > 0){
        console.log('cargará ruta');
        var marcadores = [];
        var linea = [];
        for(var i = 0; i<rutaconductor.length; i++){
            var direccion = rutaconductor[i]['direccion'];
            var direccionsplit = direccion.split(",");
            latitud = direccionsplit[0];
            longitud = direccionsplit[1];
            var titulo = 'parada '+[i+1];
            var ruta = [titulo, latitud, longitud];
            var lat = direccionsplit[0];
            var long = direccionsplit[1];
            var linearuta = new google.maps.LatLng(lat,long);
            marcadores.push(ruta);
            linea.push(linearuta);
            if(i == '2'){
                console.log('i == 2');
                //var latitudcent = lat;
                //var longitudcent = long;
                console.log(lat,long);
            }else{console.log('i != 0');}
        }
        console.log(linea);
        //console.log(latitudcent,longitudcent);
        latLng = new google.maps.LatLng(lat,long);
        console.log(latLng);
        map = new google.maps.Map(document.getElementById("map-canvas"),
            {
              zoom:16,
              center: latLng,
              panControl: false,
              zoomControl: false,
              mapTypeControl: false,
              scaleControl: false,
              streetViewControl: false,
              overviewMapControl: false,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        console.log(marcadores);
        var infowindow = new google.maps.InfoWindow();
        marker, i;
        if(tiporuta=='entrega')
        {
          var icono = 'img/iconos/marker_office.png';
        }
        else
        {
          var icono = 'img/iconos/marker_home.png';
        }
        for (i = 0; i < marcadores.length; i++) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
              icon: icono,
              map: map
            });
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                infowindow.setContent(marcadores[i][0]);
                infowindow.open(map, marker);
              }
            })(marker, i));
          }
        var lineas = new google.maps.Polyline({
            path: linea,
            map: map,
            strokeColor: '#222000',
            strokeWeight: 4,
            strokeOpacity: 0.6,
            clickable: false
        });
        $('.cargando').hide();
            //latLng = new google.maps.LatLng(latitud,longitud);
     }else if(mapausuario === 'punto'){
        //alert('minimapa');
        //alert(latitud+' '+longitud);
         map = new google.maps.Map(divMapa,
            {
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: false,
                mapTypeControl: false,
                streetViewControl: false
            });
        latLng = new google.maps.LatLng(latitud,longitud);
            // CREACION DEL MARCADOR
            marker = new google.maps.Marker(
            {
              position: latLng,
              map: map,
          //    icon: 'img/ubicacion.png'
            });
            map.setCenter(latLng);
            //alert('punto');
        validarDistancias();
        $('.cargando').hide();
     }else{
         console.log('no cargará ruta');
         console.log(localStorage.ubicacion);
         latLng = new google.maps.LatLng(latitud,longitud);
         console.log(latLng);
         map = new google.maps.Map(divMapa,
            {
              zoom:17,
              center: latLng,
              panControl: false,
              zoomControl: false,
              mapTypeControl: false,
              scaleControl: false,
              streetViewControl: false,
              overviewMapControl: false,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            // CREACION DEL MARCADOR
            if (localStorage.rol=='3') {//si es el conductor el marcador no se puede arrastrar
                var dragg=false;
            }else{
                var dragg=true;
            }
            marker = new google.maps.Marker(
            {
              position: latLng,
              title: '',
              map: map,
              draggable: dragg
          //    icon: 'img/ubicacion.png'
            });
            map.setCenter(latLng);
            // Agrega el control de ubicacion
            //alert('geocodeposition');
            //alert(mapausuario)
            if(mapausuario == 'conductor'){
                setTimeout(function(){
                    //alert('existe idruta ? : '+localStorage.idruta);
                    //alert('aceptado ? : '+estadocond);
                    if(!localStorage.idruta){
                        //alert('idruta no hay: '+localStorage.idruta);
                        mostrarMiruta('aceptado');
                    }else{//posible solucion
                        if(localStorage.serivioIniciado=='si'){
                            mostrarMiruta('iniciado','true');
                        }else{
                            //alerta('¡Tiene un nuevo servicio asignado!',nombreaplicacion,'Aceptar');
                            var mensajito='¡Tienes un nuevo servicio asignado!';
                            navigator.notification.alert(
                                mensajito,  // mensaje
                                null,     // callback
                                nombreaplicacion,   // titulo
                                'Aceptar' // nombre de boton
                            );
                            mostrarMiruta('aceptado');
                        }
                        /*if(estadocond == 'aceptado'){
                            //alert('ahora si entro donde debe');
                            mostrarMiruta('aceptado');
                        }else{
                            mostrarMiruta('iniciado','true');
                        }*/
                    }
                },200);
                console.log('mapa conductor');
            }else{
                geocodePosition(latLng);

                // Permito los eventos drag/drop sobre el marcador
                google.maps.event.addListener(marker, 'dragstart', function() {
                  updateMarkerAddress('Arrastrando...');
                });

                google.maps.event.addListener(marker, 'drag', function() {
                  updateMarkerStatus('Arrastrando...');
                  updateMarkerPosition(marker.getPosition());
                });

                google.maps.event.addListener(marker, 'dragend', function() {
                  updateMarkerStatus('Calculando...');
                  geocodePosition(marker.getPosition());
                });
            }
            $('.cargando').hide();
     }
     //alert('finalizo initialize');
//  console.log(localStorage.rol);
//  console.log(localStorage.ubicacion);
}

// ESTA FUNCION OBTIENE LA DIRECCION A PARTIR DE LAS COORDENADAS POS
function geocodePosition(pos){
    //alert('entro geocodeposition');
    geocoder.geocode({
          latLng: pos
        },
        function(responses){
            if (responses && responses.length > 0){
            var direcciones = responses[0].formatted_address.split("-");
            ciudadUbicacion = responses[4].formatted_address.split("-");
            updateMarkerAddress(direcciones[0] + " - ");
            }else{
                updateMarkerAddress('No puedo encontrar esta direcci\u00f3n.');
            }
        }
    );
}

//funcion que actualiza la direccion del marcador
function updateMarkerAddress(str){
    console.log(str);
    console.log(ak);
    if(ak == 'salida'){
        console.log('salida');
        document.getElementById("direccionsalida").value = str;
    }else if(ak == 'llegada'){
        console.log('llegada');
        document.getElementById("direccionllegada").value = str;
    }else if(ak == ''){
//        document.getElementById("direccionsalida").value = str;
//        document.getElementById("direccionllegada").value = str;
    }
}

//funcion que actualiza el estado del marcador.
function updateMarkerStatus(str){
    if(ak == 'salida'){
        console.log('salida');
        document.getElementById("direccionsalida").value = str;
    }else if(ak == 'llegada'){
        console.log('llegada');
        document.getElementById("direccionllegada").value = str;
    }

}

// RECUPERO LOS DATOS LON LAT Y DIRECCION Y LOS PONGO EN EL FORMULARIO
function updateMarkerPosition (latLng){
    var long,lat = '';
    console.log(ak);
    if(ak == 'salida'){
        long = 'longitudsalida';
        lat = 'latitudsalida';
    }else if(ak == 'llegada'){
        long = 'longitudllegada';
        lat = 'latitudllegada';
    }
    console.log(lat+long);
    document.getElementById(long).value =latLng.lng();
    document.getElementById(lat).value = latLng.lat();
    latitud = document.getElementById(lat).value;
    longitud = document.getElementById(long).value;
}

function cambioDireccion(tipo)
{
  codeAddress(tipo);
}
// ESTA FUNCION OBTIENE LA DIRECCION A PARTIR DE LAS COORDENADAS POS
//function geocodePosition(pos){
//    geocoder.geocode({
//        latLng: pos
//    },
//    function(responses){
//            if (responses && responses.length > 0){
//              var direcciones = responses[0].formatted_address.split("-");
//              updateMarkerAddress(direcciones[0] + " - ");
//            }else{
//              updateMarkerAddress('No puedo encontrar esta direcci\u00f3n.');
//            }
//        }
//    );
//}

// OBTIENE LA DIRECCION A PARTIR DEL LAT y LON DEL FORMULARIO
function codeLatLon()
{
  str= document.getElementById("longitud").value+" , "+document.getElementById("latitud").value;
  latLng2 = new google.maps.LatLng(document.getElementById("latitud").value ,document.getElementById("longitud").value);
  marker.setPosition(latLng2);
  map.setCenter(latLng2);
  geocodePosition (latLng2);
}

// OBTIENE LAS COORDENADAS DE lA DIRECCION EN LA CAJA DEL FORMULARIO
function codeAddress(tipo,caso)
{
    var addressSalida,addressLlegada = null;
    var address = '';
    console.log(tipo+' '+caso);
    if(caso == 'cambio'){
        geocoder = new google.maps.Geocoder();
        if(tipo == 'salida'){
            addressSalida = document.getElementById("dir_recogida_edit").value + ","+ciudadUbicacion;
        }else if(tipo == 'destino'){
            addressLlegada = document.getElementById("dir_entrega_edit").value + ","+ciudadUbicacion;
        }
    }else{
        addressSalida = document.getElementById("direccionsalida").value + ","+ciudadUbicacion;
        addressLlegada = document.getElementById("direccionllegada").value + ","+ciudadUbicacion;
    }
    if(tipo == 'salida'){
        address = addressSalida;
    }else{ address = addressLlegada}
    console.log(address);
    if(caso == 'cambio'){
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
//                latSalida = $('#latitudsalida').val();
//                longSalida = $('#longitudsalida').val();
//                latLlegada = $('#latitudllegada').val();
//                longLlegada = $('#longitudllegada').val();
//                dirSalida = $('#direccionsalida').val();
//                dirLLegada = $('#direccionllegada').val();
//                horaentrada = $('#horaentrada').val();
//                diarecogida = $('#diarecogida').val();
//                otrosDias = $('#diasrecogida').val();
                if(tipo == 'salida'){
                    $("#latitudsalida").val(results[0].geometry.location.lat());
                    $("#longitudsalida").val(results[0].geometry.location.lng());
                    $('#direccionsalida').val($('#dir_recogida_edit').val());
                    console.log($("#latitudsalida").val()+' '+$("#longitudsalida").val()+' '+$('#direccionsalida').val());
                }else if(tipo == 'destino'){
                    $("#latitudllegada").val(results[0].geometry.location.lat());
                    $("#longitudllegada").val(results[0].geometry.location.lng());
                    $('#direccionllegada').val($('#dir_entrega_edit').val());
                    console.log($("#latitudllegada").val()+' '+$("#longitudllegada").val()+' '+$('#direccionllegada').val());
                }

            }else{
                console.log('ERROR : ' + status);
            }
        });
    }else{
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
               updateMarkerPosition(results[0].geometry.location);
               console.log(marker);
               marker.setPosition(results[0].geometry.location);
               map.setCenter(results[0].geometry.location);
            } else {
              console.log('ERROR : ' + status);
            }
        });
    }
}
function cambiarHoraEntrada(val){
    $("#horaentrada").val(val);
    console.log($("#horaentrada").val());
}

//FUNCION QUE ABRE EL POPUP DE LA RESERVA
function abrirReserva(){
    var latsalida = $("#latitudsalida").val();
    var longsalida = $("#longitudsalida").val();
    var recogida = latsalida+','+longsalida;
    var latllegada = $("#latitudllegada").val();
    var longllegada = $("#longitudllegada").val();
    var entrega = latllegada+','+longllegada;
    $('.cargando').show();
    console.log(recogida+' --- '+entrega);
    validarZona(recogida,entrega);

    //alert(navigator.platform+' == Win32');
}
function validarZona(recogida,entrega){
    var datos = '{"userid" : "'+localStorage['idusuario']+'" , "aksalida" : "'+recogida+'", "akllegada" : "'+entrega+'"}';
    var url = direccionservidor + "web/app.php/movilapi/validarZonas";
    console.log(datos);
    var objeto=$.ajax
    ({
      type: "POST",
      url: url,
      data: datos,
      cache: false,
      success: function()
      {
          var json = JSON.parse(objeto.responseText);
          console.log(json);
          if(json.error == "")
            {
                localStorage.ubicacion = 'popup_reserva';
                //$('#popup_recogida').popup('open');
                //$('#dejada').removeClass('dejada').addClass('dejadar');
                //$("#map-canvas-user").css({height: '100px'});
                //valVersionAndroid('abrir');
                //valVersionAndroid('valdia');
               saldoActual('reserva');
              //navigator.notification.alert("Parada guardada con \u00e9xito" , null, nombreaplicacion, "Aceptar");
            }
            else
            {
                alerta(json.error, nombreaplicacion, "Aceptar");
              //navigator.notification.alert(json.error , null, nombreaplicacion, "Aceptar");
            }
            $('.cargando').hide();
      },
      error: function()
      {
          $('.cargando').hide();
          alerta("Error en la conexión 5, valide e intente de nuevo", nombreaplicacion,"Aceptar");
          console.log("Ocurrió un error al validar la zona.");
//          $('.cargando').hide('active');

      }
    });
}
function valVersionAndroid(tipo) {
    var res = null;
    if (navigator.platform == 'Win32') {
        //alert('win32');
        if (tipo == 'abrir') {
            localStorage.android = 'mayor';
            $('#diarecogidamayor').removeClass('oculta');
        } else if (tipo == 'valdia') {
            res = $('#diarecogidamayor').val();
            valFechaReserva(res);
            //diasRecogidas(res);
        }
    } else {
        var ua = navigator.userAgent;
        //alert(ua.indexOf("Android"));
        if (ua.indexOf("Android") >= 0) {
            var androidversion = parseFloat(ua.slice(ua.indexOf("Android") + 8));
            //alert('version: '+androidversion);
            if (androidversion < 5) {
                if (tipo == 'abrir') {
                    localStorage.android = 'menor';
                    $('#diarecogidamayor').attr('type', 'hidden');
                    $('#diarecogidamenor').attr('type', 'text');
                    //alertr('android 4.1');
                } else if (tipo == 'valdia') {
                    res = $('#diarecogidamenor').val();
                    valFechaReserva(res);
                    //diasRecogidas(res);
                }
            } else {
                if (tipo == 'abrir') {
                    console.log('android mayor y seteara atributo input type');
                    localStorage.android = 'mayor';
                    $('#diarecogidamayor').attr('type', 'text');
                    $('#diarecogidamenor').attr('type', 'hidden');
                    //alert(androidversion);
                } else if (tipo == 'valdia') {
                    res = $('#diarecogidamayor').val();
                    valFechaReserva(res);
                    //diasRecogidas(res);
                    //alert(res);
                }
            }
        } else {
            //alert(tipo);
            if (tipo == 'abrir') {
                //alert('android mayor y seteara atributo input type');
                localStorage.android = 'mayor';
                $('#diarecogidamayor').attr('type', 'text');
                $('#diarecogidamenor').attr('type', 'hidden');
                //alert(androidversion);
            } else if (tipo == 'valdia') {
                res = $('#diarecogidamayor').val();
                valFechaReserva(res);
                //diasRecogidas(res);
                //alert(res);
            }
        }
    }
}
//FUNCION QUE ABRE EL POPUP DE LA RESERVA
function cerrarReserva(){
    $('#dejada').removeClass('dejadar').addClass('dejada');
    $("#map-canvas-user").css({height: '670px'});
}
function validarGps(dato,caso,agendado){
    document.addEventListener("deviceready",function() {
        //alert('entro a validar gps: '+dato+' caso: '+caso);
            datoGPS = dato;
            casoGps = caso;
            cordova.plugins.diagnostic.isLocationEnabled(function(enabled){//depende del cordova-plugin-diagnostic
                var estado = (enabled ? "enabled" : "disabled");
                console.log('estado para activarme en app: '+estado+'  valor de boton: '+dato);
                console.log("activar gps esta " + (enabled ? "enabled" : "disabled"));
                if(estado === 'disabled'){
                    //alerta('Por favor activar el gps');
                    dialogActivarGps();
                }else if(estado === 'enabled'){
                    //alert('else de validar localizacion redireccion obtener mi ruta datoGPS '+datoGPS+' casoGPS '+casoGps);
                    if(datoGPS,casoGps){
                        setTimeout(function(){obtenerMiruta(datoGPS,casoGps,agendado);},200);
//                        if(localStorage.rol == '4'){
//                            setTimeout(function(){valDatosImportantes();},3000);
//                        }
                    }else{
                        console.log('va a crear el mapa despues del inicio');
                        setTimeout(function(){createMap(dato);},200);
                    }
                }else{
                    console.log('el gps no esta activado ni desactivado o.O');
                }
            }, function(error){
                console.error("El siguiente error a ocurrido: "+error);
            });
        });
}
function dialogActivarGps(){
    navigator.notification.confirm(//depende del plugin cordova-plugin-dialogs
        "Su GPS esta desactivado por favor activelo e intente de uevo.",//message
        onConfirm,                // callback to invoke with index of button pressed
        "Por favor active su GPS",//Titulo
        ["Cancel","Despues","Continuar"]//Nombre botones
    );
}
function onConfirm(buttonIndex) {
    console.log(buttonIndex);
    switch(buttonIndex) {
        case 1: validarGps(datoGPS,casoGps);//cancelar
        case 2: validarGps(datoGPS,casoGps);//opcion neutra
        case 3: activarGpsAndroidIos(datoGPS,casoGps);break;//confirmacion
    }
    //alert('You selected button ' + buttonIndex);
}
function activarGpsAndroidIos(dato,caso){
    if(typeof cordova.plugins.settings.open != undefined){//depende del plugin cordova-plugin-native-settings
            cordova.plugins.settings.open("location",function(){
            //alert("activar gps activo");
            //alert(dato+caso);
            if(dato,caso){
//                if(caso == 'singps'){
//                    validarGps(datoGPS,casoGps);
//                }else{
                    obtenerMiruta(dato,caso);
                //}
            }else{
                createMap();
            }
        },
        function(){
            console.log("falla al intentar abrir gps");
        });
    }
}
$( document ).on( "pageinit", function() {
    $( "#popup_recogida" ).on({
        //Disparado después de que una ventana emergente haya completado los preparativos para la apertura, pero aún no se ha abierto
        popupbeforeposition: function(event, ui) { },
        //Disparador que se activa cuando una ventana emergente está completamente abierta
        popupafteropen: function(event, ui) {},
        //Disparador que se activa cuando una ventana emergente está completamente cerrada
        popupafterclose: function() {
            cerrarReserva();
        }
    });
});
