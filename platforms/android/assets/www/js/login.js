var nombreaplicacion = 'BLACK';
var direccionservidor = "https://www.blacktransport.co/";
//funcio que loguea en el sistema
function login(){
    try{
        $('.cargando').show();
        if(document.getElementById("username").value == "" || document.getElementById("password").value == ""){
            $('.cargando').hide();
            alerta("Datos incompletos, por favor completa los datos faltantes",nombreaplicacion);
        }else{
            //var y = '{"login" : [ "'+document.getElementById("username").value+'"] , "password" : [ "'+ hex_sha1(document.getElementById("password").value)+'"] }';
            var y = '{"login" : [ "'+document.getElementById("username").value+'"] , "password" : [ "'+document.getElementById("password").value+'"] }';
            var url = direccionservidor + "web/app.php/userapi/ingreso";
            console.log(y);
            var objeto=$.ajax({
                type: "POST",
                url: url,
                data: y,
                cache: false,
                success: function()
                {
                    var json = JSON.parse(objeto.responseText);
                    console.log(json);
                    if(json[0].error == ""){

                        localStorage.idusuario = json[0].id;
                        localStorage.rol = json[0].rol;
                        localStorage.token = json[0].token;
                        localStorage.convenio = json[0].convenio;
                        localStorage.telefono = json[0].telefono;
                        localStorage.disponible = json[0].disponible;

                        if(localStorage.rol == 3){
                            //alert('rol = 3');
                            $('.cargando').hide();
                            //$.mobile.changePage( "main.html#home", { transition: "fade", changeHash: true }, true, true);
                            console.log(json);
                            localStorage['conductor'] = json[0].conductor;
                            localStorage['idrutaLocation'] = 0;
                            var contadorcarros = 0;
                            var formaselect = document.getElementById('selectcarroconduce');
                            var html = "";
                            while(json[0].carro[contadorcarros + '~1'] != null && json[0].carro[contadorcarros + '~1'] != undefined)
                            {
                              html += "<option value ='" + json[0].carro[contadorcarros + '~1'] + "'>" + json[0].carro[contadorcarros + '~2'] + "</option>";
                              contadorcarros++;
                            }
                            console.log(contadorcarros);
                            $(formaselect).html(html);

                              accionesConductor('inicial');

                        }else{
                            $('.cargando').hide();
                            localStorage.email = json[0].email;

                            console.log('se mostrara alerta');

                            accionesPasajero();
                        }

                        if(navigator.platform == 'Win32'){
                            console.log('aqui deberia de registrar el dispositivo para las notificaciones.');
                        }else{
                            registerdevice();
                        }

                    }else{
                        $('.cargando').hide();
                        alerta("Usuario o contrase\u00F1a incorrectos ,verifique may\u00Fasculas y min\u00Fasculas",nombreaplicacion,'Aceptar');
                    }
                },error: function(err){
                    alert('No hemos detectado internet, valide e intente nuevamente', nombreaplicacion,"Aceptar");
                    console.log("Ocurri\u00f3 un error al conectarse con el servidor, valide su conexion e int\u00e9ntelo nuevamente"+ err +"");
                    $('.cargando').hide();
                }
            });
        }
    }catch(err){
        console.log(err);
    }
}

function accionesSalir(){
    $.mobile.changePage( "#index", { transition: "slide", changechangeHash: false});

    localStorage.clear();
    sessionStorage.clear();
    console.log('limpia interval');
    clearInterval(intervalC);
    clearInterval(interval);

    limpiarIntervalo();
    parar();
    $('#obtener_miruta_entrega').hide();
    $('#obtener_miruta_recogida').hide();
    $('#obtener_misrutas').hide();
    $('#historico').hide();

    $('#btn_prfil').hide();

    $('#ingresar').show();
    $('#btnregistro').show();
    $('#salir').hide();
    $('#btn_recargar_cuenta').hide();
    $('#seguimiento').hide();
    $('#nueva_reserva').hide();
    $('#descargar_qr').hide();

    $("#total_tickets").val(0).change();
    login_limpiezaVariablesGlobales();
    resetContinuarReserva();
    localStorage.ubicacion = 'index';
}
function exitoborrando(response){
    console.log(response);
    console.log('exito borrando ubicaciones');
}
function fallaborrando(response){
    console.log(response);
    console.log('FALLA borrando ubicaciones');
}
function salir(){
    if(localStorage.rol == '3'){
      if(bgGeo){
        bgGeo.stop();
        bgGeo.deleteAllLocations(exitoborrando,fallaborrando);
    }
  }
//    if(localStorage['rol'] == 3){
        var y = '{"userid" : ["'+localStorage['idusuario']+'"] , "token" : ["'+localStorage['token']+'"]}';
        var url = direccionservidor + "web/app.php/userapi/salir";
        console.log(y);
        var objeto=$.ajax({
            type: "POST",
            url: url,
            data: y,
            cache: false,
            success: function(){
                var json = JSON.parse(objeto.responseText);
                console.log(json);
                if(json.error == ""){
                     console.log('exito borrando el device');
                }else{
                    alerta(json.error,nombreaplicacion,'Aceptar');
                }
            },
            error: function(){
                console.log('Ocurrio un error al intentar salir.');
                alert('No hemos detectado internet, valide e intente nuevamente', nombreaplicacion,"Aceptar");

                $('.cargando').hide();
            }
        });
        accionesSalir();

}

function login_limpiezaVariablesGlobales(){

    console.log('limpio todas las variables globales');
    entrega = null;
    mipos = null;
    hederSel = null;
    titulos = [];
    caduca = "31 Dec 2020 23:59:59 GMT"
    saldo = null;
    sinsaldo = null;
    bgGeo = null;
    actualPos=null;
    longOrigen=null;
    latOrigen=null;
    longDestino=null;
    latDestino=null;
    dirOrigen=null;
    dirDestino=null;
    fechaServ=null;
    n_pasajeros=null;
    horaServ=null;

    dirllegadaPush = null;
    dirdestinoPush = null;
    //servicios.js
    refreshIntervalId=undefined;
    nuevalat = '';
    nuevalong = '';
    us = '';
    interval = null;
    circulo = null;
    infowindow = null;
    mapa=undefined;
    marker=undefined;
    i=undefined;
    datos = null;
    conductorpush = null;
    celularpush = null;
    rutadestinopush = null;
    rutallegadapush = null;
    placapush = null;
    idrutapush = null;
    tipomapa = null;
    polilinea=null;
    centerControlDiv = '';
    //alertas.js
    tipoDevice=undefined;//revisar si esto esta en index, variable global que guarda el tipo de dialogo que se mostrara en el dispositivo movil.
    idrecibido=undefined; //
    conductor = null;
    celular = null;
    rutadestino = null;
    rutallegada = null;
    placa = null;
    conductornotificado = null;
    celularnotificado = null;
    rutadestinonotificado = null;
    rutallegadanotificado = null;
    placanotificado = null;
    idrutanotificado = null;
    //loginredes.js
    nombre=undefined;
    apellido=undefined;
    email=undefined;
    foto=undefined;
    accessToken=undefined;
    uid=undefined;
    status=undefined;
    //mapas.js
    geocoder=undefined;
    markerYO=undefined;
    latLng=undefined;
    map=undefined;
    latitud = 0;
    longitud = 0;
    rutaconductor = '';
    ak = '';
    mapausuario = '';
    tiporuta = '';
    puntoDestino= '';
    coordenaActual = "";
    divMapa = "";
    datoGPS = "";
    casoGps = "";
    estadocond = null;
    ciudadUbicacion="";
    //pagos.js
    horasentrada = null;
    //reserva.js
    num = '';
    diasrecogida = new Array();
    y = [];
    latSalida=undefined;
    longSalida=undefined;
    latLlegada=undefined;
    longLlegada=undefined;
    horaentrada=undefined;
    diarecogida=undefined;
    otrosDias=undefined;
    cordSalida=undefined;
    cordLlegada=undefined;
    userid=undefined;
    horacompleta=undefined;
    //rutas.js
    intcalificacion=6;
    ubicaciones_pasaj=[];
    orden_realpasaj=[];
    rutasDomicilios = [];
    rutasOficina = [];
    direccion = "";
    telefono = "";
    devicePasajero = "";
    idreserva = "";
    nombrePasajero = "";
    latitudConductor = "";
    longitudConductor = "";
    calculoDistancia = false;
    posicionActual = "";
    watchId  = null;
    traerDireccion = [];
    estado = true;
    a = null;
    pagina = null;
    intervalC = null;
    autoincremento = 1;
    alertamostrada = false;
    servicioiniciado = false;
    veces=0;
    ultimaHora=0;
    //tiempo.js
    centesimas = 0;
    segundos = 0;
    minutos = 0;
    horas = 0;
    minutosR = 0;
    horasR = 0;
    minutofin = '';
    minutopasado = 0;
    horafin = '';
    contadormin = 0;
    contadorhoras = 0;
    nombreaplicacion = 'BLACK';
    direccionservidor = "https://www.blacktransport.co/";
    alertamodal = 0;
    control = false;
}

//funcio que envia el form del login le facebook.
function enviarLogin(tipo,email,nombre,foto,accessToken,uid){
    var datos = null;
    var url = direccionservidor + "web/app.php/userapi/registroFB";
    var usuario = email;
    var clave = uid;
    //alert(usuario+"--"+clave+"--"+nombre);
    datos = '{"login" : [ "'+email+'"] , "password" : [ "'+ uid+'"] , "nombre" : [ "'+ nombre+'"]}';
    var ob1 = $.ajax ({
        url: url,
        type: "POST",
        data: datos,
        success: function() {

            var json = JSON.parse(ob1.responseText);
            var token = json.token;
            var idusuario = json.idusuario;
            var rol = json.rol;
            console.log(token,idusuario,rol);
            if(token != ''){
                $('.cargando').hide();
                localStorage['idusuario'] = idusuario;
                localStorage['rol'] = rol;
                localStorage['token'] = token;
                localStorage['convenio'] = '';
                localStorage['telefono'] = '';
                localStorage['email'] = email;
                obtenerDatosUsuario(tipo);
                $('.cargando').hide();
                console.log('se mostrara alerta');
                //la funcion alerta valida el tipo de dispositivo que se usa y muestra la alerta o la confirmacion segun sea el caso.
                accionesPasajero();
                //alerta('se registrara el movil',nombreaplicacion,'Aceptar');
                if(navigator.platform == 'Win32'){
                    console.log('aqui deberia de registrar el dispositivo para las notificaciones.');
                }else{
                    registerdevice();
                }
            }else{
                alerta("Error al intentar conectar con facebook, por favor intente de nuevo.",nombreaplicacion,'Aceptar');
            }
        },
        error: function(err) {
            console.log("Ocurrió un error en la validacion con el servidor"+ err +"",nombreaplicacion,'Aceptar');
            alert('No hemos detectado internet, valide e intente nuevamente');
            $('.cargando').hide();
        }
    });
}

function esperita(seleccion) {
    if (seleccion.value == 'si') {
        setTimeout(function () { prepareReadQR(); }, 500);
        //QRScanner.show();
    }
}

function prepareReadQR() {
    QRScanner.scan(mostrarCedula);
    readQR();
}

//ajustes visuales al leer QR
function readQR() {
    //QRScanner.scan(mostrarCedula);
    //$("#leerQR").val('no');
    //$("#leerQR").slider("refresh");
    QRScanner.show();
    $("#homeconductor").css("opacity", "0.0");
    //$("#index").css("opacity", "0.0");
    $("#homeconductor").css("display", "none");
    //$("#index").css("display", "none");
    $("#camera").css("display", "block");

}


function mostrarCedula(err, text) {
    if (err) {
        //alert('error '+err);
        console.error(err._message);
        QRScanner.hide();
        QRScanner.destroy();
        // an error occurred, or the scan was canceled (error code `6`)
    } else {
        // The scan completed, display the contents of the QR code:
        //comprobarCedula(text);
        //alert(text);
        QRScanner.hide();
        //QRScanner.cancelScan(function(status){
        //      console.log(status);
        //  });
        //QRScanner.pausePreview();
        QRScanner.destroy(function (status) {
            console.log(status);
        });
        $("#homeconductor").css("opacity", "1.0");
        //$("#index").css("opacity", "1.0");
        $("#camera").css("display", "none");
        $("#homeconductor").css("display", "block");
        //$("#index").css("display", "block");
        if (typeof text=='string') {
            valRecogida(text);
        }else{
        valRecogida(text.result);}

    }
}

function login_cancelar_scan() {
    QRScanner.destroy(function (status) {
        console.log(status);
    });
    $("#homeconductor").css("opacity", "1.0");
    //$("#index").css("opacity", "1.0");
    $("#camera").css("display", "none");
    $("#homeconductor").css("display", "block");
    //$("#index").css("display", "block");
    QRScanner.destroy(function (status) {
        console.log(status);
    });
}

function login_ingresar_codigo() {
    //QRScanner.destroy(function(status){
    //  console.log(status);
    // });
    //$("#camera").css("display","none");
    var funcioncancel='cancelar_ingresarCodigo()';
    var mensaje='Escribe el código:';
    var botonl='Listo';
    login_modal2(mensaje,true, true, true,funcioncancel, true,botonl,'login_codigo_ingresado()');
    //$("#ingresarCodigo").css("display", "block");
}

function login_modal2(mensaje,pregunta,entrada,cancelar,funcionCancelar,seguir,mnsboton,funcionSeguir){
    $("#ingresarCodigo").css("display", "block");
    if (pregunta==true){
        $('.pregunta').css('display','initial');
        $('.pregunta').text(mensaje);
    }else{$('.pregunta').css('display','none');}
    if (entrada==true){
        $('.entrada1').css('display','initial');
    }else{$('.entrada1').css('display','none');}
    if (cancelar==true){
        $('.cancelar1').css('display','block');
        $('#cancelar1').attr('onclick',funcionCancelar);
    }else{$('.cancelar').css('display','none');}
    if (seguir==true){
        $('.continuar1').css('display','block');
        $('#continuar1').attr('onclick',funcionSeguir);
        $('#continuar1').text(mnsboton);
    }else{$('.continuar1').css('display','none');}
}


function cancelar_ingresarCodigo() {
    $("#ingresarCodigo").css("display", "none");
}

function login_codigo_ingresado(a) {
    QRScanner.destroy(function (status) {
        console.log(status);
    });
    $("#camera").css("display", "none");
    $("#ingresarCodigo").css("display", "none");
    $("#homeconductor").css("opacity", "1.0");
    //$("#index").css("opacity", "1.0");
    $("#homeconductor").css("display", "block");
    //$("#index").css("display", "block");
    if (!a) {
        var codig=$("#codigoC").val();
        if (codig == '') {
            alerta("¡Datos incompletos!",nombreaplicacion);
        } else {
            valRecogida(codig);
            $("#codigoC").val("");
        }
    } else {
        valRecogida('');
    }
    QRScanner.destroy(function (status) {
        console.log(status);
    });
}
